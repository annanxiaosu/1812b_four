
const http=require('http');

const AlipaySdk =require('alipay-sdk');
const AlipayFormData =require( 'alipay-sdk/lib/form');

const alipaySdk = new AlipaySdk({
    // 参考下方 SDK 配置
    appId: '2021000118609273',
    privateKey: 'MIIEpAIBAAKCAQEA2grJJs3FgKsZo0VBDfddw/FKY/m+aPbq8j9CqpQ/Zda3C+QaaSzsNHQucVg7mWxa/EnPq/SR8MXnSev/HK/iapdUqz+0Spnq4A9hl5OdZyJBZQ9MYj+bXUF7lewDc2uzPA81oX+uNPGdLMrbN3pogdELtoNlfXRV2BFOdQsOEgV5WIt5/b1lfcz2ZnWRJjaQcrJlFlA6qbjCasRqFPTdYw8VQBplaPcLa+0g6nI3hYA5PcVaZfaAk6+nIq9shgjskBT/aWx+FOMEjtanxGo2sgyrG0HHm081GSAwYorfSyeIZrm3wSpBsDMpkf3C7qphi1CWM7DIcFmz4ISG0pYU/QIDAQABAoIBAFX+QPz71LP9Rxw8eP7AG4vmwvcRDUaqc1jdGDbcRvCsMP8QlFX8sJ+zXom3SbIlqPCtT6k6yMAT3TcrSy8OMxbk3jC37Cwr7hU2G8BDKKX/PZlU4kQxX930sIAdV/MSe9rpCLbL+3HIBLwguTp1HXhtcNgnlERZOG4BzVad6Iu0QCPzLR7zPHcZ+QfOS5bq8keh/tqj/hoz1dP7jtyMkhreAR09g84oUDEk22ELnm7BvNlg9hQCXuOt43Ivgb1YPl63i87NErirn2niVyLjtLZcFQdAaNY5JZ+kSN0ZSGNhD7NQgqe2i+zq/9WgcfBrZyophFHLZiiOP7wbxri6dgUCgYEA/dASbmeFAVtWt0cohDdxF4aZiYoMEtD0qwHr+pkxRm59NIikBgp8i4QjUBCqyZsXiubO8evK4hd/GmTDE/jP2Ep0UcnWLifsytnWdoOsH0d4UJI+HGZLdT+smihbxVzgsHWscMoSb4xK2eFsUQVglxsqzimtThVx/Jm5KwWb0YcCgYEA2+vNIhu1cMJqCwHfJZ9SlDQHwfpvT9Q6+V8GvYJx+wP65Q1oKMJdFHo0hWPHDuOGDTQIDaTYGxotcs0qp8xsYIla4ufkBvPIVlsFeKRNsym3pjOp2DTi1B2xqa+KS+ly5c3ZcpUG95MOSZAE9vgFzQmPgao9jtuUXgEaB/6HFlsCgYEAnz0U4zmFY+WHUTOOwuz5whXLbFQ04PVCG6v3/mBwXszhNr1nkvavTuTXNttOG3B3Y8kNGxChWBn/v3JqRbR7yipkMhAclRatZFMpT2+q5HD1li86Up0HSIdrZcheU0PDIFKjnpvY1y1ApNPeHjLud78uquvQ9EVWH3Hej5RUPy8CgYAuvAposCi7oKT2V9x620OqOPBZGYYkGb66laYSCbF9HVho6pNy7fLn0hxtKEn58QHgtmjrtVczUznLRqxE4l05mpbU12CO1amCKG+eNpRIeZoMWLhEOODnILRLkgE6po6DJV6LSf4kCH0lxtQ3Q3fAHcMAVBQm0qTR4TwN7Nn6NwKBgQC6xmwqvd/XYYBs5Nc8rdKZkRJw2Uow1oFEF6qO75k5lHEAZJgCW84h0wzUmrSZyEvHr3m1Oukl4KW8C9/abpyuaY3kzVrM/h5M51Y1fMsDQoHRxXf0+SywidZdnbox9JlAEqUaT3u7tB4sQf8+xXCuB6dFyp5iyih/pGMgovNYLg==',
    //可设置AES密钥，调用AES加解密相关接口时需要（可选）
    encryptKey: 'HMZOcp3yih6+W9ydfVTyZw=='
});

http.createServer((req,res)=>{
    if(req.url='/alipay/create'&&req.method==='POST')
    {
        const formData = new AlipayFormData();
        formData.setMethod('get');
        formData.addField('notifyUrl', 'http://www.com/notify');
        formData.addField('bizContent', {
            outTradeNo: 'out_trade_no',
            productCode: 'FAST_INSTANT_TRADE_PAY',
            totalAmount: '0.01',
            subject: '商品',
            body: '商品详情',
        });

        res.setHeader('Access-Control-Allow-Origin','*');
        const result = alipaySdk.exec(
            'alipay.trade.page.pay',
            {},
            { formData: formData },
        ).then(result=>{
            res.end(JSON.stringify({
                code: 200,
                meg: result
            }))
        }).catch(err=>{
            res.end(JSON.stringify({
                code: 500,
                meg: err.toString()
            }))
        });

        // result 为 form 表单
        console.log(result);
        
    }else{
        res.end(JSON.stringify({
            code:403,
            meg:"禁止访问"
        }))
    }
}).listen(9999);
console.log('正在监听端口9999');


