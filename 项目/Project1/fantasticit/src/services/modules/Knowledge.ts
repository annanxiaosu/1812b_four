import { request } from 'umi';
// 获取知识小册列表
export function getKnowledge(page: number, pageSize = 6, status = 'publish') {
    return request('/api/knowledge', {
        // 表示请求地址栏上面的查询参数
        params: {
            page,
            pageSize,
            status,
        },
        // 表示请求体里面传递的数据
        data: {},
    })
}
//获取文章分类数据接口
export function getKnowledgeClassify(articleStatus = 'publish') {
    return request('/api/category', {
        params: {
            articleStatus
        },
        data: {}
    })
}

// 获取第一个详情页
export function getKnowledgeDetail(id?: string) {
    return request(`/api/knowledge/${id}`)
}

// 获取第二个详情页
export function getKnowledgeDetailList(id: string) {
    return request(`/api/knowledge/${id}/views`, {
        method: 'POST'
    })
}
interface Like {
    id: string,
    type: string,
}
// 喜欢
export function getKnowledgeLike(payload: Like) {
    return request(`/api/knowledge/${payload.id}/likes`, { method: 'post', data: { type: payload.type } })
}