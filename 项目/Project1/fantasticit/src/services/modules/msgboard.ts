import { addMsg } from '@/utils/msgabroad';
import { request } from 'umi';
//获取评论列表
export const getMessage = (params: any) => {
    return request('/api/comment/host/8d8b6492-32e5-44e5-b38b-9a479d1a94bd?', { params })
};
//获取推荐阅读
export const getRead = () => {
    return request('/api/article/recommend')
};
//追加评论
export const addmsg = (data: addMsg) => {
    return request('/api/comment', {
        method: 'post',
        data
    })
}