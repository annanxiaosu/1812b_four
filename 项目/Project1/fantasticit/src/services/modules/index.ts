import {request} from "umi"
//列表数据
export let getList = () => {
    return request("/api/Knowledge")
}
//推荐阅读数据
export let readingList=()=>{
    return request ('/api/article/recommend')
}
// //文章分类数据
export let getArticle = ()=>{
    return request(`/api/category`,{articleStatus:"publish"})
}
//进详情
export let goDetail = (id:string)=>{
    return request(`/api/article/${id}/views`,{
        method:"POST"
    })
}
//详情右边请求
export let detailRead =(id:string)=>{
    return request(`/api/article/recommend?articleId=${id}`)
}
//喜欢
export let changeLikes=(id:string,params:object)=>{
    return request(`/api/article/${id}/likes`,{
        method:"POST",
        data:params
    })
}