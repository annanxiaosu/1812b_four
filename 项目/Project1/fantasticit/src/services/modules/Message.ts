import { request } from 'umi';

// 获取留言板评论的接口
export function getMessage(page: number, pageSize = 6) {
    return request('/api/comment/host/8d8b6492-32e5-44e5-b38b-9a479d1a94bd', {
        params: {
            page,
            pageSize,
        },
    })
}
