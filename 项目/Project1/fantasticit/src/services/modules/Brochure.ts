import { request } from 'umi';
// 获取知识小册列表
export function getBrochure(page: number, pageSize = 6, status = 'publish') {
    return request('/api/knowledge', {
        // 表示请求地址栏上面的查询参数
        params: {
            page,
            pageSize,
            status,
        },
        // 表示请求体里面传递的数据
        data: {},
    })
}
export function getReading(articleStatus = 'publish') {
    return request('/api/category', {
        params: {
            articleStatus
        },
        data: {}
    })
}

// export function getRecommend(id?:string){
//     let queryString = '';
//     id && (queryString = `?articleId=${id}`);
//     return request(`/api/article/recommend${queryString}`);
// }

export function getknow(id?: string) {
    console.log(id);
    return request(`/api/knowledge/${id}`)
}

// 获取第二个详情页
export function getknowList(id: string) {
    console.log(id);

    return request(`/api/knowledge/${id}/views`, {
        method: 'POST'
    })
}
