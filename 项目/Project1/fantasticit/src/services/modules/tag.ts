import { request } from 'umi';

export let GetTagData = (params:string) =>{
    return request( `api/article/tag/${params}?page=1&pageSize=12&status=publish` )
};
