import { request } from 'umi';

// 获取文章列表
export function getArticleList(page: number, pageSize = 12, status = 'publish') {
    return request('/api/article', {
        // 表示请求地址栏上面的查询参数
        params: {
            page,
            pageSize,
            status,
        },
        // 表示请求体里面传递的数据
        data: {},
    })
}

// 获取推荐文章
export let getRecommend = () => {
    return request('/api/article/recommend');
}

//文章——前端
export let getfelist = () => {
    return request('/api/article/category/fe', {
        data: {}
    })
}

// 文章后端
export let getbelist = () => {
    return request('/api/article/category/be', {
        data: {}
    })
}


// 文章——阅读
export let getreadinglist = () => {
    return request('/api/article/category/reading', {
        data: {}
    })
}

// 文章——linux
export let getlinuxlist = () => {
    return request('/api/article/category/linux', {
        data: {}
    })
}

// 文章——leetcode
export let getleetcodelist = () => {
    return request('/api/article/category/leetcode', {
        data: {}
    })
}

// 文章——要闻
export let getnewslist = () => {
    return request('/api/article/category/news', {
        data: {}
    })
}


// 获取文章详情
export function getArticleDetail(id: string) {
    return request(`/api/article/${id}/views`, {
        method: 'POST'
    })
}

// 获取文章评论
export function getArticleComment(id: string, page = 1, pageSize = 6) {
    return request(`/api/comment/host/${id}?page=${page}&pageSize=${pageSize}`)
}
// 文章——轮播图：详情列表
export let getlbtlist = () => {
    return request('/api/article/', {
        data: {}
    })
}
// 文章列表——详情
// export let getArticleDetail = (id: string) => {
//     return request(`/api/article/${id}/views`, {
//         method: 'POST'
//     })
// }
export let getlabel=()=>{
    return request("api/tag")
}