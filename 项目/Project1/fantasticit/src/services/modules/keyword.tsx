//https://api.blog.wipi.tech/api/search/article?keyword=%E5%86%99%E5%9C%A8%E5%B7%A5%E4%BD%9C

import { request } from "umi";

//模糊搜索
export function getkeyword(keyword:string) {
    return request('/api/search/article', {
        // 表示请求地址栏上面的查询参数
        params: {
            keyword
        },
    })
}