import { request } from 'umi';

// 获取留言板评论的接口
export function getreply(content: string, email: string, hostId: string, name: string, parentCommentId: string, replyUserEmail: string, replyUserName: string, url: "/page/msgboard") {
    return request('/api/comment', {
        params: {
            content,
            email,
            hostId,
            name,
            parentCommentId,
            replyUserEmail,
            replyUserName,
            url
        },
    })
}
