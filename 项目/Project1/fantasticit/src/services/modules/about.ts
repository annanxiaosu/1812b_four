import { request } from 'umi';
//获取评论列表
export function getAboutList(page:number,pageSize = 6){
    return request('/api/comment/host/a5e81ffe-0ad0-4be9-acca-c0462b1b98a1',{
        //表示请求地址栏上面的查询参数
        params:{
            page,
            pageSize,
       
        },
        //表示请求体里面传递的数据
        data:{},
        
    })
}

//获取推荐文章
export function getRecommendList(){
    return request('/api/article/recommend')
}
 

// import { IPages } from "@/utils/about" 
// export let getAbout = (params:IPages) => {
//     return request(`/api/comment/host/a5e81ffe-0ad0-4be9-acca-c0462b1b98a1?`,{params})
// }


 