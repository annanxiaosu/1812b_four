import { request } from "umi";

//文章标签
export function article_left_two(articleStatus = 'publish'){
    return request(`/api/tag?articleStatus=${articleStatus}`)
}

//跳详情
export function article_left_three(value:string,page=1,pageSize=12,status='publish'){
    return request(`/api/article/tag/${value}?page=${page}&pageSize=${pageSize}&status=${status}`)
}


//推荐阅读
export const article_right_top = () => {
    return request(`/api/article/recommend`)
}

//文章分类
export const article_right_botton = (articleStatus = 'publish')=>{
    return request(`/api/category?articleStatus=${articleStatus}`)
}