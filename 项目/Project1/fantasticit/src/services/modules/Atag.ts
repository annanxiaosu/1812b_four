import {request} from "umi"
export let getDetaillist = (value:string) => {
    return request(`/api/article/category/${value}`,{page:1,pageSize:12,status:"publish"})
}