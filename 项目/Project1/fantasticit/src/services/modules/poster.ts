import { IPosterItem } from '@/types/poster';
import { request } from 'umi';
// 生成海报
export function genePoster(data: IPosterItem){
    return request('/api/poster', {
        data,
        method: 'POST'
    })
}
