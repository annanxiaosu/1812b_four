export * from './modules/article'
// 知识小册
export * from './modules/Knowledge'
//抛出 关于页面
export * from "./modules/about"
//留言板
export * from './modules/Message'

export * from "./modules/index"

// export * from "./modules/msgboard"

//关于
export * from "./modules/about"

export * from "./modules/tag"

//export * from "./modules/Atag"

//模糊搜索
export * from "./modules/keyword"

//文章页面的文章标签
export * from "./modules/article_label"

export * from "./modules/Brochure"

// 回复
export * from './modules/getreply'
