import { IArticleItem,Brochure ,know,Child} from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {getBrochure,getReading,getknow,getknowList} from '@/services';
export interface BrochureModelState {
  BrochureList: IArticleItem [];
  BrochureReading:Brochure[];
  know:Partial<know>;
  knowlist:Partial<Child>
}

export interface BrochureModelType {
  namespace: 'Brochure';
  state: BrochureModelState;
  // 异步action，相当于vuex中的action
  effects: {
    getBrochure: Effect;
    getBrochureReading:Effect;
    getknow:Effect;
    getknowList:Effect;
  };
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<BrochureModelState>;
  };
}

const BrochureModel: BrochureModelType = {
  namespace: 'Brochure',
  state: {
    BrochureList:[],
    BrochureReading:[],
    know:{},
    knowlist:{}
  },

  effects: {
    *getBrochure({payload}, { call, put }) {
      let result = yield call(getBrochure, payload);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            BrochureList: result.data[0],
          }
        })
      }
    },
    *getBrochureReading({payload}, { call, put }){
      let result = yield call(getReading, payload);
      console.log('result...', result);
      if(result.success===true){
        yield put({
          type:'save',
          payload:{
            BrochureReading:result.data
          }
        })
      }
    },
    *getknow({payload}, { call, put }){
      let result = yield call(getknow, payload);
      console.log('result...', result);
      if(result.success===true){
        yield put({
          type:'save',
          payload:{
            know:result.data
          }
        })
      }
    },
    *getknowList({payload},{call,put}){
      let result=yield call(getknowList,payload);
      console.log(result);
      if(result.success===true){
        yield put({
          type:'save',
          payload:{
            knowlist:result.data
          }
        })
      }
    }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default BrochureModel
