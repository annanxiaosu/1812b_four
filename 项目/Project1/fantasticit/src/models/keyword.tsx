import { IArticleItem, keywordInof } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getkeyword } from '@/services';

export interface KeywordModelState {
    listt: keywordInof[];
}

export interface KeywordModelType {
    namespace: 'keyword';
    state: KeywordModelState;
    effects: {
        getkeyWord: Effect;
    };
    reducers: {
        save: Reducer<KeywordModelState>;
        // 启用 immer 之后
        // save: ImmerReducer<IndexModelState>;
    };
}

const KeywordModel: KeywordModelType = {
    namespace: 'keyword',

    state: {
        listt: [],
    },

    effects: {
        *getkeyWord({ payload }, { call, put }) { 
            let result = yield call(getkeyword,payload);
            if(result.success===true){
                yield put({
                    type:'save',
                    payload:{
                        listt:result.data
                    }
                })
            }
        },
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
        // 启用 immer 之后
        // save(state, action) {
        //   state.name = action.payload;
        // },
    },
};

export default KeywordModel;