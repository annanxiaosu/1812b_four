import {GetreplyObject} from '@/types';
import { Effect,  Reducer, } from 'umi';
import { getreply } from '@/services';

export interface getreplyModelState {
    getreplylist: GetreplyObject[];
}

export interface getreplyModelType {
    namespace: 'Getreply';
    state: getreplyModelState;
    // 异步action，相当于vuex中的action
    effects: {
        getReply: Effect;
    };
    // 同步action，相当于vuex中的mutation
    reducers: {
        save: Reducer<getreplyModelState>;
    };
}

const getreplyModel: getreplyModelType = {
    namespace: 'Getreply',
    state: {
        getreplylist: [],
    },

    effects: {
        *getReply({ payload }, { call, put }) {
            let result = yield call(getreply,payload);
            console.log('result...', result);
            if (result.success === true) {
                yield put({
                    type: 'save',
                    payload: { getreplylist: result.data[0]}
                })
            }
        },
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
    }
};

export default getreplyModel;