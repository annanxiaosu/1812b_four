import { IArticleItem, Knowledge, know, Child, Likes } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getKnowledge, getKnowledgeClassify, getKnowledgeDetail, getKnowledgeDetailList ,getKnowledgeLike} from '@/services';
export interface KnowledgeModelState {
  KnowledgeList: IArticleItem[];
  KnowledgeClassifyList: Knowledge[];
  know: Partial<know>;
  knowlist: Partial<Child>;
  KnowLike:Partial<Likes>
}

export interface KnowledgeModelType {
  namespace: 'Knowledge';
  state: KnowledgeModelState;
  // 异步action，相当于vuex中的action
  effects: {
    getKnowledge: Effect;
    getKnowledgeClassify: Effect;
    getKnowledgeDetail: Effect;
    getKnowledgeDetailList: Effect;
    getKnowledgeLike:Effect;
  };
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<KnowledgeModelState>;
  };
}

const KnowledgeModel: KnowledgeModelType = {
  namespace: 'Knowledge',
  state: {
    // 知识小册初始化数据
    KnowledgeList: [],
    // 知识小册右侧文章分类数据
    KnowledgeClassifyList: [],
    know: {},
    knowlist: {},
    KnowLike:{}
  },

  effects: {
    // 知识小册初始化数据方法
    *getKnowledge({ payload }, { call, put }) {
      let result = yield call(getKnowledge, payload);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            KnowledgeList: result.data[0],
          }
        })
      }
    },
    // 知识小册右侧文章分类数据方法
    *getKnowledgeClassify({ payload }, { call, put }) {
      let result = yield call(getKnowledgeClassify, payload);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            KnowledgeClassifyList: result.data
          }
        })
      }
    },
    // 第一个详情页的数据的方法
    *getKnowledgeDetail({ payload }, { call, put }) {
      let result = yield call(getKnowledgeDetail, payload);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            know: result.data
          }
        })
      }
    },
    // 第二个详情页的数据方法
    *getKnowledgeDetailList({ payload }, { call, put }) {
      let result = yield call(getKnowledgeDetailList, payload);
      console.log(result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            knowlist: result.data
          }
        })
      }
    },
    // 喜欢
    *getKnowledgeLike({ payload }, { call, put }) {
      let result = yield call(getKnowledgeLike, payload);
      console.log(result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            KnowLike: result.data
          }
        })
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default KnowledgeModel