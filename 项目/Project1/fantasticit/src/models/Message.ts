import { MessageItem } from '@/types';
import { Effect,  Reducer, } from 'umi';
import { getMessage } from '@/services';

export interface MessageModelState {
    messagelist: MessageItem[];
    //   获取数据的长度
    messagelength: number;
}

export interface MessageModelType {
    namespace: 'Message';
    state: MessageModelState;
    // 异步action，相当于vuex中的action
    effects: {
        getMessage: Effect;
    };
    // 同步action，相当于vuex中的mutation
    reducers: {
        save: Reducer<MessageModelState>;
    };
}

const MessageModel: MessageModelType = {
    namespace: 'Message',
    state: {
        messagelist: [],
        messagelength:0,
    },

    effects: {
        *getMessage({ payload }, { call, put }) {
            let result = yield call(getMessage,payload);
            console.log('result...', result);
            if (result.success === true) {
                yield put({
                    type: 'save',
                    payload: { messagelist: result.data[0],messagelength:result.data[1]}
                })
            }
        },
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
    }
};

export default MessageModel;