import { article_left_three, article_left_two, article_right_botton, article_right_top } from '@/services';
import { articleClassif, articleDetail, articleRemond, articleTop } from '@/types/article_label';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';


export interface articleLabel{
    article_label: labelModelState
}

export interface labelModelState {
    article_left_top: Array<articleTop>
    articleDetail: Array<articleDetail>
    articleRmond: Array<articleRemond>
    articleClassif: Array<articleClassif>
}

export interface IndexModelType {
    namespace: 'article_label';
    state: labelModelState;
    effects: {
        getArticleLeftTop: Effect;
        getArticleDetail: Effect;
        getArticleRmond: Effect;
        getArticleClassif: Effect;
    };
    reducers: {
        save: Reducer<labelModelState>;
        // 启用 immer 之后
        // save: ImmerReducer<labelModelState>;
    };
}

const labelModel: IndexModelType = {
    namespace: 'article_label',

    state: {
        article_left_top: [],
        articleDetail: [],
        articleRmond: [],
        articleClassif: [],
    },

    effects:{

        //第一步获取services的接口yield call
        //第二部 判断success是否正确
        //第三部 put获取到数据 yield put
        //第四部 类型 数据 type payload

        //文章标签
        *getArticleLeftTop({ payload }, { call, put }) {
            let result = yield call(article_left_two);
            console.log("文章标签.....",result);
            if(result.success===true)
            {
                yield put({
                    type:'save',
                    payload:{
                        article_left_top:result.data
                    }
                })
            }
        },

        //跳详情
        *getArticleDetail({ payload }, { call, put }){
            let result = yield call(article_left_three,payload);
            console.log("跳详情.....", result);
            if(result.success===true){
                yield put({
                    type:"save",
                    payload:{
                        articleDetail:result.data
                    }
                })
            }
        },

        //推荐阅读
        *getArticleRmond({payload},{call,put}){
            let result = yield call(article_right_top);
            console.log("推荐阅读.....", result);

            if(result.success===true){
                yield put({
                    type:"save",
                    payload:{
                        articleRmond:result.data
                    }
                })
            }
        },

        //文章分类
        *getArticleClassif({ payload }, { call, put }) {
            let result = yield call(article_right_botton);
            console.log("文章分类.....", result);

            if (result.success === true) {
                yield put({
                    type: "save",
                    payload: {
                        articleClassif: result.data
                    }
                })
            }
        }
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
        // 启用 immer 之后
        // save(state, action) {
        //   state.name = action.payload;
        // },
    },
};

export default labelModel;