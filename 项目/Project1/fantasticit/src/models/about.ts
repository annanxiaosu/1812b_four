import { getAboutList, getRecommendList } from '@/services';
import { IAboutItem, IArticleItem } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
//模块的状态
//接口的类型
export interface AboutModelState {
  recommendList: IArticleItem[];
  // recommend: IArticleItem[];
  //分页接口
  aboutList:IAboutItem[];
  aboutCount:number;
}

export interface  AboutModelType {
  namespace: 'about';
  state:AboutModelState ;
  //异步action,相当于vue action
  effects: {
    getRecommendList: Effect;
    // getRecommend: Effect;
    //分页
    getAboutList:Effect;
  };
  //同步action 相当于vuex 里面mutation
  reducers: {
    save: Reducer<AboutModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
  
}

const AboutModel:AboutModelType = {
  namespace: 'about',

  state: {
    recommendList: [],
    // recommend: [],
    aboutList:[],
    aboutCount:0

  },

  effects: {
    *getRecommendList({ payload }, { call, put }) {
      let result = yield call(getRecommendList);
      console.log('...result',result)
      //
      if(result.success === true){
        yield put({
          type:'save',
          payload:{getRecommendList:result.data}
        })
      }
    },
    // *getRecommend({ payload }, { call, put }) {
    //   let result = yield call(getRecommend);
    //   console.log('...result',result)
    //   //
    //   if(result.success === true){
    //     yield put({
    //       type:'save',
    //       payload:{recommend:result.data}
    //     })
    //   }
    // },
    *getAboutList({ payload }, { call, put }) {
      let result = yield call( getAboutList,payload);
      console.log('...result',result)
      //
      if(result.success === true){
        yield put({
          type:'save',
          payload:{
            aboutList:result.data[0],
            aboutCount:result.data[1]
          }
        })
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    }
  },
};

export default AboutModel;