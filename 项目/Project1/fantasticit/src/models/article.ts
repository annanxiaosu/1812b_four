import { IArticleItem, IArticleDetail, IArticleComment, IRootState } from '@/types';
import { Effect, Reducer } from 'umi';
import {
  getArticleList, getRecommend, getfelist, getbelist, getreadinglist,
  getlinuxlist, getleetcodelist, getnewslist, getlbtlist, getArticleDetail, getArticleComment
} from '@/services';

export interface ArticleModelState {
  recommend: IArticleItem[];
  articleList: IArticleItem[];
  articleCount: number;
  be_list: IArticleItem[];
  fe_list: IArticleItem[];
  read_list: IArticleItem[];
  linux_list: IArticleItem[];
  leetcode_list: IArticleItem[];
  news_list: IArticleItem[];
  articleCommentCount: number;
  articleDetail: Partial<IArticleDetail>;
  articleComment: IArticleComment[];
  lbt_list: IArticleItem[];
  article_Detail: Partial<IArticleDetail>;
}

export interface ArticleModelType {
  namespace: 'article';
  state: ArticleModelState;
  // 异步action，相当于vuex中的action
  effects: {
    getRecommend: Effect;
    getArticleList: Effect;
    getbelist: Effect;
    getfelist: Effect;
    getreadinglist: Effect;
    getlinuxlist: Effect;
    getleetcodelist: Effect;
    getnewslist: Effect;
    getArticleDetail: Effect;
    getArticleComment: Effect;
    getlbtlist: Effect;
    // getArticleDetail: Effect;
  };
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<ArticleModelState>;
  };
}

const ArticleModel: ArticleModelType = {
  namespace: 'article',
  state: {
    recommend: [],
    articleList: [],
    articleCount: 0,
    fe_list: [], //前端列表
    be_list: [],  //后端列表
    read_list: [], //阅读列表
    linux_list: [],  //linux列表
    leetcode_list: [],//letcode 列表
    news_list: [], //要闻列表
    articleCommentCount: 0,
    articleDetail: {},
    articleComment: [],
    lbt_list: [], //轮播图详情列表
    article_Detail: {}  //文章详情
  },

  effects: {
    *getRecommend({ payload }, { call, put }) {
      let result = yield call(getRecommend);
      // console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { recommend: result.data }
        })   
      }
    },
    // 文章所有列表
    *getArticleList({ payload }, { call, put }) {
      let result = yield call(getArticleList, payload);
      // console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            articleList: result.data[0],
            articleCount: result.data[1]
          }
        })
      }
    },
    // 前端列表
    *getfelist({ payload }, { call, put }) {
      let result = yield call(getfelist, payload);
      // console.log('result', result.data);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { fe_list: result.data }
        })
      }
    },
    // 后端列表
    *getbelist({ payload }, { call, put }) {
      let result = yield call(getbelist, payload);
      // console.log('result', result.data);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { be_list: result.data }
        })
      }
    },
    // 阅读列表
    *getreadinglist({ payload }, { call, put }) {
      let result = yield call(getreadinglist, payload);
      // console.log('result', result.data);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { read_list: result.data }
        })
      }
    },
    // linux列表
    *getlinuxlist({ payload }, { call, put }) {
      let result = yield call(getlinuxlist, payload);
      // console.log('result', result.data);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { linux_list: result.data }
        })
      }
    },
    // leetcode列表
    *getleetcodelist({ payload }, { call, put }) {
      let result = yield call(getleetcodelist, payload);
      // console.log('result', result.data);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { leetcode_list: result.data }
        })
      }
    },
    // 要闻列表
    *getnewslist({ payload }, { call, put }) {
      let result = yield call(getnewslist, payload);
      // console.log('result', result.data);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { news_list: result.data }
        })
      }
    },

    *getArticleDetail({ payload }, { call, put }) {
      let result = yield call(getArticleDetail, payload);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            articleDetail: result.data
          }
        })
      }
    },
    *getArticleComment({ payload }, { call, put, select }) {
      let preArticleComment = yield select((state: IRootState) => state.article.articleComment);
      let result = yield call(getArticleComment, payload.id, payload.page);
      console.log('result...', result);
      if (result.success === true) {
        let articleComment = result.data[0];
        if (payload.page !== 1) {
          articleComment = [...preArticleComment, ...articleComment];
        }
        yield put({
          type: 'save',
          payload: {
            articleComment,
            articleCommentCount: result.data[1]
          }
        })
      }
    },
    *getlbtlist({ payload }, { call, put }) {
      let result = yield call(getlbtlist, payload);
      // console.log('result', result.data);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: { lbt_list: result.data }
        })
      }
    },
    // *getArticleDetail({ payload }, { call, put }) {
    //   let result = yield call(getArticleDetail, payload);
    //   // console.log('result...',result);
    //   if (result.success === true) {
    //     yield put({
    //       type: 'save',
    //       payload: {
    //         article_Detail: result.data
    //       }
    //     })
    //   }
    // },

  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default ArticleModel;
