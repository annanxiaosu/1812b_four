import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getList, readingList,getArticle,changeLikes,goDetail,detailRead} from "@/services";
import {Iknow,Ireading,Iartice} from "../utils/know"
export interface IndexModelState {
  knowList:Array<Iknow>,
  readList:Array<Ireading>,
  articleList:Array<Iartice>
  readDetailList:Ireading
  detailReadList:Ireading
}

export interface IndexModelType {
  namespace: 'index'; 
  state: IndexModelState;
  effects: {
    getlist: Effect;
    readinglist:Effect;
    getarticle:Effect;
    changelikes:Effect;
    godetail:Effect;
    deatilread:Effect
  };
  reducers: {
    save: Reducer<IndexModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
  subscriptions?: { setup: Subscription };
}

const IndexModel: IndexModelType = {
  namespace: 'index',

  state: {
    knowList:[],
    readList:[],
    articleList:[],
    readDetailList:{},
  detailReadList:{}
  },
  effects: {
    //列表数据
    *getlist({ payload }, { call, put }) {
        let res =yield call(getList)
      yield put({
            type:"save",
            payload:{
              knowList:res.data
            }
        })
    },
    //推荐阅读
    *readinglist({payload},{call,put}){
      let result=yield call(readingList)
      yield put({
        type:'save',
        payload:{
          readList:result.data
        }
      })
    }, 
    //文章
    *getarticle({payload},{call,put}){
      let result =yield call(getArticle,100,200);
      yield put({
        type:"save",
        payload:{
          articleList:result.data
        }
      })
    },
    //进详情
    *godetail({payload},{call,put}){
      let result =yield call(goDetail,payload.id);
      yield put({
        type:"save",
        payload:{
          readDetailList:result.data
        }
      })
    },
    //喜欢
    *changelikes({payload},{call,put}){
      let par=payload.like?"dislike":"like"
      let type={type:par}
      let result=yield call(changeLikes,payload.id,type);
      yield put({
        type:"save",
        payload:{
          readDetailList:result.data
        }
      })
    },
    //详情阅读
    *deatilread({payload},{call,put}){
       
        
      let result =yield call(detailRead,payload.id);
      yield put({
        type:"save",
        payload:{
          detailReadList:result.data
        }
      })
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  },
 
};

export default IndexModel;