export interface Knowledge{
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}

export interface know {
  id: string;
  parentId?: any;
  order: number;
  title: string;
  cover: string;
  summary: string;
  content?: any;
  html?: any;
  toc?: any;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  children: Child[];
}

export interface Child {
  id: string;
  parentId: string;
  order: number;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}
// 喜欢
export interface Likes {
  id: string;
  parentId: string;
  order: number;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}