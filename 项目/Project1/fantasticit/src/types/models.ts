import { ArticleModelState } from "@/models/article";
import { KnowledgeModelState } from "@/models/Knowledge";
import { BrochureModelState } from "@/models/Brochure";

export interface IRootState {
    archives: any;
    article: ArticleModelState,
}

export interface KnowledgeState {
    Knowledge: KnowledgeModelState
}
export interface BrouchureState {
    Brochure: BrochureModelState
}

import { AboutModelState } from "@/models/about"
export interface IRootState {

    article: ArticleModelState;
    about: AboutModelState;
}
import { MessageModelState } from '@/models/Message'

export interface IRootState {
    article: ArticleModelState
}
// 留言板
export interface MessageState {
    Message: MessageModelState
}