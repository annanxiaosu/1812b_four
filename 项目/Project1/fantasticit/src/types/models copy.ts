import { ArticleModelState } from "@/models/article";
import { LanguageModelState } from "@/models/language";

export interface IRootStates{
    article: ArticleModelState,
    language: LanguageModelState
    loading: {
        global: boolean;
    }
}