
//文章标签
export interface articleTop {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
    articleCount: number;
}

//详情
export interface articleDetail {
    map: any;
    id: string;
    title: string;
    cover: string;
    summary: string;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    tags: Tag[];
}

interface Tag {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
    
}

//文章推荐
export interface articleRemond {
    id: string;
    title: string;
    cover?: string;
    summary?: string;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    category?: Category;
    tags: Category[];
}

interface Category {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
}

//文章分类
export interface articleClassif {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
    articleCount: number;
}
//文章标签
export interface articleTop {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
    articleCount: number;
}

//详情
export interface articleDetail {
    id: string;
    title: string;
    cover: string;
    summary: string;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    tags: Tag[];
}

interface Tag {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
}

//文章推荐
export interface articleRemond {
    id: string;
    title: string;
    cover?: string;
    summary?: string;
    content: string;
    html: string;
    toc: string;
    status: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    category?: Category;
    tags: Category[];
}

interface Category {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
}

//文章分类
export interface articleClassif {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
    articleCount: number;
}