import { useSelector, useDispatch, history} from "umi";
import React, { useEffect } from "react"
import { BrouchureState } from "@/types";
import "./index.less"


// 文章分类组件

const Index: React.FC = () => {

    const dispatch = useDispatch();

    const Brochure = useSelector((state: BrouchureState) => state.Brochure);

    useEffect(() => {
        dispatch({
            type: 'Brochure/getBrochureReading',
            payload: '',
        });
    }, [])

    return <div className="rightlist2" >
        <h4><b>文章分类</b></h4>
        {
            Brochure.BrochureReading.map((item, index) => {
                return (
                    <li key={index} onClick={()=>history.push(`/category/${item.value}`)}>
                        <span>{item.label}</span>
                        <span>共计 {item.articleCount} 篇文章</span>
                    </li>
                );
            })
        }
    </div>
}

export default Index;