import { useSelector, useDispatch, history} from "umi";
import React, { useEffect } from "react"
import { articleLabel} from "../../models/article_label"
import "./index.less"


    // 文章标签

const Index: React.FC = () => {

    const dispatch = useDispatch();


    const articleLabel = useSelector((state: articleLabel) => state.article_label);

    useEffect(() => {
        dispatch({
            type: "article_label/getArticleLeftTop",
            payload: ""
        })
    }, [])

    return <div className="rightlist2" >
        <h4><b>文章标签</b></h4>
        {
            articleLabel.article_left_top.map(item => {
                return <div key={item.id} onClick={() => {history.push(`/tag/${item.value}`) }}>{item.label}[{item.articleCount}]</div>
            })
        }
    </div>
}

export default Index;