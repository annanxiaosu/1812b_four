import React, { FC } from 'react'
import { useHistory } from "umi"
import { Ireading } from "../utils/know"
import moment from "moment";
import "../utils/fonts/iconfont.css";//图标
import "./rightTop.less"
import Thetime from "../store/modules/index";
interface Iitem {
    item: Ireading;
}
const RightTop: React.FC<Iitem> = (props) => {
    let item = props.item;
    // 计算时间
    let allTimes = Thetime(item.publishAt as string | number);
    const history = useHistory();
    function goDetail(item: Ireading){
        history.push('/readDetail', item.id);
    };
    return (
        <div className='top'>
            <ul>
                {/* 详情推荐阅读的渲染 */}
                <li onClick={() => goDetail(item)}>
                    <span>{item.title}</span> &emsp; <span>{allTimes}</span>
                </li>
            </ul>
        </div>
    )
}
export default RightTop
