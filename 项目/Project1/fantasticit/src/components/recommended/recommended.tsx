import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector, history } from 'umi';
import styles from './recommended.less'; // 等于启用了css-module
import React from 'react';

const classNames = require('classnames');

function Recommended() {
    let [page, setPage] = useState(1);
    const dispatch = useDispatch();
    const article = useSelector((state: IRootState) => state.article);

    useEffect(() => {
        dispatch({
            type: 'article/getArticleList',
            payload: page
        })
    }, [page]);

    return (
        <div className={classNames(styles.home_da)}>
            {/* 下侧列表 */}
            <div className={classNames(styles.bottomlist)} >
                <div className="main">
                    {
                        article.articleList && article.articleList.map((item: any, index: number) => {
                            return <div key={index} className={classNames(styles.box_da)}
                                onClick={() => history.push(`/category/article/${item.id}`)} >
                                <h2><a href="">{item.title}</a><span>超过一年前</span><span>后端</span></h2>

                                <div className={classNames(styles.box_left)}>
                                    {item.cover === null ? <div className={styles.box_kong}></div> : <img src={item.cover} alt="" />}
                                </div>
                                <div className={classNames (styles.box_right)}>
                                    <p className={classNames (styles.box_up)}>{item.summary}</p>
                                    <p className={classNames (styles.box_down)}><span>🤍{item.likes}</span>·<span>👁️{item.views}</span>·<span>※分享</span></p>
                                </div>
                            </div>
                        })
                    }
                </div>
            </div>
        </div>
    );
}
export default Recommended;