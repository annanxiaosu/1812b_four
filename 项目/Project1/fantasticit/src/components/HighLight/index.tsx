import React, { useEffect } from 'react';
import hljs from 'highlight.js';
import './index.less';
import { copyText } from '@/utils/copy';

// 复制组件

const HightLight: React.FC = props=>{
  console.log('6666666666666.......',props);
  
  const ref = React.createRef<HTMLDivElement>();
  useEffect(()=>{
    let blocks = ref.current!.querySelectorAll('pre code');
    blocks.forEach(block =>{
      hljs.highlightElement(block as HTMLElement);
      let copy = document.createElement('button');
      copy.innerText = '复制';
      copy.className = 'copy-code';
      copy.onclick = function(){
        copyText((block as HTMLElement).innerText);
      }
      block.parentNode!.insertBefore(copy, block);
    })
  }, [props.children])
 return <div ref={ref} className="markdown">{props.children}</div>   
}

export default HightLight;