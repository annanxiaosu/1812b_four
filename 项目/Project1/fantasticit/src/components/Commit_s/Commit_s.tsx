import React from 'react'
import { Popover, Button, Modal, Input, Form, message } from 'antd';
import './Commit_s.css'
import { useState } from 'react';
import { ChangeEvent } from 'react';

const Commit_s: React.FC = () => {
    // 弹出框
    const [visible, setVisible] = React.useState(false);
    const [confirmLoading, setConfirmLoading] = React.useState(false);
    const [text, settext] = useState('');
    const [about, setabout] = useState(false);

    // name
    const [name, setname] = useState('');
    // email
    const [email, setemail] = useState('');


    const showModal = () => {
        setVisible(true);

        let names = localStorage.getItem('name');
        console.log(names);

        if (names) {
            setVisible(false);
        } else {
            setVisible(true);
        }
    };

    const handleOk = () => {
        setConfirmLoading(true);
        setTimeout(() => {
            setVisible(false);
            setConfirmLoading(false);
        }, 10);
        localStorage.setItem('name', name);
        localStorage.setItem('email', email);
    };

    const handleCancel = () => {
        setTimeout(() => {
            setVisible(false);
            setConfirmLoading(false);
        }, 10);
    };

    // 全局提示
    const success = () => {
        message.success('评论成功，已提交审核');
        settext('')
    };

    //  输入框内容
    let edit = (e?: ChangeEvent<HTMLTextAreaElement>) => {
        if (e) {
            settext(e.target.value)
            e.target.value.length > 0 ? setabout(true) : setabout(false)
        }
    }

    // name 监听
    function getUser(e: React.ChangeEvent<HTMLInputElement>) {
        console.log(e.target.value);
        setname(e.target.value)
    }
    
    // email  监听
    function getEmail(e: React.ChangeEvent<HTMLInputElement>) {
        console.log(e.target.value);
        setemail(e.target.value)
    } 
    const content = (
        <ul className="SO1DW6R4gpGKoWYBL5g62">
            <li>😀</li><li>😃</li><li>😄</li><li>😁</li><li>😆</li>
            <li>😆</li><li>😅</li><li>😂</li><li>😉</li><li>😊</li>
            <li>😇</li><li>😍</li><li>😘</li><li>😗</li><li>😚</li>
            <li>😙</li><li>😋</li><li>😛</li><li>😜</li><li>😝</li>
            <li>😐</li><li>😑</li><li>😶</li><li>😏</li><li>😒</li>
            <li>😌</li><li>😔</li><li>😪</li><li>😴</li><li>😷</li>
            <li>😵</li><li>😎</li><li>😕</li><li>😟</li><li>😮</li>
            <li>😯</li><li>😲</li><li>😳</li><li>😦</li><li>😧</li>
            <li>😨</li><li>😰</li><li>😥</li><li>😢</li><li>😭</li>
            <li>😱</li><li>😖</li><li>😣</li><li>😞</li><li>😓</li>
            <li>😩</li><li>😫</li><li>😡</li><li>😡</li><li>😠</li>
            <li>😈</li><li>😺</li><li>😸</li><li>😹</li><li>😻</li>
            <li>😼</li><li>😽</li><li>🙀</li><li>😿</li><li>😾</li>
            <li>❤️</li><li>✋</li><li>✋</li><li>✌️</li><li>☝️</li><li>✊</li>
            <li>✊</li><li>🐵</li><li>🐱</li><li>🐮</li><li>🐭</li><li>☕</li>
            <li>♨️</li><li>⚓</li><li>✈️</li><li>⌛</li><li>⌚</li><li>☀️</li>
            <li>⭐</li><li>☁️</li><li>☔</li><li>⚡</li><li>❄️</li><li>✨</li>
            <li>🃏</li><li>🀄</li><li>☎️</li><li>☎️</li><li>✉️</li><li>✏️</li>
            <li>✒️</li><li>✂️</li><li>♿</li><li>⚠️</li><li>♈</li><li>♉</li>
            <li>♊</li><li>♋</li><li>♌</li><li>♍</li><li>♎</li><li>♏</li>
            <li>♐</li><li>♑</li><li>♒</li><li>♓</li><li>✖️</li><li>➕</li>
            <li>➖</li><li>➗</li><li>‼️</li><li>⁉️</li><li>❓</li><li>❔</li>
            <li>❕</li><li>❗</li><li>❗</li><li>〰️</li><li>♻️</li><li>✅</li><li>☑️</li>
            <li>✔️</li><li>❌</li><li>❎</li><li>➰</li><li>➿</li><li>〽️</li><li>✳️</li>
            <li>✴️</li><li>❇️</li><li>©️</li><li>®️</li><li>™️</li><li>ℹ️</li><li>Ⓜ️</li>
            <li>⚫</li><li>⚪</li><li>⬛</li><li>⬜</li><li>◼️</li><li>◻️</li><li>◾
                </li><li>◽</li><li>▪️</li><li>▫️</li>
        </ul>

    );
    return (
        <div className="commit_s">
            <div className="_13J4GEYHDRu5MejyviLpnU">
                <div className="N1BwmgIkZZbNoeNN_FRbs">
                    <div className="TaXtx7NU2ZhkaBRHq0-9o" onClick={showModal}>
                        <Modal title="请设置你的信息" visible={visible} onOk={handleOk} confirmLoading={confirmLoading} onCancel={handleCancel}>
                            <Form.Item
                                label="名称："
                                name="name"
                                rules={[{ required: true, message: '请输入您的称呼' }]}>
                                <Input value={name} onChange={(e) => getUser(e)} />
                            </Form.Item>
                            <Form.Item
                                label="邮箱："
                                name="email"
                                rules={[{ required: true, pattern: /^\w+@\w+\.(com|cn|net)$/, message: '输入合法邮箱地址，以便在收到回复时邮件通知' }]}>
                                <Input value={email} onChange={(e) => getEmail(e)} />
                            </Form.Item>
                        </Modal>
                        <div className="_3D8H76H-ABokj1dp7OR3a4" onClick={(e) => {
                            e.preventDefault()
                        }}></div>
                        <textarea value={text} placeholder="请输入评论内容（支持 Markdown）" className="ant-input" onChange={edit}></textarea>
                    </div>
                    <footer>
                        <Popover content={content} trigger="click" placement="bottom">
                            <Button><span className="_12avHmKjidVRHTuf5Qbd4K">
                                <svg viewBox="0 0 1024 1024" width="18px" height="18px">
                                    <path d="M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z" fill="currentColor">
                                    </path>
                                </svg>
                                <span>表情</span>
                            </span>
                            </Button>
                        </Popover>

                        <div>
                            <button type="button" className="ant-btn ant-btn-primary" onClick={success}>
                                <span>发 布</span>
                            </button>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    )
}

export default Commit_s;