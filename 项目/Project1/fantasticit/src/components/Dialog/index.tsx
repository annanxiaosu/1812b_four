import React, { useCallback, useEffect, useState } from 'react';
import { NavLink, useDispatch, useHistory, useSelector } from 'umi';
import { KeywordModelState } from "../../models/keyword"
import { Input } from 'antd';
import styles from "./indexx.less"

interface Props {
    keyword: KeywordModelState;
}

interface childProps {
    flag: boolean
    clickFlag: Function
}

const { Search } = Input;


const Index: React.FC<childProps> = props => {
    console.log(props);

    const dispatch = useDispatch();
    const history = useHistory();
    //接收父组件传过来的数据
    const { flag, clickFlag } = props;
    const [value, setValue] = useState('');

    const keyword = useSelector((state: Props) => state.keyword);


    const childClick = useCallback(
        () => {
            clickFlag(!flag);
        },
        [flag, clickFlag],
    )




    useEffect(() => {
        if (value !== "") {
            dispatch({
                type: 'keyword/getkeyWord',
                payload:value
            })
        }
        setValue("");
    }, [value]);

    function clickTitle(id:string) {
        childClick();
        history.push('/category/article/' + id);
    }

    return <div>
        {
            flag ?
                <div className={styles.layer}>
                    <div className={styles.layerr}>
                        <div className={styles.topp}>
                            <h1>文章搜索</h1>
                            <b onClick={childClick}>X</b>
                        </div>
                        <br />
                        <div className={styles.search}>
                            <Search placeholder="请输入需要查找的内容"
                                onSearch={value => {
                                    setValue(value)
                                }}
                                enterButton

                            />
                            {
                                keyword.listt.map(item => {
                                    return <li>
                                        <span onClick={() => {
                                            clickTitle(item.id)
                                        }} >{item.title}</span>
                                    </li>

                                })
                            }
                        </div>
                    </div>
                </div>
                : ""
        }
    </div>
}

export default Index




