import React from 'react'
import { RightListItem } from '../../types'
import { history } from 'umi'
import moment from '../moment'
import './RightList.css'

interface RightList {
  recommend: RightListItem[];
}
const RightList: React.FC<RightList> = (props) => {
  let { recommend } = props;
  return (
    <div className='tuijian'>
      <p>推荐阅读</p>
      {/* 推荐列表组件 */}
      <ul className="_1PytTgSelvgOTMZR4ckInB">
        {recommend.map((item, index) => {
          return (
            <li className="listItem" key={index}>
                <span onClick={() => {history.push(`/category/article/${item.id}`) }}><a>{item.title}</a></span> ·{moment(item.createAt).fromNow()}
            </li>
          );
        })}
      </ul>
    </div>
  )
}

export default RightList;