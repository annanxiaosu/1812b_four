function Thetime(Time: string | number) {
    let startTime = +new Date(Time);
    let endTime = +new Date();
    let thetimes = Math.floor(endTime - startTime);

    if (thetimes < 60 * 60 * 1000) {
        return Math.floor(thetimes / (60 * 1000)) + '分钟前';

    } else if (thetimes < 24 * 60 * 60 * 1000) {
        return Math.floor(thetimes / (60 * 60 * 1000)) + '小时前';

    } else if (thetimes < 30 * 24 * 60 * 60 * 1000) {
        return Math.floor(thetimes / (24 * 60 * 60 * 1000)) + '天前';

    } else if (thetimes < 12 * 30 * 24 * 60 * 60 * 1000) {
        return Math.floor(thetimes / (30 * 24 * 60 * 60 * 1000)) + '月前';

    } else {
        return Math.floor(thetimes / (12 * 30 * 24 * 60 * 60 * 1000)) + '年前';

    }
}
export default Thetime;