import React, { FC, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'umi';
import moment from 'moment';
import './readDetail.less';
import '@/utils/fonts/iconfont.css';
import { BackTop } from 'antd';
import RightList from '@/components/RightList/RightList';
import { IRootState } from '@/types';
//
import Thetime from "@/store/modules/index"

import ImageView from '@/components/ImageView';//图片放大

const ReadDetail: FC = () => {
  let id = history.state.state;
  console.log(history.state);

  const item1 = useSelector((state: any) => state);
  const article = useSelector((state: IRootState) => state.article);

  let item = item1.index.readDetailList;
  let detailReadList = [];

  if (typeof item.toc == 'string') {
    detailReadList = JSON.parse(item.toc);
  }

  //时间
  let dd = Thetime(item.publishAt as string | number);
  const [likes, setlikes] = useState(false);
  const dispatch = useDispatch();



  useEffect(() => {
    dispatch({
      type: 'article/getRecommend'
    })
    //详情数据
    dispatch({
      type: 'index/godetail',
      payload: { id: id },
    });
    //详情右边
    dispatch({
      type: 'article/getRecommend'
    });
    dispatch({
      type: 'index/readinglist',
      payload: { id: id }
    });
  }, []);

  console.log(item1.index.readList, '1');

  function changeFlag() {
    dispatch({
      type: 'index/changelikes',
      payload: { id: id, like: likes },
    });
    setlikes(!likes);
  }
  return (
    <div className="detail">
      <div className="detailBox">
        {/* 收藏 分享 */}
        <div className="ce">
          <i
            className={`iconfont icon-shoucang1 ${likes ? 'active' : ''}`}
            onClick={() => changeFlag()}
          >
            <span>{item.likes}</span>
          </i>
          <i className="iconfont icon-xinxi"></i>
          <i className="iconfont icon-fenxiang"></i>
        </div>
        <div className="mainDetail">
          <div className="left">
            <ImageView>
              <img src={item.cover} alt="" />
            </ImageView>
            <div className="con">
              <div>
                <h1>{item.title}</h1>
                <p className='p1'>
                  发布时间{moment(item.createAt).format('YYYY-MM-DD h:mm:ss')}
                  ·阅读量{item.views}
                </p>
              </div>
              <div dangerouslySetInnerHTML={{ __html: item.html }}></div>
              <p className="p2">
                发布于{moment(item.createAt).format('YYYY-MM-DD h:mm:ss')} |
                版权信息：非商用-署名-自由转载
              </p>
            </div>
            {/* <div className="markdown-body">{item.content}</div> */}

          </div>
          <div className="right">
            <div className="right_top">
              {/* <h2>推荐阅读</h2> */}
              <RightList recommend={article.recommend} />
            </div>
            <div className="right_bottom">
              <h2>目录</h2>
              {
                detailReadList && detailReadList.map((item: any) => {
                  return (
                    <ul key={item.id}>
                      {
                        item.level == "2" ? <li>{item.id}</li> : <li style={{ marginLeft: "20px" }}>{item.id}</li>
                      }
                    </ul>
                  );
                })
              }
            </div>
          </div>
        </div>
        <BackTop className="gotop">
          {/* DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> */}
          <div style={{
            height: 40,
            width: 40,
            lineHeight: '40px',
            borderRadius: '50%',
            backgroundColor: '#000000',
            color: '#fff',
            textAlign: 'center',
            fontSize: 16,
          }}>
            <i className="iconfont icon-arrow-up"></i>
          </div>
        </BackTop>
      </div>
    </div>
  );
};

export default ReadDetail;
