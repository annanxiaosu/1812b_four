import React from 'react';
import { IRootState, IArticleItem } from '@/types';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector, history } from 'umi';
import styles from './category/category.less'; // 等于启用了css-module
import { Carousel,BackTop } from 'antd';
import '@/utils/fonts/iconfont.css';

import Category from './category/category';
import RightList from '@/components/RightList/RightList';
import RightTagtwo from "../components/RightList_Tag_01"
import share from '@/components/Share';
const classNames = require('classnames');

export default function Home() {
  let [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const article = useSelector((state: IRootState) => state.article);

  useEffect(() => {
    dispatch({
      type: 'article/getArticleList',
      payload: page
    })
  }, [page]);

  useEffect(() => {
    dispatch({
      type: 'article/getRecommend'
    })
    dispatch({
      type: "archives/getarchives"
    })
    dispatch({
      type: "archives/readinglist"
    })
    dispatch({
      type: "archives/getlabel"
    });
  }, []);
  // 点击分享
  function shangArticle(e: React.MouseEvent, item: IArticleItem) {
    console.log('e...', e);
    e.stopPropagation();
    e.preventDefault();
    share(item);
  }


  //百度访问
  function onclickdetail(){
    window._hmt.push(['小楼又清风', '首页', '文章跳详情', '三月前','item']);
  }

  //访问次数
  useEffect(() => {
    // setTimeout(()=>{
    //   [...document.querySelectorAll(".article-item")].forEach(item => {
    //     item.click();
    //   })
    //   window.location.reload();
    // },5000)
  }, [])



  return (
    <div className={classNames(styles.home_da)}>
      <div className={classNames(styles.home)}>
        {/* 中间盒子 */}
        <div className={classNames(styles.middle)}>
          {/* 中间左边 */}
          <div className={classNames(styles.middle_left)}>
            {/* 轮播图 */}
            <div className={classNames(styles.slideshow)}>
              <Carousel autoplay>
                {
                  article.recommend && article.recommend.map((item, index) => {
                    if (item.id == 'db67efc4-3020-4ba9-95bb-8bb96a198b4a' || item.id == 'f92ffbce-5945-4e7a-9f66-28aaee7dc0ae') {
                      return <div>
                        <img src={item.cover} alt="" />
                        <div className={classNames(styles.zhezhaoceng)} onClick={() => history.push(`/category/article/${item.id}`)}>
                          <div className={classNames(styles.ceng)} >
                            <h2>{item.title}</h2>
                            <p className="article-item" onClick={() => onclickdetail()}>3个月前 · {item.views}阅读</p>
                          </div>
                        </div>
                      </div>
                    }
                  })
                }
              </Carousel>
            </div>
            {/* 下侧列表 */}
            <div className={classNames(styles.bottomlist)} >
              <div className="head">
                <Category />
                <div className="main">
                  {
                    article.articleList && article.articleList.map((item: any, index: number) => {
                      return <div
                        key={index}
                        className={classNames(styles.box_da)}
                        onClick={() => history.push(`/category/article/${item.id}`)} >
                        <h2>
                          <a href="">{item.title}</a>
                          <span>超过一年前</span>
                          <span>后端</span>
                        </h2>
                        <div className={classNames(styles.box_left)}>
                          {item.cover === null ? <div className={classNames(styles.box_kong)}></div> : <img src={item.cover} alt="" />}
                        </div>
                        <div className={classNames(styles.box_right)}>
                          <p className={classNames(styles.box_up)}>{item.summary}</p>
                          <p className={classNames(styles.box_down)}>
                            <span>🤍{item.likes}</span>·
                            <span>👁️{item.views}</span>·
                            <span onClick={e => shangArticle(e, item)}>※·分享</span>
                          </p>
                        </div>
                      </div>
                    })
                  }
                </div>
              </div>
            </div>
          </div>
          {/* 中间右边 */}
          <div className={classNames(styles.middle_right)}  >
            {/* 右侧列表 */}
            <RightList recommend={article.recommend} />
            <RightTagtwo />
          </div>
          <div>
            <BackTop>
              <div style={{
                height: 40,
                width: 40,
                lineHeight: '40px',
                borderRadius: '50%',
                backgroundColor: '#000000',
                color: '#fff',
                textAlign: 'center',
                fontSize: 16,
              }}>
                <i className="iconfont icon-arrow-up"></i>
              </div>
            </BackTop>
          </div>
        </div >
      </div >
    </div >
  );
}
