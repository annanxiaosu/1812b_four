import React from 'react'
import { NavLink } from 'umi'
import styles from './category.less'; // 等于启用了css-module
const classNames = require('classnames');

const category: React.FC = () => {
    return (
        <div className={classNames(styles.category)}>
            <NavLink to="/home" activeStyle={{ color: "red" }}> 所有</NavLink>
            <NavLink to="/category/fe" activeStyle={{ color: "red" }} >前端</NavLink>
            <NavLink to="/category/be" activeStyle={{ color: "red" }}>后端</NavLink>
            <NavLink to="/category/reading" activeStyle={{ color: "red" }}>阅读</NavLink>
            <NavLink to="/category/linux" activeStyle={{ color: "red" }}>Linux</NavLink>
            <NavLink to="/category/leetcode" activeStyle={{ color: "red" }}>LeetCode</NavLink>
            <NavLink to="/category/news" activeStyle={{ color: "red" }}>要闻</NavLink>
        </div>
    )
}
export default category
