import React, { useEffect, useState } from 'react';
import { IRouteComponentProps, useDispatch, useSelector } from 'umi'
import { BackTop } from 'antd';
import { IRootState } from '@/types';
import RightList from '@/components/RightList/RightList';
import { articleLabel } from "../../../models/article_label";
import ImageView from '@/components/ImageView';
import HightLight from '@/components/HighLight';
import styles from './artcle.less';
import '@/utils/fonts/iconfont.css';

const classNames = require('classnames');

const ArticleDetail: React.FC<IRouteComponentProps<{ id: string }>> = props => {
    let id = props.match.params.id;
    const dispatch = useDispatch();

    const article = useSelector((state: IRootState) => state.article);
    const { articleDetail } = useSelector((state: IRootState) => state.article)
    const articleLabel = useSelector((state: articleLabel) => state.article_label);
    useEffect(() => {
        dispatch({
            type: 'article/getArticleDetail',
            payload: id
        })
        dispatch({
            type: 'article/getRecommend',
            payload: '',
        });
    }, [])
    if (!Object.keys(articleDetail).length) {
        return null;
    }
    function click_rightlist(name: string, id: number) {
        // 获取整个楼层
        let floor_lou = [...document.querySelectorAll('.particulars___2Amus h2,h3')];
        // 计算点击属性到顶部距离
        let floor_num = floor_lou[id].offsetTop;
        // 跳转到相应位置
        window.scrollTo(0, floor_num - 20);

        // 获取右侧：列表
        let floor_list = [...document.querySelectorAll('.rightlist2___3DOyn .one_ji___umbjm,.two_ji___2-1Pj')];
        // 获取右侧：点击元素 、给样式
        floor_list[id].style = `background-color: #f8f8f8;`
    }

    // 滚动条、滚动函数
    window.onscroll = function () {
        //滚动距离
        let scrollTop = window.scrollY;
        // 获取楼层
        let floor_lou = [...document.querySelectorAll('.particulars___2Amus h2,h3')];
        //  获取楼层列表
        let floor_list = [...document.querySelectorAll('.rightlist2___3DOyn div div')];

        for (var i = 0; i < floor_lou.length; i++) {
            if (scrollTop >= floor_lou[i].offsetTop) {
                console.log(i);
                for (var j = 0; j < floor_list.length; j++) {
                    floor_list[j].style = `background-color: #f8f8f8;`
                }
            }
        }
    }

    // 详情列表右侧列表
    let right_list = JSON.parse(articleDetail.toc as string);
    console.log(right_list);
    return <div className={classNames(styles.home_da)} >
        <div className={classNames(styles.home)}>
            {/* 中间盒子 */}
            <div className={classNames(styles.middle)}>
                {/* 中间左边 */}
                <div className={classNames(styles.middle_left)}>
                    {/* 分类文章 */}
                    <ImageView>
                        <div className={classNames(styles.particulars)}>
                            {articleDetail.cover && <img src={articleDetail.cover} />}
                            {
                                <div className={classNames(styles.markdown)}>
                                    <HightLight>< div dangerouslySetInnerHTML={{ __html: articleDetail.html! }}></div></HightLight>
                                </div>
                            }
                        </div>
                    </ImageView>
                    <p style={{ textAlign: 'center', fontSize: '20px', height: '50px', lineHeight: '50px' }}>评论</p>
                    {/* 下侧列表 */}
                    <div className={classNames(styles.bottomlist3)} >
                        <textarea placeholder='请输入评论内容（支持 Markdown）'></textarea>
                        <p><span>😊表情</span><button>发布</button></p>
                    </div>
                </div>
                {/* 中间右边 */}
                <div className={classNames(styles.middle_right)}>
                    {/* 右侧列表 */}
                    <RightList recommend={article.recommend} />
                    <div className={classNames(styles.rightlist2)} >
                        <h4>目录</h4>
                        {
                            right_list.map((item: any, index: number) => {
                                if (item.level == 2) {
                                    return <div className={classNames(styles.one_ji)}
                                        onClick={() => click_rightlist(item.text, index)} >
                                        <div  >
                                            {item.text}
                                        </div>
                                    </div>
                                }
                                if (item.level == 3) {
                                    return <div className={classNames(styles.two_ji)}
                                        onClick={() => click_rightlist(item.text, index)}>
                                        <div >
                                            {item.text}
                                        </div>
                                    </div>
                                }
                            })
                        }
                    </div>
                </div>
            </div>
        </div>
        {/* 回到顶部 */}
        <BackTop>
            <div style={{
                height: 40,
                width: 40,
                lineHeight: '40px',
                borderRadius: '50%',
                backgroundColor: '#000000',
                color: '#fff',
                textAlign: 'center',
                fontSize: 16,
            }}>
                <i className="iconfont icon-arrow-up"></i>
            </div>
        </BackTop>
    </div >
}

export default ArticleDetail;