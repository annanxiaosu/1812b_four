import React from 'react'
import Category from './category';
import styles from './category.less'; // 等于启用了css-module
import { useEffect, useState } from 'react';
import { useDispatch, useSelector, history } from 'umi';
import { IRootState } from "@/types";
import RightList from '@/components/RightList/RightList';
import { articleLabel } from "../../models/article_label";
const classNames = require('classnames');

const news: React.FC = () => {
    const dispatch = useDispatch();
    const article = useSelector((state: IRootState) => state.article);
    // 文章标签
    const articleLabel = useSelector((state: articleLabel) => state.article_label);

    useEffect(() => {
        dispatch({
            type: 'article/getnewslist'
        })
        dispatch({
            type: 'article/getRecommend'
        })
        dispatch({
            type: "archives/getarchives"
        })
        dispatch({
            type: "archives/readinglist"
        })
        dispatch({
            type: "archives/getlabel"
        });
    }, []);

    return (
        <div className={classNames(styles.home_da)}>
            <div className={classNames(styles.home)}>
                {/* 中间盒子 */}
                <div className={classNames(styles.middle)}>
                    {/* 中间左边 */}
                    <div className={classNames(styles.middle_left)}>
                        {/* 分类文章 */}
                        <div className={classNames(styles.classify_title)}>
                            <p><span>要闻</span>分类文章</p>
                            <p>共搜到<span> {article.news_list[1]}</span>篇</p>
                        </div>
                        {/* 下侧列表 */}
                        <div className={classNames(styles.bottomlist2)} >
                            <Category />
                            {
                                article.news_list[0] && article.news_list[0].map((item: any, index: number) => {
                                    return <div key={index} className={classNames(styles.box_da)}
                                        onClick={() => history.push(`/category/article/${item.id}`)}>
                                        <h2><a href="">{item.title}</a><span>超过一年前</span><span>后端</span></h2>
                                        <div className={classNames(styles.box_left)}>
                                            <img src={item.cover} alt="" />
                                        </div>
                                        <div className={classNames(styles.box_right)}>
                                            <p className={classNames(styles.box_up)}>{item.summary}</p>
                                            <p className={classNames(styles.box_down)}><span>🤍{item.likes}</span>·<span>👁️{item.views}</span>·<span>※分享</span></p>
                                        </div>
                                    </div>
                                })
                            }
                        </div>
                    </div>

                    {/* 中间右边 */}
                    <div className={classNames(styles.middle_right)}  >
                        {/* 右侧列表 */}
                        <div className={classNames(styles.rightlist1)}>

                            <RightList recommend={article.recommend} />
                        </div>
                        <div className={classNames(styles.rightlist2)} >
                            <h4><b>文章标签</b></h4>
                            {
                                articleLabel.article_left_top.map(item => {
                                    return <div key={item.id} onClick={() => { history.push(`/tag/${item.value}`) }}>{item.label}[{item.articleCount}]</div>
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default news
