import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector, NavLink } from 'umi';
import { KnowledgeState } from '@/types';
import { IRootState } from '@/types';
import moment from '../../components/moment';
import RightList from '@/components/RightList/RightList';

const Brochure: React.FC = () => {
  let [page, setPage] = useState(1);
  // 引用useDispatch
  const dispatch = useDispatch();
  // 获取到Knowledge下的数据
  const Knowledge = useSelector((state: KnowledgeState) => state.Knowledge);
  // 获取到article的数据
  const article = useSelector((state: IRootState) => state.article);
  // 调用getKnowledge方法
  useEffect(() => {
    dispatch({
      type: 'Knowledge/getKnowledge',
      payload: page,
    });
  }, [page]);

  // 调用getRecommend的方法
  useEffect(() => {
    dispatch({
      type: 'article/getRecommend',
      payload: '',
    });
    
    // 调用getKnowledgeClassify的方法
    dispatch({
      type: 'Knowledge/getKnowledgeClassify',
      payload: '',
    });
  }, []);

  return (
    <div className="Brochure">
      <div className="container">
        <div className="_3tC3y9W6nEYVdAwGypNta9">
          <section className="_1r_Dp71aY9wU_PZOrRyGzl">
            <div>
              {/* 列表渲染 */}
              <div className="_3dE21Z8kljGaURC-u52HoH">
                {Knowledge.KnowledgeList.map((item, index) => {
                  return (
                    <div className="XifRPckEIhR-ii13RLWBw" key={index}>
                      <div>
                        <NavLink to={`/knowledge/${item.id}`}>
                          <header>
                            <div className="_1vA_Or4uXSUk_Q0pAjXNAk">
                              {item.title} <span>{moment(item.createAt).fromNow()}</span>
                            </div>
                            <div className="_2vFIdR7o0i5RPcwxmGqkn7">
                              <div
                                className="ant-divider ant-divider-vertical"
                                role="separator"
                              ></div>
                              <span className="_37XZHWLrCz-FDEOzkwvIHD"></span>
                            </div>
                          </header>
                          <main>
                            <div className="_2Cv_sMdeyVvYq6XC91OAQX">
                              <img src={item.cover} alt="cover" />
                            </div>
                            <div className="_1RUkQHMUEKR84DQvX4ZPGy">
                              <div className="uzxZQqwvdEFwe6mgNPeYD">
                                {item.summary}
                              </div>
                              <div className="_2-5WsQK_Q67xXsuDTBYLqO">
                                <span>
                                  <span
                                    role="img"
                                    aria-label="eye"
                                    className="anticon anticon-eye"
                                  >
                                    <svg
                                      viewBox="64 64 896 896"
                                      focusable="false"
                                      data-icon="eye"
                                      width="1em"
                                      height="1em"
                                      fill="currentColor"
                                      aria-hidden="true"
                                    >
                                      <path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path>
                                    </svg>
                                  </span>
                                  <span className="MxfDTpSSoWKL3RwXzbvFj">
                                    {item.views}
                                  </span>
                                </span>
                                <span className="aAj27t_TOL5PyHxn0uc08">·</span>
                                <span>
                                  <span>
                                    <span
                                      role="img"
                                      aria-label="share-alt"
                                      className="anticon anticon-share-alt"
                                    >
                                      <svg
                                        viewBox="64 64 896 896"
                                        focusable="false"
                                        data-icon="share-alt"
                                        width="1em"
                                        height="1em"
                                        fill="currentColor"
                                        aria-hidden="true"
                                      >
                                        <path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path>
                                      </svg>
                                    </span>
                                    <span className="MxfDTpSSoWKL3RwXzbvFj">
                                      分享
                                    </span>
                                  </span>
                                </span>
                              </div>
                            </div>
                          </main>
                        </NavLink>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </section>
          <aside className="_3yp3HpYTQik_UvNquTL2uQ _2dyoSl4ynuBmhwRO6bebOZ">
            <div className="sticky">
              <div className="ammbB8tpRTlzwGu9Q8GNE _3PpmFreY8xlC6Z2Y0lcyk-">

                <div className="ant-spin-nested-loading">
                  <div className="ant-spin-container">
                    <RightList recommend={article.recommend}>

                    </RightList>
                  </div>
                </div>
              </div>

              <div className="_3KFGvyCY0omWyEQOzioDDk">
                <div className="_2b0LQ_j7dN-JQtnGkqmrDz">
                  <span>文章分类</span>
                </div>
                <ul>
                  {Knowledge.KnowledgeClassifyList.map((item, index) => {
                    return (
                      <li className="fileItem" key={index}>
                        <a href="/category/fe">
                          <span>{item.label}</span>
                          <span>共计 {item.articleCount} 篇文章</span>
                        </a>
                      </li>
                    );
                  })}
                </ul>
              </div>
            </div>
          </aside>
        </div>
      </div>
    </div>
  );
};
export default Brochure;
