import { KnowledgeState } from '@/types';
import React, { useEffect, useState } from 'react';
import {
  IRouteComponentProps,
  useDispatch,
  useSelector,
  NavLink,
  useHistory,
} from 'umi';
import './know.css';
import moment from '../../../components/moment';
import { fomatTime } from '../../../utils/today';

const KnowDetail: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
  // 获取到跳详情时候的路由的id
  let id = props.match.params.id;
  // 引入useDispatch
  const dispatch = useDispatch();

  const Knowledge = useSelector((state: KnowledgeState) => state.Knowledge);
  // 调用getKnowledgeDetail的方法，id每次发生变化就执行一次
  useEffect(() => {
    dispatch({
      type: 'Knowledge/getKnowledgeDetail',
      payload: id,
    });
  }, [id]);
  function goToDetail() {
    props.history.push({
      pathname: `/knowledge/${id}/${
        Knowledge.know.children && Knowledge.know.children![0].id
        }`,
      query: {
        idd: id,
      },
    });
  }

  
  return (
    <>
      <div className="_17xg_Uj0Bv758wYxH8pUyB">
        <div className="_1t2rVgBPz8M9qwAr17sVyW"></div>
        {/* 头部 */}
        <div className="container">
          <div className="_3osL_ruI0cY_acDyk7fPBc">
            <div className="ant-breadcrumb">
              <span>
                <span className="ant-breadcrumb-link">
                  <a href="/knowledge">知识小册</a>
                </span>
                <span className="ant-breadcrumb-separator">/</span>
              </span>
              <span>
                <span className="ant-breadcrumb-link">Web 性能指南</span>
                <span className="ant-breadcrumb-separator">/</span>
              </span>
            </div>
          </div>
        </div>
        {/* 内容区 */}
        <div className="knowmain">
          <div className="container">
            <div className="_3tC3y9W6nEYVdAwGypNta9">
              {/* 左边内容区 */}
              <section className="_1r_Dp71aY9wU_PZOrRyGzl">
                <div className="_2UTUWlplOlKdNijjElaV1p">
                  <section className="_3TXwBqbIYTFMKAc3SRF0Pr">
                    <header>{Knowledge.know.title}</header>
                    <main className="QiGjUNedfkQdkJgVr40al">
                      <section className="_2KiElqNlNiOb400crvLXp_">
                        <div className="_3UiAcfZHhE3n0ISBQ26csd">
                          <img src={Knowledge.know.cover} alt="cover" />
                        </div>
                        <div className="_2Anl2UOlqXVqpsRdBHbjee">
                          <div>
                            <p className="_38mtgKp3kF0sl0CPqcXCr">
                              {Knowledge.know.title}
                            </p>
                            <p className="_2KiElqNlNiOb400crvLXp_">
                              {Knowledge.know.summary}
                            </p>
                            <p className="_2OCK4h4MhPhfi5hXsERjF6">
                              <span>{Knowledge.know.views} 次阅读</span>
                              <span className="_3f9UlP3M9s-ZwYvdDunr6v">·</span>
                              <span>{fomatTime(Knowledge.know.createAt!)}</span>
                            </p>
                            <div className="ga5BV4q_7-gSHbHryBWfx">
                              <button
                                type="button"
                                className="ant-btn ant-btn-primary"
                              >
                                {/* <NavLink to={`/knowledge/${id}/${Knowledge.know.children && Knowledge.know.children![0].id}`}> */}
                                <li onClick={() => goToDetail()}>开始阅读</li>
                                {/* </NavLink> */}
                              </button>
                            </div>
                          </div>
                        </div>
                      </section>
                      <ul>
                        {Knowledge.know.children
                          ? Knowledge.know.children!.map((item, index) => {
                            return (
                              <li key={index}>
                                <NavLink to={`/knowledge/${id}/${item.id}`}>
                                  <span>{item.title}</span>
                                  <span>
                                    {fomatTime(item.createAt)}
                                    <span
                                      role="img"
                                      aria-label="right"
                                      className="anticon anticon-right"
                                    >
                                      <svg
                                        viewBox="64 64 896 896"
                                        focusable="false"
                                        data-icon="right"
                                        width="1em"
                                        height="1em"
                                        fill="currentColor"
                                        aria-hidden="true"
                                      >
                                        <path d="M765.7 486.8L314.9 134.7A7.97 7.97 0 00302 141v77.3c0 4.9 2.3 9.6 6.1 12.6l360 281.1-360 281.1c-3.9 3-6.1 7.7-6.1 12.6V883c0 6.7 7.7 10.4 12.9 6.3l450.8-352.1a31.96 31.96 0 000-50.4z"></path>
                                      </svg>
                                    </span>
                                  </span>
                                </NavLink>
                              </li>
                            );
                          })
                          : null}
                      </ul>
                    </main>
                  </section>
                </div>
              </section>
              {/* 右边内容区 */}
              <aside className="_3yp3HpYTQik_UvNquTL2uQ">
                <div className="sticky _3TXwBqbIYTFMKAc3SRF0Pr">
                  <header>其他知识笔记</header>
                  <main>
                    <div className="_3dE21Z8kljGaURC-u52HoH">
                      {Knowledge.KnowledgeList &&
                        Knowledge.KnowledgeList.map((item, index) => {
                          return (
                            item.id !== id && (
                              <NavLink to={`/knowledge/${item.id}`} key={index}>
                                <div className="XifRPckEIhR-ii13RLWBw">
                                  <div>
                                    <header>
                                      <div className="_1vA_Or4uXSUk_Q0pAjXNAk">
                                        {item.title}
                                      </div>
                                      <div className="_2vFIdR7o0i5RPcwxmGqkn7">
                                        <div
                                          className="ant-divider ant-divider-vertical"
                                          role="separator"
                                        ></div>
                                        <span className="_37XZHWLrCz-FDEOzkwvIHD">
                                          {moment(item.createAt).fromNow()}
                                        </span>
                                      </div>
                                    </header>
                                    <main>
                                      <div className="_2Cv_sMdeyVvYq6XC91OAQX">
                                        <img src={item.cover} alt="cover" />
                                      </div>
                                      <div className="_1RUkQHMUEKR84DQvX4ZPGy">
                                        <div className="uzxZQqwvdEFwe6mgNPeYD">
                                          {item.summary}
                                        </div>
                                        <div className="_2-5WsQK_Q67xXsuDTBYLqO">
                                          <span>
                                            <span
                                              role="img"
                                              aria-label="eye"
                                              className="anticon anticon-eye"
                                            >
                                              <svg
                                                viewBox="64 64 896 896"
                                                focusable="false"
                                                data-icon="eye"
                                                width="1em"
                                                height="1em"
                                                fill="currentColor"
                                                aria-hidden="true"
                                              >
                                                <path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path>
                                              </svg>
                                            </span>
                                            <span className="MxfDTpSSoWKL3RwXzbvFj">
                                              {item.views}
                                            </span>
                                          </span>
                                          <span className="aAj27t_TOL5PyHxn0uc08">
                                            ·
                                          </span>
                                          <span>
                                            <span>
                                              <span
                                                role="img"
                                                aria-label="share-alt"
                                                className="anticon anticon-share-alt"
                                              >
                                                <svg
                                                  viewBox="64 64 896 896"
                                                  focusable="false"
                                                  data-icon="share-alt"
                                                  width="1em"
                                                  height="1em"
                                                  fill="currentColor"
                                                  aria-hidden="true"
                                                >
                                                  <path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path>
                                                </svg>
                                              </span>
                                              <span className="MxfDTpSSoWKL3RwXzbvFj">
                                                分享
                                              </span>
                                            </span>
                                          </span>
                                        </div>
                                      </div>
                                    </main>
                                  </div>
                                </div>
                              </NavLink>
                            )
                          );
                        })}
                    </div>
                  </main>
                </div>
              </aside>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default KnowDetail;
