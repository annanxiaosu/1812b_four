import { KnowledgeState } from '@/types';
import React, { useEffect, useState } from 'react';
import { IRouteComponentProps, useDispatch, useSelector, NavLink } from 'umi';
import styles from './knowid.css';
const classNames = require('classnames');
import { fomatTime } from '../../../utils/today';

const knowDetail: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
  // 获取调路由传的id
  let idd = props.location.query.idd;
  // 跳转第二个路由时候的id
  let id = props.match.params.id;
  const dispatch = useDispatch();
  const [activeId, setactiveId] = useState(id);
  const [like, setlike] = useState('likes');
  const Knowledge = useSelector((state: KnowledgeState) => state.Knowledge);
  // 调用getKnowledgeDetailList的方法
  useEffect(() => {
    dispatch({
      type: 'Knowledge/getKnowledgeLike',
      payload: { id: id, type: like },
    });
  }, []);
  useEffect(() => {
    dispatch({
      type: 'Knowledge/getKnowledgeDetailList',
      payload: id,
    });
  }, [id]);
  // 点击改变类名
  function setId(e: React.MouseEvent<HTMLLIElement, MouseEvent>) {
    console.log(e.target);
  }

  return (
    <>
      <div className={classNames(styles.home_da)}>
        <div className={classNames(styles.home)}>
          {/* 中间盒子 */}
          <div className={classNames(styles.middle)}>
            {/* 中间左边 */}
            <div className={classNames(styles.middle_left)}>
              <div>
                {/* 点赞，分享 */}
                <div className="_3DPqg1sEeVKyZvQGiGR7MK">
                  {/* 点赞 */}
                  <div className="opW4K-r7eKwtlgiMzfCHm">
                    <span className="ant-badge">
                      <div className="_1qU35tL8S-yk6hGSLgkF1n">
                        <svg
                          viewBox="0 0 1024 1024"
                          version="1.1"
                          xmlns="http://www.w3.org/2000/svg"
                          width="1em"
                          height="1em"
                        >
                          <path
                            d="M859.8 191.2c-80.8-84.2-212-84.2-292.8 0L512 248.2l-55-57.2c-81-84.2-212-84.2-292.8 0-91 94.6-91 248.2 0 342.8L512 896l347.8-362C950.8 439.4 950.8 285.8 859.8 191.2z"
                            fill="currentColor"
                          ></path>
                        </svg>
                      </div>
                      <sup
                        data-show="true"
                        className="ant-scroll-number ant-badge-count ant-badge-count-sm"
                        title="2"
                        style={{ backgroundColor: 'var(--primary-color)' }}
                      >
                        <span
                          className="ant-scroll-number-only"
                          style={{ transition: 'none 0s ease 0s' }}
                        >
                          <p className="ant-scroll-number-only-unit current">
                            {Knowledge.KnowLike.likes}
                          </p>
                        </span>
                      </sup>
                    </span>
                  </div>
                  {/* 评论 */}
                  <div className="opW4K-r7eKwtlgiMzfCHm">
                    <div className="oymssHUh0CsJHhRBTfMNy">
                      <svg
                        viewBox="0 0 1024 1024"
                        version="1.1"
                        xmlns="http://www.w3.org/2000/svg"
                        width="1em"
                        height="1em"
                      >
                        <path
                          d="M988.8 512a348.8 348.8 0 0 1-144.96 278.72v208.32l-187.84-131.52a387.2 387.2 0 0 1-56 4.8A408.64 408.64 0 0 1 384 811.84l-36.8-23.04a493.76 493.76 0 0 0 52.8 3.2 493.44 493.44 0 0 0 51.2-2.88c221.44-23.04 394.24-192 394.24-400a365.12 365.12 0 0 0-4.16-51.84 373.44 373.44 0 0 0-48.96-138.56l18.88 11.2A353.6 353.6 0 0 1 988.8 512z m-198.72-128c0-192-169.6-349.76-378.24-349.76h-24C192 47.04 33.92 198.72 33.92 384a334.08 334.08 0 0 0 118.4 253.12v187.52l86.08-60.48 66.24-46.4a396.16 396.16 0 0 0 107.52 16C620.48 734.08 790.08 576 790.08 384z"
                          fill="currentColor"
                        ></path>
                      </svg>
                    </div>
                  </div>
                  {/* 分享 */}
                  <div className="opW4K-r7eKwtlgiMzfCHm">
                    <span>
                      <div className="_2pkVHcLoLMRrby9QPbVsmv">
                        <svg
                          viewBox="0 0 1024 1024"
                          version="1.1"
                          xmlns="http://www.w3.org/2000/svg"
                          width="1em"
                          height="1em"
                        >
                          <path
                            d="M753.607 584.7c-48.519 0-91.596 23.298-118.66 59.315l-233.123-116.96c3.684-12.936 5.657-26.591 5.657-40.71 0-15.465-2.369-30.374-6.76-44.391l232.241-116.52c26.916 37.549 70.919 62.017 120.644 62.017 81.926 0 148.34-66.412 148.34-148.34 0-81.926-66.413-148.34-148.34-148.34-81.927 0-148.34 66.413-148.34 148.34 0 5.668 0.33 11.258 0.948 16.762l-244.945 122.892c-26.598-25.259-62.553-40.762-102.129-40.762-81.926 0-148.34 66.412-148.34 148.34s66.413 148.34 148.34 148.34c41.018 0 78.144-16.648 104.997-43.555l242.509 121.668c-0.904 6.621-1.382 13.374-1.382 20.242 0 81.927 66.412 148.34 148.34 148.34s148.34-66.413 148.34-148.34c-0.001-81.925-66.409-148.339-148.336-148.339l0 0z"
                            fill="currentColor"
                          ></path>
                        </svg>
                      </div>
                    </span>
                  </div>
                </div>
                {/* 分类文章 */}
                <section>
                  <div className={classNames(styles._1sHvTwlFk1qI5GYNQQYAOA)}>
                    <h1>{Knowledge.knowlist.title}</h1>
                    <p>
                      <span>
                        发布于
                        <time dateTime="2021-07-24 13:12:52">
                          {fomatTime(Knowledge.knowlist.createAt!)}
                        </time>
                      </span>
                      <span> • </span>
                      <span>阅读量{Knowledge.knowlist.views}</span>
                    </p>
                    {
                      <div className={classNames(styles.markdown)}>
                        <div
                          dangerouslySetInnerHTML={{
                            __html: Knowledge.knowlist.html!,
                          }}
                        ></div>
                      </div>
                    }
                  </div>
                  <p
                    style={{
                      textAlign: 'center',
                      fontSize: '20px',
                      height: '50px',
                      lineHeight: '50px',
                    }}
                  >
                    评论
                  </p>
                  {/* 下侧列表 */}
                  <div className={classNames(styles.bottomlist3)}>
                    <textarea placeholder="请输入评论内容（支持 Markdown）"></textarea>
                    <p>
                      <span>😊表情</span>
                      <button>发布</button>
                    </p>
                  </div>
                </section>
                {/* 中间右边 */}
                <div className={classNames(styles.middle_right)}>
                  {/* 右侧列表 */}
                  <div className={classNames(styles.rightlist1)}>
                    <div>
                      <p>{Knowledge.knowlist.title}</p>
                      <ul className={classNames(styles.KnowUl)}>
                        {Knowledge.know.children &&
                          Knowledge.know.children.map((item, index) => {
                            return (
                              <li
                                key={item.id}
                                className={
                                  item.id === activeId
                                    ? classNames(styles.knowActive)
                                    : ''
                                }
                                onClick={() => setactiveId(item.id)}
                              >
                                <NavLink
                                  to={`/knowledge/${idd}/${item.id}`}
                                  key={index}
                                  id={item.id}
                                >
                                  {item.title}
                                </NavLink>
                              </li>
                            );
                          })}
                      </ul>
                    </div>
                  </div>
                  <div className={classNames(styles.rightlist2)}>
                    <h3>目录</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default knowDetail;
