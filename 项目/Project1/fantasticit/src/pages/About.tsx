import React from 'react';
import styles from './index.less'; //等于启动了css-module
import { useDispatch, useSelector,history } from 'umi';
import { useEffect, useState } from 'react';
import { IRootState } from '@/types/models';
import { Modal, Input, Collapse, message, Button} from 'antd';
//引入组件
import Recommended from '../components/recommended/recommended';
import "../components/recommended/recommended.less"
//classname
const classNames = require('classnames');
const { TextArea } = Input;
const { Panel } = Collapse;
export default function AboutPage(this: any) {
  let [page, setPage] = useState(1);
  let [state, setState] = useState(true);
  //回复功能的状态
  let [flag, setFlag] = useState(false)
  let [reply, setReply] = useState(false);
  let [inputVisible,setInputVisible] = useState(false)
  //
  // const { comments, submitting, value } = this.state; 
  // let [submitting,setSubmitting] = useState('')
  let dispatch = useDispatch();
  //全部的类型
  let about = useSelector((state: IRootState) => state.about);
  useEffect(() => {
    dispatch({
      type: 'about/getAboutList',
      payload: page,
    });
  }, [page]);
  //阅读文字推荐
  let article = useSelector((state: IRootState) => state.article);
  useEffect(() => {
    dispatch({
      type: 'article/getRecommendList',
      payload: '',
    });
  }, []);
  //遮罩层
  const showModal = function () {
    setState(true);
  };
  const handleOk = (e: any) => {
    console.log(e);
    setState(false);
  };
  let handleCancel = (e: any) => {
    console.log(e);
    setState(false);
  };
  //点击发布
  const success = () => {
    message.success('评论成功，正在审核');
  };
  //点击收起
  useEffect(() => {
    console.log('收起')
  }, [flag])
  useEffect(() => {
    console.log('拉下')
  }, [reply])
  //点击回复
  const { Panel } = Collapse;
  function callback(key: any) {
    console.log(key);
  }
  const showInput = () => {
    // useState((inputVisible= true))
    console.log('1111')
  
  };

  return (
    <div className={styles.about}>
      <div className={styles.top}>
        <img
          src="https://wipi.oss-cn-shanghai.aliyuncs.com/2020-04-04/despaired-2261021_1280.jpg"
          alt=""
          className={styles.img}
        />
        <h2 className={styles.h2}>
          这世界只有一种英雄主义，就是在看清了生活的真相之后，依然热爱生活。
        </h2>
      </div>
      <div className={styles.main}>
        <p className={styles.com}>评论</p>
        <div className={styles.publish}>
          <textarea
            className={styles.aboutContent}
            placeholder="请输入评论内容 (支持Markdown)"
          ></textarea>
          <div>
            <div>
              <div className={styles.low}>
                <span className={styles.express} onClick={showModal}>😊表情</span>
                <Button onClick={success} className={styles.btn} >发布</Button>
              </div>
              <Modal
                visible={state}
                title="请设置你的信息"
                onOk={handleOk}
                onCancel={handleCancel}
              >
                <p className={styles.user}>
                  <span className={styles.active}>*</span>名称:{' '}
                  <input className={styles.aboutInput}></input>
                </p>
                <p className={styles.user}>
                  <span className={styles.active}>*</span> 邮箱:
                  <input className={styles.aboutInput}></input>
                </p>
              </Modal>
            </div>
          </div>
        </div>
        <section>
          {about.aboutList.map((item: { id: React.Key | null | undefined; name: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined; content: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined; userAgent: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined; }) => {
            return (
              <li key={item.id} className={styles.li}>
                <div>
                  <span className={styles.span}>F</span>
                  <span>{item.name}</span>
                </div>
                <p>{item.content}</p>                

                 <footer
                                      style={{
                                        fontSize: '.8rem',
                                        paddingLeft: '40px',
                                      }}
                                    >
                                      <div style={{fontSize:'12px'}}>
                                        <span>{item.userAgent}</span>
                                       
                                        <span
                                          style={{ paddingLeft: '5px' }}
                                          onClick={() => {
                                            setReply((reply = true));
                                          }}
                                        >
                                          <span
                                            role="img"
                                            aria-label="message"
                                            className={styles.anticonMessage}
                                          >
                                            <svg
                                              viewBox="64 64 896 896"
                                              focusable="false"
                                              data-icon="message"
                                              width="1em"
                                              height="1em"
                                              fill="currentColor"
                                              aria-hidden="true"
                                            >
                                              <path d="M464 512a48 48 0 1096 0 48 48 0 10-96 0zm200 0a48 48 0 1096 0 48 48 0 10-96 0zm-400 0a48 48 0 1096 0 48 48 0 10-96 0zm661.2-173.6c-22.6-53.7-55-101.9-96.3-143.3a444.35 444.35 0 00-143.3-96.3C630.6 75.7 572.2 64 512 64h-2c-60.6.3-119.3 12.3-174.5 35.9a445.35 445.35 0 00-142 96.5c-40.9 41.3-73 89.3-95.2 142.8-23 55.4-34.6 114.3-34.3 174.9A449.4 449.4 0 00112 714v152a46 46 0 0046 46h152.1A449.4 449.4 0 00510 960h2.1c59.9 0 118-11.6 172.7-34.3a444.48 444.48 0 00142.8-95.2c41.3-40.9 73.8-88.7 96.5-142 23.6-55.2 35.6-113.9 35.9-174.5.3-60.9-11.5-120-34.8-175.6zm-151.1 438C704 845.8 611 884 512 884h-1.7c-60.3-.3-120.2-15.3-173.1-43.5l-8.4-4.5H188V695.2l-4.5-8.4C155.3 633.9 140.3 574 140 513.7c-.4-99.7 37.7-193.3 107.6-263.8 69.8-70.5 163.1-109.5 262.8-109.9h1.7c50 0 98.5 9.7 144.2 28.9 44.6 18.7 84.6 45.6 119 80 34.3 34.3 61.3 74.4 80 119 19.4 46.2 29.1 95.2 28.9 145.8-.6 99.6-39.7 192.9-110.1 262.7z"></path>
                                            </svg>
                                          </span>
                                          回复
                                        </span>
                                       
                                      </div>
                                      <div
                                        className={styles.revert}
                                        style={{
                                          opacity: 1,
                                          height: '180px',
                                          overflow: 'hidden',
                                          display: reply ? 'block' : 'none',
                                        }}
                                      >
                                        <div
                                          className={styles.revertIpt}
                                          style={{ paddingTop: '16px' }}
                                        >
                                          <div className={styles.revertI}>
                                            <div
                                              className={styles.revertII}
                                              style={{ position: 'relative' }}
                                            >
                                              <textarea
                                                placeholder={
                                                  '  回复' + item.name
                                                }
                                                className={styles.aboutout}
                                                style={{
                                                  height: '98px',
                                                  minHeight: '98px',
                                                  maxHeight: '186px',
                                                  resize: 'none',
                                                  overflowY: 'hidden',
                                                }}
                                              >1212</textarea>
                                              <footer>
                                                <span
                                                  className={styles.expression}
                                                >
                                                  <svg
                                                    viewBox="0 0 1024 1024"
                                                    width="18px"
                                                    height="18px"
                                                  >
                                                    <path
                                                      d="M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z"
                                                      fill="currentColor"
                                                    ></path>
                                                  </svg>

                                                </span>
                                                <div>
                                                  <button
                                                    type="button"
                                                    className={styles.antBtnSm}
                                                    style={{
                                                      marginRight: '16px',
                                                    }}
                                                    onClick={() => {
                                                      setReply((reply = false));
                                                    }}
                                                  >
                                                    <span className={styles.btn1}>收起</span>
                                                  </button>
                                                  <button
                                                    type="button"
                                                    className={styles.antBtnSm}
                                                    style={{
                                                      borderColor: '#d9d9d9',
                                                      color: 'rgba(0,0,0,0.85)',
                                                      background: '#f5f5f5',
                                                    }}
                                                  >
                                                    <Button  className={styles.btn1}  onClick={showInput}> 发布 </Button>
                                                  </button>
                                                </div>
                                              </footer>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </footer>
              </li>
  );
})}
        </section >
      </div >
      <p className={styles.com}>推荐阅读</p>
      <div className={styles.aboutI}>
       <Recommended />
      </div>
       
    </div >
  );
}
