import React from 'react'
import './Message.css'
// 引入类型
import { IRootState, MessageState } from '@/types'
import { useDispatch, useSelector } from 'umi'
import { useEffect, useState } from 'react'
import { Pagination } from 'antd';
import monent from '../components/moment';
import { NavLink } from 'umi';
import Commit_s from '../components/Commit_s/Commit_s'
// 返回顶部
import { BackTop } from 'antd';
import { useRef } from 'react'
//下面组件
import Recommended from '../components/recommended/recommended';

const Message: React.FC = () => {
    const [page, setpage] = useState(1)
    const dispatch = useDispatch();
    const Message = useSelector((state: MessageState) => state.Message);
    const article = useSelector((state: IRootState) => state.article);
    const ref = useRef<HTMLDivElement>(null);

    useEffect(() => {
        dispatch({
            type: 'Message/getMessage',
            payload: page,
        })
    }, [page])

    useEffect(() => {
        dispatch({
            type: 'article/getRecommend'
        })
    }, []);
    // 分页
    function onmessage(page: number) {
        console.log(page);
        setpage(page)
    }

    // 回复
    function getreplys(index: number) {
        console.log(index, '-----', ref);
    }

    return (
        <div className='message'>
            {/* 头部留言板 */}
            <div className="container_ms">
                <div className="container_mess">
                    <h2>留言板</h2>
                    <p>
                        <strong>[ 请勿灌水 🤖 ]</strong>
                    </p>
                </div>
            </div>
            <div className="comment_on_reading">
                {/* 评论 */}
                <div className="comment">
                    <p className="resd">评论</p>
                    <Commit_s />
                    {/* 评论区域 */}
                    {
                        Message.messagelist.map((item, index) => {
                            return <div className="_1f7ebMJKow4NcuDVTBnvOc _3G7_JYLS4EIxTgILiJm_A5" key={index} ref={ref}>

                                <header>
                                    <span className="ant-avatar ant-avatar-circle">
                                        <span className="ant-avatar-string">{item.name.substring(0, 1)}</span>
                                    </span>
                                    <span className="_3xprCsLyoqZkArtvYiuAJU">
                                        <strong>{item.name}</strong>
                                    </span>
                                </header>
                                <main>
                                    <div dangerouslySetInnerHTML={{ __html: item.content }}></div>
                                </main>
                                <footer>
                                    <div className="_14zvEeg141WRt1wf5S37nf">
                                        <span className="sps">{item.userAgent}</span>
                                        <time dateTime="2021-08-16 02:31:45">{monent(item.updateAt).fromNow()}</time>
                                        <span className="_2Mfm5YZlUu-Qe7c9dPeV7w">
                                            <span role="img" aria-label="message" className="anticon anticon-message">
                                                <svg viewBox="64 64 896 896" focusable="false" data-icon="message" width="1em" height="1em" fill="currentColor" aria-hidden="true">
                                                    <path d="M464 512a48 48 0 1096 0 48 48 0 10-96 0zm200 0a48 48 0 1096 0 48 48 0 10-96 0zm-400 0a48 48 0 1096 0 48 48 0 10-96 0zm661.2-173.6c-22.6-53.7-55-101.9-96.3-143.3a444.35 444.35 0 00-143.3-96.3C630.6 75.7 572.2 64 512 64h-2c-60.6.3-119.3 12.3-174.5 35.9a445.35 445.35 0 00-142 96.5c-40.9 41.3-73 89.3-95.2 142.8-23 55.4-34.6 114.3-34.3 174.9A449.4 449.4 0 00112 714v152a46 46 0 0046 46h152.1A449.4 449.4 0 00510 960h2.1c59.9 0 118-11.6 172.7-34.3a444.48 444.48 0 00142.8-95.2c41.3-40.9 73.8-88.7 96.5-142 23.6-55.2 35.6-113.9 35.9-174.5.3-60.9-11.5-120-34.8-175.6zm-151.1 438C704 845.8 611 884 512 884h-1.7c-60.3-.3-120.2-15.3-173.1-43.5l-8.4-4.5H188V695.2l-4.5-8.4C155.3 633.9 140.3 574 140 513.7c-.4-99.7 37.7-193.3 107.6-263.8 69.8-70.5 163.1-109.5 262.8-109.9h1.7c50 0 98.5 9.7 144.2 28.9 44.6 18.7 84.6 45.6 119 80 34.3 34.3 61.3 74.4 80 119 19.4 46.2 29.1 95.2 28.9 145.8-.6 99.6-39.7 192.9-110.1 262.7z"></path>
                                                </svg>
                                            </span>
                                            <span onClick={() => getreplys(index)}>回复</span>
                                        </span>
                                    </div>
                                    <div className="_nbvryeiujk">
                                        <div className="_1oweEwxuizpZY8B_S3-JXP">
                                            <div className="N1BwmgIkZZbNoeNN_FRbs">
                                                <div className="TaXtx7NU2ZhkaBRHq0-9o">
                                                    <textarea placeholder="回复" className="ant-input">
                                                    </textarea>
                                                </div>
                                                <footer>
                                                    <span className="_12avHmKjidVRHTuf5Qbd4K">
                                                        <svg viewBox="0 0 1024 1024" width="18px" height="18px">
                                                            <path d="M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z" fill="currentColor">

                                                            </path>
                                                        </svg>
                                                        <span>表情</span>
                                                    </span>
                                                    <div>
                                                        <button type="button" className="ant-btn ant-btn-sm">
                                                            <span>收 起</span>
                                                        </button>
                                                        <button type="button" className="ant-btn ant-btn-primary ant-btn-sm">
                                                            <span>发 布</span>
                                                        </button>
                                                    </div>
                                                </footer>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="_14zvEeg141WRt1wf5S37nf_children">
                                        {
                                            item.children ? item.children.map((item, index) => {
                                                return <div key={index} className="_12545nrbvufhj">
                                                    <header>
                                                        <span className="ant-avatar ant-avatar-circle">
                                                            <span className="ant-avatar-string">C</span>
                                                        </span>
                                                        <span className="_3xprCsLyoqZkArtvYiuAJU">
                                                            <strong>{item.name}</strong>
                                                            回复
                                                            <strong>{item.replyUserName}</strong>
                                                        </span>
                                                    </header>
                                                    <main>
                                                        <div dangerouslySetInnerHTML={{ __html: item.content }}></div>
                                                    </main>
                                                    <footer>
                                                        <div className="_14zvEeg141WRt1wf5S37nf">
                                                            <span>{item.userAgent}</span>
                                                            <time>{monent(item.createAt).fromNow()}</time>
                                                            <span className="_2Mfm5YZlUu-Qe7c9dPeV7w">
                                                                <span role="img" aria-label="message" className="anticon anticon-message">
                                                                    <svg viewBox="64 64 896 896" focusable="false" data-icon="message" width="1em" height="1em" fill="currentColor" aria-hidden="true">
                                                                        <path d="M464 512a48 48 0 1096 0 48 48 0 10-96 0zm200 0a48 48 0 1096 0 48 48 0 10-96 0zm-400 0a48 48 0 1096 0 48 48 0 10-96 0zm661.2-173.6c-22.6-53.7-55-101.9-96.3-143.3a444.35 444.35 0 00-143.3-96.3C630.6 75.7 572.2 64 512 64h-2c-60.6.3-119.3 12.3-174.5 35.9a445.35 445.35 0 00-142 96.5c-40.9 41.3-73 89.3-95.2 142.8-23 55.4-34.6 114.3-34.3 174.9A449.4 449.4 0 00112 714v152a46 46 0 0046 46h152.1A449.4 449.4 0 00510 960h2.1c59.9 0 118-11.6 172.7-34.3a444.48 444.48 0 00142.8-95.2c41.3-40.9 73.8-88.7 96.5-142 23.6-55.2 35.6-113.9 35.9-174.5.3-60.9-11.5-120-34.8-175.6zm-151.1 438C704 845.8 611 884 512 884h-1.7c-60.3-.3-120.2-15.3-173.1-43.5l-8.4-4.5H188V695.2l-4.5-8.4C155.3 633.9 140.3 574 140 513.7c-.4-99.7 37.7-193.3 107.6-263.8 69.8-70.5 163.1-109.5 262.8-109.9h1.7c50 0 98.5 9.7 144.2 28.9 44.6 18.7 84.6 45.6 119 80 34.3 34.3 61.3 74.4 80 119 19.4 46.2 29.1 95.2 28.9 145.8-.6 99.6-39.7 192.9-110.1 262.7z">
                                                                        </path>
                                                                    </svg>
                                                                </span>
                                                                <span onClick={() => getreplys(index)}>回复</span>
                                                            </span>
                                                        </div>
                                                    </footer>
                                                    <div className="_nbvryeiujk">
                                                        <div className="_1oweEwxuizpZY8B_S3-JXP">
                                                            <div className="N1BwmgIkZZbNoeNN_FRbs">
                                                                <div className="TaXtx7NU2ZhkaBRHq0-9o">
                                                                    <textarea placeholder="回复" className="ant-input">
                                                                    </textarea>
                                                                </div>
                                                                <footer>
                                                                    <span className="_12avHmKjidVRHTuf5Qbd4K">
                                                                        <svg viewBox="0 0 1024 1024" width="18px" height="18px">
                                                                            <path d="M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z" fill="currentColor">

                                                                            </path>
                                                                        </svg>
                                                                        <span>表情</span>
                                                                    </span>
                                                                    <div>
                                                                        <button type="button" className="ant-btn ant-btn-sm">
                                                                            <span>收 起</span>
                                                                        </button>
                                                                        <button type="button" className="ant-btn ant-btn-primary ant-btn-sm">
                                                                            <span>发 布</span>
                                                                        </button>
                                                                    </div>
                                                                </footer>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            }) : ""
                                        }
                                    </div>
                                </footer>
                            </div>
                        })
                    }
                    <div className="page">
                        <Pagination defaultCurrent={1} total={Message.messagelength} pageSize={6} onChange={(page) => onmessage(page)} />
                    </div>
                    <p className="resd">推荐阅读</p>
                    {/* 推荐阅读 */}
                        <Recommended />
                </div>
                <>
                    <BackTop />
                    <strong className="site-back-top-basic"></strong>
                </>
            </div>
        </div >
    )
}
export default Message