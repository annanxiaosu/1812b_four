import React, { useEffect, useState } from 'react';
import './tag.less';
import { useDispatch, useSelector, useHistory, IRouteComponentProps } from 'umi';
import { articleLabel } from "../../models/article_label"
import { articleDetail } from '@/types/article_label';
import styles from '../category/category.less';
import Right_Tag from "../../components/RightList_Tag"
import RightList from '@/components/RightList/RightList';
import { IRootState} from '@/types';
import { BackTop} from 'antd';



const tag: React.FC<IRouteComponentProps<{ id: string }>>=props=> {
    console.log(props.match.params.id);
    let history = useHistory();
    const [value, setValue] = useState(`${props.match.params.id}`);
    const dispatch = useDispatch()
    const articleLabel = useSelector((state:articleLabel) => state.article_label);

    const article = useSelector((state: IRootState) => state.article);

    console.log(articleLabel);
    
    useEffect(() => {
        dispatch({
            type: "archives/getarchives"
        })
        dispatch({
            type: "archives/readinglist"
        })
        dispatch({
            type: "archives/getlabel"
        });
        dispatch({
            type: 'article/getRecommend'
        })
    }, []);
    
    

    useEffect(() => {
        dispatch({
            type: "article_label/getArticleLeftTop"
        })
        dispatch({
            type: "article_label/getArticleRmond"
        })
        dispatch({
            type: "article_label/getArticleClassif"
        })
    }, [])

    useEffect(() => {
        dispatch({
            type: "article_label/getArticleDetail",
            payload:value
        })
    }, [value])




    return <div className="tag">
        <div className='tag_wrap'>
            <div className="tag_left">
                <div className="tag_left_one">

                    <p>
                        与 <span>{value}</span> 标签有关的文章
                    </p>

                    <p>
                        共搜索到 <span>{articleLabel.articleDetail[1]}</span> 篇
                    </p>
                </div>
                <div className="tag_left_two">
                    <h4><b>文章标签</b></h4>
                    {
                        articleLabel.article_left_top.map(item => {
                            return <div key={item.id}
                                onClick={() => {
                                    setValue(item.value)
                                }}
                            >{item.label}[{item.articleCount}]</div>
                        })
                    }
                </div>
                <div className="tag_left_three">
                    {
                        articleLabel.articleDetail[0] && articleLabel.articleDetail[0].map((item: articleDetail) => {
                            return <div key={item.id} className={styles.box_daa}
                                onClick={() => {
                                    history.push(`/category/article/${item.id}`)
                                }}>
                                <h2><a href="">{item.title}</a><span>超过一年前</span><span>后端</span></h2>
                                <div className={styles.box_left}>
                                    <img src={item.cover} alt="" />
                                </div>
                                <div className={styles.box_right}>
                                    <p className={styles.box_up}>{item.summary}</p>
                                    <p className={styles.box_down}><span>🤍{item.likes}</span>·<span>👁️{item.views}</span>·<span>※分享</span></p>
                                </div>
                            </div>
                        })
                    }
                </div>
            </div>
            <div className="tsg_right">
                {/* 右侧列表 */}
                <RightList recommend={article.recommend}/>

                {/* 文章分类 */}
                <Right_Tag />
            </div>
            <BackTop />
        </div>
    </div>
}

export default tag

