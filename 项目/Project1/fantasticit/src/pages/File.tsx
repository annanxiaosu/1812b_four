import React, { useEffect } from 'react' 
import { useSelector, useDispatch, useHistory } from "umi";
import styles from "./File.less"; //样式 
import { BackTop } from 'antd';//回到顶部
import RightList from '@/components/RightList/RightList';
import { IRootState } from '@/types';
import RightList_Tag from '@/components/RightList_Tag';
// 归档页面
const file: React.FC = () => {
    let History = useHistory();
    const state = useSelector((state: { archives: any }) => state.archives);
    const article = useSelector((state: IRootState) => state.article);

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch({
            type: 'article/getRecommend'
        })
        dispatch({
            type: "archives/getarchives"
        })
        dispatch({
            type: "archives/readinglist"
        })
        dispatch({
            type: "archives/getlabel"
        });
    }, []);
    let month: Array<string> = [];
    if (state.keys.length > 0) {
        month = Object.keys(state.list[state.keys[0]])
    }
    console.log(state, 'file');
    return <div className={styles.big}>
        <div className={styles.bigOne}>
            <div className={styles.bigOneCenter}>
                <section className={styles.sec}>
                    <div className={styles.top}>
                        <span>归档</span>
                        <span>共计33篇</span>
                    </div>
                    {/* 数据渲染 2021 */}
                    {
                        state.keys.map((item: string, index: number) => {
                            return <h1 className={styles.h1} key={index}>{item}
                                {
                                    (Object.keys(state.list[state.keys[index]])).map((item1, index1) => {
                                        return <ul className={styles.p} key={index1}>{item1}
                                            {
                                                (state.list[state.keys[index]][month[index1]]) && (state.list[state.keys[index]][month[index1]]).map((item2: any, index2: number) => {
                                                    return <li className={styles.p1} key={index2} onClick={() => {
                                                        History.push(`/readDetail`, item2.id)
                                                    }}>{item2.title}</li>
                                                })
                                            }
                                        </ul>
                                    })
                                }
                            </h1>
                        })
                    }
                </section>
            </div>

            <div className={styles.right}>
                {/* 推荐阅读渲染 */}
                <RightList recommend={article.recommend} />
                {/* 文章标签渲染 */}
                <RightList_Tag />
            </div>
            {/* 回到顶部 */}
            <BackTop className="gotop">
                <div style={{
                    height: 40,
                    width: 40,
                    lineHeight: '40px',
                    borderRadius: '50%',
                    backgroundColor: '#000000',
                    color: '#fff',
                    textAlign: 'center',
                    fontSize: 16,
                }}>
                    <i className="iconfont icon-arrow-up"></i>
                </div>
            </BackTop>
        </div>
    </div>
}
export default file
