import React, { useCallback, useEffect, useState, useMemo } from 'react';
import { NavLink, useHistory, useSelector, useIntl, setLocale, useDispatch } from 'umi';
import './index.css';

import Dialog from "../components/Dialog"
import { Select, Spin } from 'antd';
import { IRootStates } from '@/types';
import Theme from '@/components/Theme';

const WrapDialog = React.memo(Dialog);//不想让props更新的话

const Layouts: React.FC = (props) => {
  let [pathname, usepathname] = useState([
    { path: "/home", name: 'menu.artilce' },
    { path: "/File", name: 'menu.archives' },
    { path: "/Knowledge", name: 'menu.knowledge' },
    { path: "/Message", name: 'menu.message' },
    { path: "/About", name: 'menu.about' }
  ])
  const history = useHistory();
  const intl = useIntl()
  const dispatch = useDispatch()
  const [bgc_show, setbgc_show] = useState(false);



  const { global, language } = useSelector((state: IRootStates) => {
    return {
      global: state.loading.global,
      language: state.language
    }
  })

  useEffect(() => {
    setLocale(language.locale, false);
  }, [language.locale]);

  function changeLocale(locale: string) {
    dispatch({
      type: 'language/save',
      payload: { locale }
    });
  }




  // 模糊搜索父传子
  const [flag, setFlag] = useState(false);
  const clickFlag = useCallback((t:boolean) => { setFlag(t); },[])
  
  return (
    <>
      {global && <div className="loading"></div>}

      {/* 公共头部 */}
      <header className="_3qxGtLoeoc9_cTrMop-zTn">
        <div className="_3M_Iyty-_u6Mq1VeSk14Rq">
          <div className="container">
            <div className="_3U9PAhHDbA5NYPGpvj3gix">
              <a href="/">
                <img src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png" alt="" />
              </a>
            </div>
            <div className="_1m0pVz8iNOboGDqVTeBfmy">
              <div className="_2-i62DvrXpASKd_zBrhHUV">
              </div>
              <div className="_2-i62DvrXpASKd_zBrhHUV">
              </div>
              <div className="_2-i62DvrXpASKd_zBrhHUV"></div></div>
            <nav className=""><ul><li className="_1KU4i5t4zJjAhq0ZlikqlG"></li>
              {
                pathname.map((item, index) => {
                  return <li key={index}>
                    <NavLink to={item.path} activeStyle={{color:'red'}}>{intl.formatMessage({
                      id: item.name
                    })}</NavLink>
                  </li>
                })
              }
              {/* 搜索框 */}
              <li className="_38OmGXMfjYo_xu-5O1rJEK">
                <span role="img" aria-label="search"
                  className="anticon anticon-search">
                  <svg onClick={() => { setFlag(true), history.push(`${history.location.pathname}`)}
                  }
                    viewBox="64 64 896 896"
                    focusable="false" data-icon="search"
                    width="1em" height="1em" fill="currentColor"
                    aria-hidden="true">

                    <path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0011.6 0l43.6-43.5a8.2 8.2 0 000-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z">
                    </path>
                  </svg>
                </span>
              </li>
              {/* 小太阳 */}
              <li className="_38OmGXMfjYo_xu-5O1rJEK" onClick={() => { setbgc_show(!bgc_show) }}>
                <Theme></Theme>
              </li>
              <li className="_38OmGXMfjYo_xu-5O1rJEK">
                <div className="ant-dropdown-trigger">

                  {/* 中英文切换 */}
                  <Select value={language.locale} onChange={(e: string) => changeLocale(e)}>{
                    language.locales.map(item => {
                      return <Select.Option key={item.locale} value={item.locale}>{item.label}</Select.Option>
                    })
                  }
                  </Select>
                </div>
              </li>
            </ul>
            </nav>
          </div>
        </div>
      </header>
      {/* 路由区域 */}
      <div className="main">{props.children}</div>
      {/* 公共底部 */}
      <footer className="_3SL8bXyPeDS8XR5EoRJ31s">
        <ul className="_1-uVZKG0Zi-XehmZOgdOtA">
          <li>
            <a href="/rss" target="_blank">
              <span role="img" className="anticon">
                <svg
                  viewBox="0 0 1024 1024"
                  version="1.1"
                  p-id="4788"
                  width="24"
                  height="24"
                >
                  <defs>
                    <style type="text/css"></style>
                  </defs>
                  <path
                    d="M512 0C230.4 0 0 230.4 0 512s230.4 512 512 512 512-230.4 512-512S793.6 0 512 0z m-182.4 768C288 768 256 736 256 694.4s32-73.6 73.6-73.6 73.6 32 73.6 73.6-32 73.6-73.6 73.6z m185.6 0c0-144-115.2-259.2-259.2-259.2v-80c185.6 0 339.2 150.4 339.2 339.2h-80z m172.8 0c0-240-195.2-432-432-432V256c281.6 0 512 230.4 512 512h-80z"
                    fill="currentColor"
                  ></path>
                </svg>
              </span>
            </a>
          </li>
          <li>
            <a
              href="https://github.com/fantasticit/wipi"
              target="_blank"
              rel="noreferrer"
            >
              <span
                role="img"
                aria-label="github"
                className="anticon anticon-github"
              >
                <svg
                  viewBox="64 64 896 896"
                  focusable="false"
                  data-icon="github"
                  width="1em"
                  height="1em"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path d="M511.6 76.3C264.3 76.2 64 276.4 64 523.5 64 718.9 189.3 885 363.8 946c23.5 5.9 19.9-10.8 19.9-22.2v-77.5c-135.7 15.9-141.2-73.9-150.3-88.9C215 726 171.5 718 184.5 703c30.9-15.9 62.4 4 98.9 57.9 26.4 39.1 77.9 32.5 104 26 5.7-23.5 17.9-44.5 34.7-60.8-140.6-25.2-199.2-111-199.2-213 0-49.5 16.3-95 48.3-131.7-20.4-60.5 1.9-112.3 4.9-120 58.1-5.2 118.5 41.6 123.2 45.3 33-8.9 70.7-13.6 112.9-13.6 42.4 0 80.2 4.9 113.5 13.9 11.3-8.6 67.3-48.8 121.3-43.9 2.9 7.7 24.7 58.3 5.5 118 32.4 36.8 48.9 82.7 48.9 132.3 0 102.2-59 188.1-200 212.9a127.5 127.5 0 0138.1 91v112.5c.8 9 0 17.9 15 17.9 177.1-59.7 304.6-227 304.6-424.1 0-247.2-200.4-447.3-447.5-447.3z"></path>
                </svg>
              </span>
            </a>
          </li>
        </ul>
        <div className="_2KuyeuhURuEtdsOPRH7xZB">
          <p>
            Designed by{' '}
            <a href="https://github.com/fantasticit/wipi" target="_blank">
              Fantasticit
            </a>{' '}
            .{' '}
            <a href="https://admin.blog.wipi.tech/" target="_blank">
              后台管理
            </a>
          </p>
          <p>Copyright © 2021. All Rights Reserved.</p>
          <p>皖ICP备18005737号</p>
        </div>
      </footer>

      {/* 模糊搜素 */}
      <WrapDialog 
      flag={flag}
      clickFlag={clickFlag} />
    </>
  );
};

export default Layouts;
