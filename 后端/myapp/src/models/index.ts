import Mail from "./modules/mail"
//文件管理
import File from "./modules/file"
//工作台
import Workbench from './modules/workbench'
//文章管理
import  Articles from './modules/articles'
import article from './modules/article'
//评论管理
import Comment from './modules/comment'
//统计管理
import View from "./modules/view"
//引入评论
import Login from './modules/Login'
//用户管理
import Search from "./modules/search"
import Knowledge from "./modules/knowLedge"
//用户管理
import  User from './modules/user'
import Category from "./modules/category"
import Tag from "./modules/tag"
//  页面管理
import Page from './modules/page'
export default {
    article:new article(),
    mail: new Mail(),
    file: new File(),
    workben: new Workbench(),
    articles: new Articles(),
    Login: new Login(),
    comment: new Comment(),
    view: new View(),
    search: new Search(),
    knowLedge: new Knowledge(),
    category: new Category(),
    tag: new Tag(),
    user: new User(),
    pagess: new Page(),
}
