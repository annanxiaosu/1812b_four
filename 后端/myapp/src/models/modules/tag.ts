import { getTagList } from "@/services/modules/tag";
import { ITagItem } from "@/types/tag";
import { makeAutoObservable, runInAction } from "mobx"

class Tag{
    tagList: ITagItem[] = [];
    constructor(){
        makeAutoObservable(this);
    }

    async getTagList(){
        let result = await getTagList();
        if (result.data){
            runInAction(()=>{
                this.tagList = result.data;
            })
        }
    }
}

export default Tag;