import { ILoginFrom } from "@/types";
import { removeToken, setToken } from '@/utils'
import { login } from "@/services";
import { makeAutoObservable } from "mobx"
class User {
    isLogin = false;
    userInfo = {}
    constructor() {
        makeAutoObservable(this)
    }
    async login(data:ILoginFrom){
        let result=await login(data)
        console.log('result.....',result);
        if(result.data){
            this.isLogin=true,
            this.userInfo=result.data;
            setToken(result.data.token)
        }
        return result.data
    }

    logout() {
        this.isLogin = false;
        this.userInfo = {};
        removeToken()
    }
}
export default User