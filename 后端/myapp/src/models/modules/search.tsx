import { deleteSearch, searchApi } from "@/services";
import { searchtype } from "@/types";
import { message } from "antd";
import { makeAutoObservable, runInAction } from "mobx";


class Search{

    searchdata:searchtype[]=[]
    searchtotal=0

    constructor(){
        makeAutoObservable(this);
    }

    //总数据
    async Apisearch(page: number, pageSize: number, params:{[key:string]:number|string}){
        let result=await searchApi(page,pageSize,params);
        if(result.data)
        {   
            runInAction(()=>{
                this.searchdata=result.data[0];
                this.searchtotal=result.data[1];
            })
        }
    }

    //删除
    async Apidelsearch(ids:string[]){
        message.loading('操作中', 0);
        Promise.all(ids.map(id=>deleteSearch(id))).then(res=>{
            message.destroy()
            message.success('删除成功');
            this.Apisearch(1,12,{});
        }).catch(err=>{
            message.destroy()
            message.error('删除失败');
        })   
    }
}

export default Search;