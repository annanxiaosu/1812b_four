import {pageDatas} from '@/services'
import {PageItem} from '@/types'
import {makeAutoObservable,runInAction} from 'mobx'

class Pages{
    Pagedata:PageItem[] = [];
    Pagenumber:number = 0;
    constructor(){
        makeAutoObservable(this);
    }
    async Pageman(page=1){
        let result=await pageDatas(page);
        console.log(result);
        if(result.data){
            runInAction(()=>{
                this.Pagenumber=result.data[1];
                this.Pagedata=result.data[0];
            })  
        }
        console.log(result.data[0])
        
    }
}
export default Pages;