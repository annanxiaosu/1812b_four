import { getArticleslist, getclasseslist, getlabellist, updateArticles, deleteArticle } from "@/services";
import { makeAutoObservable } from "mobx"
import { Datum, Datummm,label_lei } from '@/types';
import { message } from 'antd';

class Articles {
    article_list: Datum[] = [];
    classes_list: Datummm[] = [];
    label_list: label_lei[] = [];
    constructor() {
        makeAutoObservable(this);
    }
    // 获取：文章管理
    async GET_ARTICLES_LIST(page = 1, pageSize = 12, params: { [key: string]: string | boolean }) {
        let result = await getArticleslist(page, pageSize, params)
        console.log('result...', result);
        if (result.data) {
            this.article_list = result.data[0];
        }
        return result.data;
    }
    // 获取：分类列表
    async GET_CLASSES_LIST() {
        let result = await getclasseslist();
        console.log('result...', result);
        if (result.data) {
            this.classes_list = result.data;
        }
        return result.data;
    }
    // 获取：标签列表
    async GET_LABEL_LIST() {
        let result = await getlabellist()
        console.log('result...', result);
        if (result.data) {
            this.label_list = result.data;
        }
        return result.data;
    }

    // 更新：状态
    async UPDATE_ARTICLES(ids: string[], aa: { [key: string]: string }) {
        message.loading('操作中', 0);
        Promise.all(ids.map(id => updateArticles(id, aa))).then(res => {
            message.destroy()
            message.success('操作成功');
            this.GET_ARTICLES_LIST();
        }).catch(error => {
            message.destroy()
            message.error('操作失败');
        })
    }

    // 删除：评论
    async DELETE_ARTICLES(ids: string[]) {
        message.loading('操作中', 0);
        Promise.all(ids.map(id => deleteArticle(id)))
            .then(res => {
                message.destroy()
                message.success('删除成功');
                this.GET_ARTICLES_LIST();
            })
            .catch(err => {
                message.destroy()
                message.error('删除失败');
            })
    }
}

export default Articles;






