import { getUserlist, getclasseslistUser, updateUser } from "@/services/modules/user";
import { makeAutoObservable } from "mobx"
import { Datum, Datummm } from '@/types';
import { message } from 'antd';

class User {
    user_list: Datum[] = [];
    classes_list: Datummm[] = [];
    constructor() {
        makeAutoObservable(this);
    }
    // 获取：文章管理
    async GET_USER_LIST(page = 1, pageSize = 12, params: { [key: string]: string | boolean }) {
        let result = await getUserlist(page, pageSize, params)
        console.log('result...', result);
        if (result.data) {
            this.user_list = result.data[0];
        }
        return result.data;
    }
    // 获取：分类列表
    async GET_CLASSES_USER_LIST() {
        let result = await getclasseslistUser();
        console.log('result...', result);
        if (result.data) {
            this.classes_list = result.data;
        }
        return result.data;
    }

    async UPDATE_USERS( params: { [key: string]: any }) {
        message.loading('操作中', 0);
        let result = await updateUser(  params)
        console.log('result...', result);
        if (result.data) {
            message.destroy()
            message.success('操作成功');
            this.GET_USER_LIST();
        }else{
            message.destroy()
            message.error('操作失败');
        }
    }
}

export default User;
