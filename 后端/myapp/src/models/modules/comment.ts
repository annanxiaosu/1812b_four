import { deleteComments, getComment, updateComments,getcommit } from "@/services";
import { ICommentItem } from "@/types";
import { makeAutoObservable, runInAction } from "mobx"
import {message} from 'antd';

class Comment{
    commentList: ICommentItem[] = [];
    commentCount: number = 0;
    constructor(){
        makeAutoObservable(this);
    }
        // 分页获取评论
        async getComments(page=1){
            let result = await getcommit(page);
            if (result.data){
                runInAction(()=>{
                    this.commentCount = result.data[1];
                    this.commentList = result.data[0];
                })
            }
        }
    

    // 分页获取评论
    async getComment(page=1, params: {[key:string]:string|boolean}){
        let result = await getComment(page, params);
        if (result.data){
            runInAction(()=>{
                this.commentCount = result.data[1];
                this.commentList = result.data[0];
            })
        }
    }

    // 批量更改评论状态
    async updateComment(ids: string[], status:{[key:string]:boolean}){ 
        message.loading('操作中', 0);
        Promise.all(ids.map(id=>updateComments(id, status)))
        .then(res=>{
            message.destroy()
            message.success('操作成功');
            this.getComments();
        })
        .catch(err=>{
            message.destroy()
            message.error('操作失败');
        })
    }
     // 批量删除评论
     async deleteComment(ids: string[]){ 
        message.loading('操作中', 0);
        Promise.all(ids.map(id=>deleteComments(id)))
        .then(res=>{
            message.destroy()
            message.success('删除成功');
            this.getComments();
        })
        .catch(err=>{
            message.destroy()
            message.error('删除失败');
        })
    }

}

export default Comment;
