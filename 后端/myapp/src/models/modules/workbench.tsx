// 引入
import {getworkben} from '@/services'
import {WorkObject} from '@/types'
import { makeAutoObservable } from 'mobx'

class Workbench {
    workben() {
        throw new Error('Method not implemented.');
    }
    workdata:WorkObject[]=[];
    worknumber:number=0;
    // 构造函数
    constructor() {
        makeAutoObservable(this);
    }
    async Work(){
        let result =await getworkben();
        console.log(result);
        if(result.data){
                this.worknumber=result.data[1];
                this.workdata=result.data[0];
        }
        return result.data;

    }
}
export default Workbench;