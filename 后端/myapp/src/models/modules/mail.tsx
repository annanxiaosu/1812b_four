
import { mailApi,deleteMail, AddMail, replyMail } from "@/services";
import { makeAutoObservable,runInAction} from "mobx"
import {  IReplyMail, smtp } from "@/types";
import { message } from "antd";



class Mail{

    maildata:smtp[]=[];
    mailtotal=0;

    constructor() {
        makeAutoObservable(this)
    }

    async Apimaildata(page: number, pageSize: number, params: { [key: string]: number | string }) {
        let result = await mailApi(page, pageSize, params);
        if (result.data) {
            runInAction(() => {
                this.maildata = result.data[0];
                this.mailtotal = result.data[1];
            })
        }
    }

    //删除
    async Apidelmail(ids: string[]) {
        message.loading('操作中', 0);
        Promise.all(ids.map(id => deleteMail(id))).then(res => {
            message.destroy()
            message.success('删除成功');
            this.Apimaildata(1,8, {});
        }).catch(err => {
            message.destroy()
            message.error('删除失败');
        })
    }

    //增加及回复
    async ApiAppmail(data:smtp){
        let result = await AddMail(data);
        console.log(result);
        if(result.data)
        {
            
        }
    }

    // 回复邮件
    async replyMail(data: IReplyMail) {
        let result = await replyMail(data);
        return result;
    }
}

export default Mail;