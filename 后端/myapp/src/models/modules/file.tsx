import { request } from "@/app";
import { delfileApi, fileApi, uploadApi } from "@/services";
import { file } from "@/types";
import { message } from "antd";
import { makeAutoObservable, runInAction } from "mobx"

class File {

    filedata: file[]=[];
    filetotal=0;

    constructor() {
        makeAutoObservable(this)
    }

    async Apifile(page: number, pageSize: number,params:{[key:string]:number|string}) {
        let result = await fileApi(page,pageSize,params);
        if (result.data) {
            this.filedata=result.data[0];
            this.filetotal=result.data[1];
        }
        return result.data;
    }

    async DelApifile(id:string){
        let result=await delfileApi(id);
        if(result.data)
        {   
            runInAction(()=>{
                message.destroy()
                message.success('删除成功');
                this.Apifile(1, 8, {});
            })
        }else{
            message.destroy()
            message.error('删除失败');
        }
    }

    
    async UpApiload(unique:string|number){
        let result = await uploadApi(unique);
        console.log(result);
        
    }
}

export default File;