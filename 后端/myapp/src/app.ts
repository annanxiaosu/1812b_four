import { RequestConfig, history } from 'umi';
import { message } from 'antd'
import React from 'react'
// 引入mobx
import stateContext from '@/context/stateContext'
import store from '@/models/index'
import { getToken } from './utils';

// 关闭线上console
process.env.NODE_ENV === 'production' ? console.log = () => { } : null;


// 路由切换配置
const whiteList = ['/login', '/redirect'];
export function onRouteChange({ matchedRoutes }: any) {
  let authorization = getToken();
  if (authorization) {
    if (location.pathname === '/login') {
      history.replace('/');
    }
  } else {
    if (whiteList.indexOf(location.pathname) === -1) {
      history.replace('/login?from=' + encodeURIComponent(location.pathname));
    }
  }
}
// 覆盖根组件
export function rootContainer(container: React.ReactNode) {
  return React.createElement(stateContext.Provider, { value: store }, container);
}
const baseUrl = 'https://creationapi.shbwyz.com'
export const request: RequestConfig = {
  timeout: 1000,
  errorConfig: {},
  middlewares: [],
  
  //   请求拦截
  requestInterceptors: [(url, options) => {
      let authorization = getToken();
      if (authorization) {
        options = { ...options, headers: { authorization: 'Bearer ' + authorization } };
      }
    if (!/https?/.test(url)) {
      url = `${baseUrl}${url}`;
    }
      return {
        url,
        options,
      };
    }],
  //   响应拦截
  responseInterceptors: [
    response => {
      const codeMaps: { [key: number]: string } = {
        400: '错误的请求',
        403: '禁止访问',
        402: '找不到资源',
        500: '服务器内部错误',
        502: '网关错误。',
        503: '服务不可用，服务器暂时过载或维护。',
        504: '网关超时。',
      };
      if (Object.keys(codeMaps).indexOf(String(response.status)) !== 1) {
        message.error(codeMaps[response.status])
      }
      return response;
    }
  ],
};