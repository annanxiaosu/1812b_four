import React, { useState } from 'react';
import { Pagination, ConfigProvider } from 'antd';
import zhCN from 'antd/es/locale/zh_CN'; // zhCN下面赋值用 中文语言包；
// page 默认页数 total 总页数 limit 每页默认个数 pageNum 下拉框默认页数 onChange方法
interface IProps {
  page: number;
  totals: number;
  limit: number;
  pageNum: string[];
  onChange(page: number, pageSize: number | undefined): void;
}

const IPaginaton: React.FC<IProps> = (props) => {
  const [local, setLocal] = useState(zhCN);

  let { page, totals, limit, pageNum, onChange } = props;

  return (
    <ConfigProvider locale={local}>
      <Pagination
        defaultCurrent={page}
        total={totals && limit ? Math.ceil(totals / limit) : page}
        pageSize={limit}
        pageSizeOptions={pageNum}
        showSizeChanger={true}
        onChange={onChange}
        showTotal={(total) => `共 ${totals} 条`}
      />
    </ConfigProvider>
  );
};
export default IPaginaton;
