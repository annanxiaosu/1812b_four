import useStore from '@/context/useStore';
import { InboxOutlined } from '@ant-design/icons';

import './index.less';
import {
  Drawer,
  Form,
  Button,
  Input,
  Select,
  Switch,
  Upload,
  message,
} from 'antd';
import { useEffect, useState } from 'react';
import React from 'react';
interface IProp {
  name: string;
  multiple: boolean;
  action: string;
  onChange(info: any): void;
  onDrop(e: any): void;
}
const IDrawer: React.FC = (props) => {
  const store = useStore();
  const { Option } = Select;
  const [page, setpage] = useState(1);
  const [limit, setlimit] = useState(12);
  const [params, setParams] = useState({});
  useEffect(() => {}, [store.knowLedge.visible]);
  function onClose() {
    store.knowLedge.visible = false;
  }
  useEffect(() => {
     
  }, [store.knowLedge.flag])

  const { Dragger } = Upload;
  const prop: IProp = {
    name: 'file',
    multiple: true,
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    onChange(info) {
      const { status } = info.file;
      if (status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (status === 'done') {
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onDrop(e) {
      console.log('Dropped files', e.dataTransfer.files);
    },
  };
  function handleFile (e:React.MouseEvent<HTMLElement, MouseEvent>){
   store.knowLedge.getKnowledge(page, limit, params,e.currentTarget.firstChild!.ATTRIBUTE_NODE);
   store.knowLedge.flag=true;
   store.knowLedge.visible=false;
   
   }
  return (
    <>
      <Drawer
        title="新建知识库"
        width={600}
        onClose={onClose}
        visible={store.knowLedge.visible}
        bodyStyle={{ paddingBottom: 80 }}
        footer={
          <div
            style={{
              textAlign: 'right',
            }}
          >
            <Button onClick={onClose} style={{ marginRight: 8 }}>
              取消
            </Button>
            <Button onClick={onClose} type="primary">
              创建
            </Button>
          </div>
        }
      >
        <Form wrapperCol={{ span: 20 }} layout="horizontal" hideRequiredMark>
          <Form.Item
            colon={false}
            name="name"
            label="名称"
            style={{ alignItems: 'center' }}
            rules={[{ required: true, message: 'Please enter user name' }]}
          >
            <Input style={{ width: 502 }} placeholder="" />
          </Form.Item>

          <Form.Item
            colon={false}
            name="owner"
            style={{ alignItems: 'center' }}
            label="描述"
            rules={[{ required: true, message: 'Please select an owner' }]}
          >
            <Input.TextArea style={{ width: 502 }} placeholder="" />
          </Form.Item>

          <Form.Item
            colon={false}
            name="dateTime"
            label="评论"
            style={{ alignItems: 'center' }}
            rules={[{ required: true, message: 'Please choose the dateTime' }]}
          >
            <Switch checked={false}/>
          </Form.Item>

          <Form.Item
            colon={false}
            name="description"
            label="封面"
            style={{ alignItems: 'center' }}
            rules={[
              {
                required: true,
                message: 'please enter url description',
              },
            ]}
          >
            <div>
              <Dragger {...prop} style={{ background: '#fff', width: 502 }}>
                <p className="ant-upload-drag-icon">
                  <InboxOutlined />
                </p>

                <p className="ant-upload-text">
                  点击选择文件或将文件拖拽到此处
                </p>
                <p className="ant-upload-hint">
                  文件将上传到 OSS, 如未配置请先配置
                </p>
              </Dragger>
              <Input placeholder="" style={{ marginTop: 16, width: 502 }} />
              <Button style={{ marginTop: 16 }} onClick={handleFile}>选择文件</Button>
            </div>
          </Form.Item>
        </Form>
      </Drawer>
      
    </>
  );
};
export default IDrawer;
