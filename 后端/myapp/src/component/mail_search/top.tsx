import { Button, Input } from "antd";
import React, { useState } from "react"
import styles from "./top.less"

interface Props {
    top: Array<string>
}



const Index: React.FC<Props> = props => {

    console.log(props.top);
    const [value, setstate] = useState('')

    function clickOn(value: string) {
        console.log(value);

    }

    return <div className={styles.indextop}>
        <ul>
            {
                props.top.map((item, index) => {
                    return <li key={index}>
                        <span>{item}：</span><Input placeholder={`请输入${item}`} />
                    </li>
                })
            }
        </ul>
        <div className={styles.indexrigth}>
            <Button type="primary"
                onClick={() => clickOn(value)}
            >搜索</Button>
            <Button>重置</Button>
        </div>
    </div>
}

export default Index;
