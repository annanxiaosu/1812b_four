import React, { useEffect, useState } from 'react';
import { Form, Row, Col, Input, Button, Select } from 'antd';
import './index.less';
import useStore from '@/context/useStore';

interface IProps {
  page: number;
  limit: number;
}
const Search: React.FC<IProps> = (props) => {
  let { page, limit } = props;

  const store = useStore();
  const [params, setParams] = useState({});
  const [expand, setExpand] = useState(false);
  const [form] = Form.useForm();
  const onGenderChange = (value: string) => {
    switch (value) {
      case 'publish':
        form.setFieldsValue({ note: 'Hi, man!' });
        return;
      case 'draft':
        form.setFieldsValue({ note: 'Hi, lady!' });
        return;
    }
  };

  const getFields = () => {
    const children = [];
    children.push(
      <Col span={8} key={1}>
        <Form.Item name="title" label={`名称`}>
          <Input placeholder="请输入知识库名称" />
        </Form.Item>
        <Form.Item name="status" label={`状态`}>
          <Select placeholder="" onChange={onGenderChange} allowClear>
            <Select.Option value="publish">已发布</Select.Option>
            <Select.Option value="draft">草稿</Select.Option>
          </Select>
        </Form.Item>
      </Col>,
    );
    return children;
  };
   useEffect(() => {
    store.knowLedge.getKnowledge( page!, limit!, params);
   }, [params])

  const onFinish = (values: any) => {
    let params = {};
    for(let key in values){
      values[key] && (params[key] = values[key])
    }
    setParams(params);
    
  };
  return (
    <Form
      style={{ padding: '24px 12px' }}
      form={form}
      name="advanced_search"
      className="ant-advanced-search-form"
      onFinish={onFinish}
    >
      <Row gutter={24}>{getFields()}</Row>
      <Row>
        <Col span={24} style={{ textAlign: 'right' }}>
          <Button type="primary" htmlType="submit">
            搜索
          </Button>
          <Button
            style={{ marginLeft: '8px' }}
            onClick={() => {
              form.resetFields();
            }}
          >
            重置
          </Button>
        </Col>
      </Row>
    </Form>
  );
};

export default Search;
