import useStore from "@/context/useStore";
import { file } from "@/types";
import { Button, Card, Drawer, Form, Input, message, Pagination, Upload } from "antd";
import Meta from "antd/lib/card/Meta";
import { observer } from "mobx-react-lite";
import React, { useEffect, useState } from "react"
import ImageView from "../ImageView";
import styles from "./index.less"

interface Props {
    flag: boolean
    flagchildren: Function
}

interface IForm {
    [key: string]: number | string
}

const Drawert: React.FC<Props> = (props) => {

    // 控制关闭
    function setVisible() {
        props.flagchildren();
    }

    //提交表单
    const [form] = Form.useForm();
    const [params, setparam] = useState<IForm>({})
    function submit() {
        let values = form.getFieldsValue();
        let params: IForm = {}
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setparam(params);
        console.log(params);

    }

    //获取数据
    const store = useStore();
    useEffect(() => {
        store.file.Apifile(1, 12, params);
    }, [params])

    //上传
    const propt = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info: any) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    }

    return <div>
        {/* 抽屉 */}
        <Drawer
            width="35%"
            title="文件选择"
            placement="right"
            onClose={() => setVisible()}
            visible={props.flag}
        >

            {/* --------------------标题-------------------- */}
            <div className={styles.articles_head}>
                <Form form={form} onFinish={submit}>
                    <div className={styles.articles_head_one}>
                        <div className={styles.biaoti}>
                            <Form.Item
                                name="originalname"
                                label="文件名"
                            >
                                <Input size="small" type="text" placeholder="请输入发件人" />
                            </Form.Item>
                        </div>
                        <div className={styles.biaoti}>
                            <Form.Item
                                name="type"
                                label="文件类"
                            >
                                <Input size="small" type="text" placeholder="请输入收件人" />
                            </Form.Item>
                        </div>
                    </div>
                    <div className={styles.articles_head_two}>
                        <Button size="small" htmlType="reset">重置</Button>
                        <Button size="small" htmlType="submit" type="primary">搜索</Button>
                    </div>
                </Form>

                <div className={styles.uploadright}>
                    <Upload {...propt}>
                        <Button size="middle">上传文件</Button>
                    </Upload>
                </div>


            </div>



            {/* 数据 */}

            <div className={styles.tables}>
                {
                    store.file.filedata && store.file.filedata.map((item: file) => {
                        return <div className={styles.tablestop}>
                            <ImageView>
                                <Card
                                    key="id"
                                    hoverable
                                    cover={<img alt="example" src={item.url} />}
                                >
                                    <Meta title={item.originalname} className={styles.metaa} />
                                </Card></ImageView>
                        </div>

                    })
                }

                {/* 分页 */}
                <Pagination
                    showSizeChanger
                    showTotal={total => `共 ${total} 条`}
                    // onShowSizeChange={onShowSizeChange}
                    pageSizeOptions={['8', '12', '24', '36']}
                    defaultPageSize={12}
                    total={store.file.filetotal}
                    onChange={async function change(page, pageSize) {
                        await store.file.Apifile(page, pageSize as number, params);
                    }}

                />
            </div>
        </Drawer>
    </div>
}


export default observer(Drawert);