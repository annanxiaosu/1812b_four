import React from "react"
import { Upload, message } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import styles from "./index.less"



const { Dragger } = Upload;

const props = {
    name: 'file',
    multiple: true,
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    onChange(info: { file: { name?: any; status?: any; }; fileList: any; }) {
        const { status } = info.file;
        if (status !== 'uploading') {
            console.log(info.file,info.file.name);
        }
        if (status === 'done') {
            message.success(`${info.file.name} file uploaded successfully.`);
        } else if (status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    },
    onDrop(e: { dataTransfer: { files: any; }; }) {
        console.log('Dropped files', e.dataTransfer.files);
    },
};

const Uploading:React.FC=()=>{
    return <div className={styles.divv}>
        <Dragger {...props}>
            <p className="ant-upload-drag-icon">
                <InboxOutlined />
            </p>
            <p className="ant-upload-text">点击选择文件或将文件拖拽到此处 <br />文件将上传到 OSS, 如未配置请先配置</p>
            {/* <p className="ant-upload-hint">
            Support for a single or bulk upload. Strictly prohibit from uploading company data or other
            band files
        </p> */}
        </Dragger>
    </div>
}

export default Uploading;







