import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import { Input, Button } from 'antd';
import './classify.less';

const Classify: React.FC = () => {
    const store = useStore();
    useEffect(() => {
        store.articles.GET_CLASSES_LIST();
    }, []);
    return (
        <div className='classify'>
            <div className="classify_head">

                <div className='classify_head_left'>
                    <h1>添加分类</h1>
                    <Input type="text" placeholder='请输入分类名称' />
                    <Input type="text" placeholder='输入分类值（请输入英文，作为路由使用）' />
                    <Button type="primary" >保存</Button>
                </div>
                <div className='classify_head_right'>
                    <h1>所有分类</h1>
                    {
                        store.articles.classes_list.map((item, index) => {
                            return <div className='classify_box'>
                                <div>{item.label}</div>
                            </div>
                        })
                    }
                </div>

            </div>
        </div>
    )
}
export default observer(Classify);