import React, { Key, useState, useEffect } from 'react'
import { Input, Button, Select, Table, Space, Spin, Form } from 'antd';
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import { articles_all, Datum } from '@/types'
import './articles.less';
const { Option } = Select;
const { useForm } = Form;

interface IForm {
    [key: string]: string | boolean
}

const Articles: React.FC = () => {
    const store = useStore();
    const [form] = useForm();
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(12);
    const [params, setParams] = useState<IForm>({});
    const [selectedRowKeys, setselectedRowKeys] = useState<Key[]>([]);
    const [spinning_flag, setspinning_flag] = useState(true);
    const table_list: articles_all[] = [];
    const rowSelection = { selectedRowKeys, onChange: onSelectChange };

    useEffect(() => {
        store.articles.GET_ARTICLES_LIST(page, pageSize, params).then(() => {
            setspinning_flag(false);
        }).catch(() => {
            setspinning_flag(true);
        })
        store.articles.GET_CLASSES_LIST();
    }, [page, pageSize, params]);


    function submit() {
        let values = form.getFieldsValue();
        let params: IForm = {};
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setParams(params);
    }
    
    // 列表：标题头部
    const columns = [
        { title: '标题', dataIndex: 'title', width: 220, fixed: 'left' },
        { title: '状态', dataIndex: 'status', width: 100, fixed: 'left' },
        { title: '分类', dataIndex: '', width: 100 },
        { title: '标签', dataIndex: '', width: 200 },
        { title: '阅读量', dataIndex: 'views', width: 100 },
        { title: '喜欢树', dataIndex: 'likes', width: 100 },
        { title: '发布时间', dataIndex: 'publishAt', width: 200 },
        {
            title: '操作', dataIndex: 'isRecommended', width: 300, fixed: 'right',
        },
    ];

    store.articles.article_list && store.articles.article_list.map((item, index: number) =>
        table_list.push({
            id: item.id,
            key: index,
            title: item.title,
            status: item.status == 'publish' ? <><b></b>已发布</> : <><b></b>草稿</>,
            views: <span className='lv'>{item.views}</span>,
            likes: <span className='hu'>{item.likes}</span>,
            publishAt: item.publishAt.replace(/T/g, ' ').replace(/\.[\d]{3}Z/, ''),
            isRecommended: item.isRecommended ? < Space size="middle" >
                <a>编辑</a>
                <a>撤销首焦</a>
                <a>查看访问</a>
                <a>删除</a>
            </Space> : < Space size="middle" >
                <a>编辑</a>
                <a>首焦推荐</a>
                <a>查看访问</a>
                <a onClick={() => store.articles.DELETE_ARTICLES([item.id])}>删除</a>
            </Space>
        })
    )

    // 列表——多选框
    function onSelectChange(selectedRowKeys: Key[]) {
        setselectedRowKeys(selectedRowKeys);
        console.log(selectedRowKeys);
    }
    return (
        <div className='articles'>
            {/* --------------------标题-------------------- */}
            <div className="articles_head">
                <Form form={form} onFinish={submit}>
                    <div className='articles_head_one'>
                        <div className='biaoti'>
                            <Form.Item name='title' label="标题" >
                                <Input placeholder="请输入文章标题" />
                            </Form.Item>
                        </div>
                        <div className='biaoti'>
                            <Form.Item name='status' label='状态'>
                                <Select defaultValue="" >
                                    <Option value="publish">已发布</Option>
                                    <Option value="draft">草稿</Option>
                                </Select>
                            </Form.Item>
                        </div>
                        <div className='biaoti'>
                            <Form.Item name='category' label='分类'>
                                <Select defaultValue="">
                                    {
                                        store.articles.classes_list && store.articles.classes_list.map((item, index) => {
                                            return <Option value={item.id}>{item.label}</Option>
                                        })
                                    }
                                </Select>
                            </Form.Item>
                        </div>
                    </div>
                    <div className='articles_head_two'>
                        <Button htmlType="reset">重置</Button>
                        <Button htmlType='submit' type="primary">搜索</Button>
                    </div>
                </Form>
            </div>
            {/* --------------------列表-------------------- */}
            <div className="articles_main">
                <div style={{ marginBottom: 16 }} className='articles_main_one' >
                    {
                        selectedRowKeys.length ? <>
                            <Button onClick={() => store.articles.UPDATE_ARTICLES(selectedRowKeys as string[], { status: 'publish' })}>发布</Button>
                            <Button onClick={() => store.articles.UPDATE_ARTICLES(selectedRowKeys as string[], { status: 'draft' })}> 草稿</Button>
                            <Button>首焦推荐</Button>
                            <Button>撤销推荐</Button>
                            <Button onClick={() => store.articles.DELETE_ARTICLES(selectedRowKeys as string[])} danger>删除</Button></> : null
                    }
                    <Button type="primary">+ 新建</Button>
                </div>
                {/* 加载、列表 */}
                <Spin tip='loading...' spinning={spinning_flag}>
                    <Table
                        rowSelection={rowSelection}
                        columns={columns}
                        dataSource={table_list}
                        scroll={{ x: 1000 }}
                        rowKey="id"
                        pagination={{
                            defaultCurrent: 1,
                            showSizeChanger: true,
                            pageSizeOptions: ['8', '12', '24', '36'],
                            pageSize: 8
                        }}
                    />
                </Spin>
            </div>
        </div >
    )
}

export default observer(Articles);