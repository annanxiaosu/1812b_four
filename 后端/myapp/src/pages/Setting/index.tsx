import React, { FC, useState, useEffect } from 'react'
import './ineex.css'
import "./Setting.less";// 样式 
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import Meta from 'antd/lib/card/Meta';
import Item from 'antd/lib/list/Item';
import { setting } from "@/types/setting";
import ImageView from '@/component/ImageView';//图片放大

import { Tabs, Radio, Space, Input, Button, Drawer, Card, Pagination } from 'antd';
import { CommentOutlined } from '@ant-design/icons';
const { Search } = Input;
const onSearch = (value: any) => console.log(value);
const { TabPane } = Tabs;
const { TextArea } = Input;

const Setting: React.FC = () => {

    const store = useStore();
    useEffect(() => {
        store.file.Apifile(1, 12);
    }, [])

    const state = {
        tabPosition: 'left',
    };
    const changeTabPosition = (e: { target: { value: any; }; }) => {
        useState({ tabPosition: e.target.value });
    }
    const { tabPosition } = state;
    const onChange = (e: any) => {
        console.log(e);
    };
    // 抽屉
    const [visible, setVisible] = useState(false);
    const showDrawer = () => {
        setVisible(true);
    };
    const onClose = () => {
        setVisible(false);
    };



    return (
        <div className="Setting">
            <div className="heanderSet">
                系统设置
                <Space style={{ marginBottom: 24 }}>
                    {/* value={tabPosition}   onChange={changeTabPosition} */}
                    <Radio.Group value={tabPosition} onChange={changeTabPosition} >
                        <Radio.Button value="left">left</Radio.Button>
                    </Radio.Group>
                </Space>
            </div>
            {/* tabPosition={tabPosition} */}
            <div className="TabsSet">
                <Tabs tabPosition={tabPosition} >
                    <TabPane tab="系统设置" key="1">

                        <div>系统设置</div>
                        <Input placeholder="creationad.shbwyz.com" allowClear onChange={onChange} />

                        <div>后台地址</div>
                        <Input placeholder="creationadmin.shbwyz.com" allowClear onChange={onChange} />

                        <div>系统标题</div>
                        <Input placeholder="八维创作平台" allowClear onChange={onChange} />

                        <div>Logo</div>
                        <Search placeholder="Logo" onClick={showDrawer} onSearch={onSearch} style={{ width: 200 }} />

                        <div>Favicon</div>
                        <Search placeholder="Favicon" onClick={showDrawer} onSearch={onSearch} style={{ width: 200 }} />

                        <div>页脚信息</div>
                        <TextArea placeholder="博客，创作，blog" allowClear onChange={onChange} >
                            八维创作平台八维创作平台八维创作平台八维创作平台
                        </TextArea>
                        <Button type="primary"> 保存</Button>

                    </TabPane>
                    <TabPane tab="国际化设置" key="2">
                        Content of Tab 2
                    </TabPane>
                    <TabPane tab="SEO设置" key="3">

                        <div>
                            <div>关键字</div>
                            <Input placeholder="八维创作平台" allowClear onChange={onChange} />
                        </div>

                        <div>
                            <div>描述信息</div>
                            <TextArea placeholder="博客，创作，blog" allowClear onChange={onChange} >博客，创作，blog</TextArea>
                        </div>
                        <Button type="primary"> 保存</Button>

                    </TabPane>
                    <TabPane tab="数据统计" key="4">
                        <div>百度统计</div>
                        <Input placeholder="请输入SMTP 密码" allowClear onChange={onChange} />

                        <div>谷歌分析</div>
                        <Input placeholder="请输入正确的邮箱地址" allowClear onChange={onChange} />

                        <Button type="primary"> 保存</Button>
                    </TabPane>
                    <TabPane tab="OSS设置" key="5">
                        Content of Tab 5
                    </TabPane>
                    <TabPane tab="SMTP服务" key="6">
                        <div>SMTP 地址</div>
                        <Input placeholder="请输入SMTP" allowClear onChange={onChange} />

                        <div>SMTP 端口 (强制使用SSL)</div>
                        <Input placeholder="请输入SMTP 端口" allowClear onChange={onChange} />

                        <div>SMTP 用户</div>
                        <Input placeholder="请输入SMTP 用户" allowClear onChange={onChange} />

                        <div>SMTP 密码</div>
                        <Input placeholder="请输入SMTP 密码" allowClear onChange={onChange} />

                        <div>发件人</div>
                        <Input placeholder="请输入正确的邮箱地址" allowClear onChange={onChange} />

                        <Button type="primary"> 保存</Button><Button > 测试</Button>
                    </TabPane>
                </Tabs>

                <Drawer className='Drawer'
                    title="文件选择"
                    placement="right"
                    closable={false}
                    onClose={onClose}
                    visible={visible} 
                    width="786px"
                    >

                    <div className="Drawer_one">
                        <div className='Drawer_head_one'>
                            <div className='Drawer-col'>
                                <span>文件名：</span>
                                <Input placeholder="请输入文件名" />
                            </div>
                            <div className='Drawer-col'>
                                <span>文件类：</span>
                                <Input placeholder="请输入文件类" />
                            </div>

                        </div>
                        <div className='Drawer_head_two'>
                            <Button >重置</Button>
                            <Button type="primary">搜索</Button>
                        </div>
                    </div>

                    <div className="tables">
                        
                        {
                            store.file.filedata.map((item: setting) => {
                                return <div className="tablestop">
                                    <ImageView><Card
                                        hoverable
                                        cover={<img alt="example" src={item.url} />}
                                    >
                                        <Meta title={item.originalname} description={`上传于${item.createAt}`} />
                                    </Card></ImageView>
                                </div>

                            })

                        }

                        {/* 分页 */}
                        <div className="tablesright">
                            <Pagination
                                showSizeChanger
                                // onShowSizeChange={onShowSizeChange}
                                pageSizeOptions={['8', '12', '24', '36']}
                                defaultPageSize={12}
                                total={store.file.filetotal}
                                onChange={async function change(page, pageSize) {
                                    await store.file.Apifile(page, pageSize as number);
                                }}

                            />
                        </div>

                    </div>
                </Drawer>









            </div>
        </div>
    )
}
export default observer(Setting);




