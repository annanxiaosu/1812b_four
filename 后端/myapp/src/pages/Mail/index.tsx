import React, { Key, useCallback, useEffect, useState } from 'react'
import styles from "./index.less"
import { Button, Form, Input, InputNumber, message, Modal, Popconfirm, Table } from 'antd'
import { searchtype, smtp } from '@/types'
import useStore from '@/context/useStore'
import { observer } from 'mobx-react-lite'
import { fomatTime } from '@/utils/time'
import Drawer from '@/component/setDrawer'
import { makeHtml } from '@/utils/markdown'

interface IForm {
    [key: string]: number | string
}


const mail: React.FC = () => {

    //获取数据
    const store = useStore();
    //实现获取表单 1
    const [form] = Form.useForm();
    //多选单选
    const [selectedRowKeys, setselectedRowKeys] = useState<Key[]>([]);
    //表单
    const [params, setparam] = useState<IForm>({})

    const [showModal, setShowedModal] = useState(false);
    const [current, setCurrent] = useState<smtp>();




    function submit() {
        let values = form.getFieldsValue();
        let params: IForm = {}
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setparam(params);
        console.log(params);

    }


    useEffect(() => {
        store.mail.Apimaildata(1, 12, params)
    }, [params])

    //表格
    const columns = [
        {
            title: '发件人',
            dataIndex: 'from',
        },
        {
            title: '收件人',
            dataIndex: 'to',
        },
        {
            title: '主题',
            dataIndex: 'subject',
        },
        {
            title: '发送时间',
            render: ((item: searchtype) => {
                return <span>{fomatTime(item.createAt)}</span>
            })
        },
        {
            title: '操作',
            render: (item: smtp) => {
                return <div>
                    <Popconfirm
                        title="确定删除这个文件吗?"
                        okText="确定"
                        cancelText="取消"
                        onConfirm={() => handleDelete(item.id)}>
                        <Button>删除</Button>
                    </Popconfirm>
                    <Button onClick={() => {
                        setShowedModal(true);
                        setCurrent(item);
                    }}>回复</Button>
                </div>
            }
        },
    ]

    // 删除
    const handleDelete = (id: string) => {
        console.log(id);
        //问题id得加数组
        store.mail.Apidelmail([id])
    }



    //单选框
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange
    }

    function onSelectChange(selectedRowKeys: Key[]) {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        setselectedRowKeys(selectedRowKeys)
    };



    //抽屉
    const [flag, setflag] = useState(false)
    const flagchildren = useCallback(() => { setflag(false) }, []);


    //增加回复——弹出框
    const [isModalVisible, setIsModalVisible] = useState(false);
    function showModals() {
        setIsModalVisible(true);
    };
    const handleOk = () => {
        setIsModalVisible(false);
    };
    const handleCancel = () => {
        setIsModalVisible(false);
    };

    //增加回复——表单事件
    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
    };

    //校验邮箱
    const validateMessages = {
        types: {
            email: '${label} is not a valid email!'
        }
    };

    const [addedit, setaddedit] = useState(false);
    const [iffrom, setiffrom] = useState<smtp>();
    function onFinishs(values: any) {
        // values.push({
        //     id:+new Date + "",
        //     createAt:'2021-09-08T08:26:55.526Z'
        // })
        setiffrom({
            id: +new Date + "",
            createAt: '2021-09-08T08:26:55.526Z',
            ...values
        })
        console.log('iffrom.......', iffrom);
        store.mail.ApiAppmail(iffrom!)
    };


    // 回复文本
    const [reply, setReply] = useState('');
    // 回复邮件
    async function replyMail() {
        if (!reply) {
            message.warn('请输入回复内容');
            return;
        }
        let result = await store.mail.replyMail({
            to: current?.from!,
            subject: `回复: ${current?.subject}`,
            html: makeHtml(reply)
        })
        if (result.message !== '邮件发送成功') {
            message.warn(result.message);
        } else {
            message.success(result.message);
        }
        setShowedModal(false);
    }

    return (
        <div className={styles.mail}>

            <Modal title="Basic Modal" visible={isModalVisible} footer={null} onCancel={handleCancel}>
                <Form {...layout} name="nest-messages" onFinish={onFinishs} validateMessages={validateMessages}>
                    <Form.Item name='from' label="发件人" rules={[{ type: 'email' }]}>
                        <Input />
                    </Form.Item>
                    <Form.Item name='to' label="收件人" rules={[{ type: 'email' }]}>
                        <Input />
                    </Form.Item>
                    <Form.Item name='subject' label="主题">
                        <Input />
                    </Form.Item>
                    <Form.Item name='text' label="文本">
                        <Input.TextArea />
                    </Form.Item>
                    <Form.Item name='html' label="页面">
                        <Input.TextArea />
                    </Form.Item>
                    <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                        <Button type="primary" htmlType="submit" onClick={() => { handleOk }}>
                            提交
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>

            {/* 抽屉按钮 */}
            <button onClick={() => { setflag(true) }}>抽屉</button>
            <Drawer flag={flag} flagchildren={flagchildren} />


            {/* --------------------标题-------------------- */}
            <div className={styles.articles_head}>
                <Form form={form} onFinish={submit}>
                    <div className={styles.articles_head_one}>
                        <div className={styles.biaoti}>
                            <Form.Item
                                name="from"
                                label="发件人"
                            >
                                <Input type="text" placeholder="请输入发件人" />
                            </Form.Item>
                        </div>
                        <div className={styles.biaoti}>
                            <Form.Item
                                name="to"
                                label="收件人"
                            >
                                <Input type="text" placeholder="请输入收件人" />
                            </Form.Item>
                        </div>
                        <div className={styles.biaoti}>
                            <Form.Item
                                name="subject"
                                label="主题"
                            >
                                <Input type="text" placeholder="请输入主题" />
                            </Form.Item>
                        </div>
                    </div>
                    <div className={styles.articles_head_two}>
                        <Button htmlType="reset">重置</Button>
                        <Button htmlType="submit" type="primary">搜索</Button>

                    </div>
                </Form>
            </div>

            {/* 表格数据 */}
            <div className={styles.tables}>
                {
                    selectedRowKeys.length ? <Button danger ghost
                        onClick={() => {
                            store.mail.Apidelmail(selectedRowKeys as string[])
                        }}
                    >删除</Button> : ""
                }
                <div className={styles.buttonight}>
                    <Button
                        // onClick={()=>{
                        //     store.mail.ApiAppmail({
                        //         id:+new Date()+"",
                        //         from:"1987803052@qq.com",
                        //         to:"a1580072427@qq.com",
                        //         subject:"nb666",
                        //         createAt:+new Date()+"",
                        //     })
                        // }}
                        onClick={() => showModals()}
                    >+新建</Button>
                </div>
                <Table
                    rowKey="id"
                    rowSelection={rowSelection}
                    columns={columns}
                    dataSource={store.mail.maildata}
                    pagination={{
                        total: store.mail.mailtotal,
                        showTotal: total => `共 ${total} 条`,
                        showSizeChanger: true,
                        defaultPageSize: 12,
                        pageSizeOptions: ['8', '12', '24', '36'],
                        onChange: async function changeconfige(page, pageSize) {
                            console.log(page, pageSize);
                            await store.mail.Apimaildata(page, pageSize as number, {});
                        }
                    }}
                >
                </Table>

            </div>


            {/* 回复功能 */}
            <Modal
                visible={showModal}
                onCancel={() => setShowedModal(false)}
                footer={<Button onClick={() => replyMail()}>回复</Button>}
            >
                <Input.TextArea rows={10} style={{ marginTop: '25px' }} placeholder="支持markdown，html和文本回复..." value={reply} onChange={e => setReply(e.target.value)}></Input.TextArea>
            </Modal>
        </div>
    )
}
export default observer(mail)