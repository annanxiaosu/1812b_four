import React, { useState, Key, useEffect } from 'react'
import { PageItem } from '@/types';
import style from './index.less'
import { Table, Button, Select, Input } from 'antd';
import useStore from '@/context/useStore';
import { observer } from 'mobx-react-lite';
const Pagemaxs: React.FC = () => {
    const { Option } = Select;
    const [showModal, setShowedModal] = useState(false);
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([])
    const [current, setCurrent] = useState<PageItem>();
    const store = useStore();
    // 获取邮件列表
    useEffect(() => {
        store.pagess.Pageman();
    }, []);
    console.log(store.pagess.Pagedata);
    // 表格选中操作
    function onSelectChange(selectedRowKeys: Key[], selectedRows: PageItem[]) {
        setSelectedRowKeys(selectedRowKeys);
    }

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange
    }


    const columns = [{
        title: '名称',
        dataIndex: 'name'
    },{
        title: '路径',
        dataIndex: 'path'
    }, {
        title: '顺序',
        dataIndex: 'order'
    }, {
        title: '阅读量',
        dataIndex: 'views'
    }, {
        title: '状态',
        dataIndex: 'status'
    }, {
        title: '发布时间',
        dataIndex: 'createAt'
    }, {
        title: '操作',
        render: (item: PageItem) => {
            return <div>
                <Button>编辑</Button>
                <Button>下线</Button>
                <Button>查看访问</Button>
                <Button>删除</Button>
            </div>
        }
    }]

    function onChange() {
        //   console.log(`selected ${value}`);
    }

    function onBlur() {
        console.log('blur');
    }

    function onFocus() {
        console.log('focus');
    }

    function onSearch() {
        console.log(111);
    }
    return (
        <div className={style.page_s}>
            <header>
                <div className={style.ant_breadcrumb}>
                    <span>
                        <span className="ant_breadcrumb_link">
                            <a href="/Workbench">工作台</a>
                        </span>
                        <span className={style.ant_breadcrumb_separator}>/</span>
                    </span>
                    <span>
                        <span className={style.ant_breadcrumb_link}>
                            <a href="/Page">页面管理</a>
                        </span>
                    </span>
                </div>
            </header>
            <main>
                <div className={style._3rOJucTZ4pIum7etE1HHJw}>
                    <div>
                        <div className={style.top_page}>
                            <div className={style.top_top}>
                                <div>名称：<Input placeholder="请输入页面名称" /></div>
                                <div>路径：<Input placeholder="请输入页面路径" /></div>
                                <div>状态：<Select
                                    showSearch
                                    style={{ width: 200 }}
                                    optionFilterProp="children"
                                    onChange={onChange}
                                    onFocus={onFocus}
                                    onBlur={onBlur}
                                    onSearch={onSearch}>
                                    <Option value="草稿">草稿</Option>
                                    <Option value="已上线">已上线</Option>
                                </Select></div>
                            </div>
                            <div className={style.top_bot}>
                                <Button htmlType="submit" type="primary">搜索</Button>
                                <Button htmlType="reset">重置</Button>
                            </div>
                        </div>
                        <div className={style.top_bottom}>
                            <div className={style.top_btn}><Button type="primary">+ 新建</Button></div>
                            <Table
                                rowSelection={rowSelection}
                                columns={columns}
                                dataSource={store.pagess.Pagedata}
                                rowKey="id"
                                pagination={{ showSizeChanger: true }}
                            />

                        </div>
                    </div>
                </div>
            </main>
        </div>
    )
}
export default observer(Pagemaxs);