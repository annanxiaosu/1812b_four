import useStore from '@/context/useStore';
import { ICommentItem } from '@/types';
import React from 'react';
import { Key, useEffect, useState } from 'react';
import { history } from 'umi';
import { observer } from 'mobx-react-lite'
import { Table, Input, Button, Select, Modal, notification, Popover, Popconfirm, message, Badge, Form } from 'antd';
//引入样式
import styles from './index.less'; //等于启动了css-module
//搜索类型
interface IForm {
  [key: string]: string | boolean
}

//表单
const {useForm} = Form;
const Comment: React.FC = () => {
  //表单输入功能
  const [form] = useForm();
    const store = useStore();
    const [page, setPage] = useState(1);
    const [params, setParams] = useState<IForm>({})
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);


    useEffect(()=>{
      store.comment.getComment(page, params);
  }, [page, params])

  function submit(){
    let values = form.getFieldsValue();
    //联合类型
    let params: IForm= {};
    for (let key in values){
        values[key] && (params[key] = values[key]);
    }
    setParams(params);
}
  //渲染数据
  const columns = [{
    title: '状态',
    width: 100,
    fixed: 'left',
    render: (row: ICommentItem) => {
      return row.pass ? <Badge status="success" text="通过" /> : <Badge status="warning" text="未通过" />
    }
  }, {
    title: '称呼',
    dataIndex: 'name'
  }, {
    title: '联系方式',
    dataIndex: 'email'
  }, {
    title: '原始内容',
    render: (item: ICommentItem) => {
      return <Popover content={item.content} title="评论详情-原始内容">
        <a>查看内容</a>
      </Popover>
    }
  }, {
    title: 'HTML内容',
    render: (item: ICommentItem) => {
      return <Popover content={<div dangerouslySetInnerHTML={{ __html: item.html }}></div>} title="评论详情-HTML内容">
        <a>查看内容</a>
      </Popover>
    }
  }, {
    title: '管理文章',
    render: (item: ICommentItem) => {
      return <Popover content={<iframe src={'https://creation.shbwyz.com' + item.url}></iframe>} title="页面预览">
        <a>文章</a>
      </Popover>
    }
  }, {
    title: '创建时间',
    dataIndex: 'createAt'
  }, {
    title: '父级评论',
    dataIndex: 'parentCommentId',
    render: (item: ICommentItem) => {
      return <p>无</p>
    }

  }, {
    title: '操作',
    fixed: 'right',
    width: 200,
    render: (row: ICommentItem) => {
      return <p>
        <a onClick={() => store.comment.updateComment([row.id], { pass: true })}>通过</a><span className={styles.span}>|</span>
        <a onClick={() => store.comment.updateComment([row.id], { pass: false })}>拒绝</a><span className={styles.span}>|</span>
        <a onClick={showModal}>回复</a><span className={styles.span}>|</span>
        <Popconfirm
          title="确认删除这个评论？"
          onConfirm={confirm}
          onCancel={cancel}
          okText="确定"
          cancelText="取消"
        >
          <a onClick={() => store.comment.deleteComment([row.id])}>删除</a>
        </Popconfirm>

      </p>
    }
  }]

  function onSelectChange(selectedRowKeys: Key[], items: ICommentItem[]) {
    setSelectedRowKeys(selectedRowKeys);
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  }
  //点击回复
  let [state, setState] = useState(false);
  const showModal = function () {
    setState(true);
  };
  const handleOk = (e: any) => {
    console.log(e);
    setState(false);

  };
  const openNotification = () => {
    notification.open({
      message: '回复评论失败',
      type:"error",
      description:
        '请前往系统设置完善 SMTP 设置，前往个人中心更新个人邮箱。',
      onClick: () => {
        console.log('Notification Clicked!');
      },
    });
    setState(false);
  };
  let handleCancel = (e: any) => {
    console.log(e);
    setState(false);
  };
  //分页
  function onShowSizeChange(current: any, pageSize: any) {
    console.log(current, pageSize);
  }
  //删除
  function confirm(e: any) {
    console.log(e);
    message.warning('访客无权进行该操作');
  }
  function cancel(e: any) {
    console.log(e);
    message.error('访客无权进行该操作');
  }
  return (
    <div className={styles.commit}>
      <div>
        <p><span className={styles.work} onClick={() => history.push(`/Workbench`)}>工作台</span>/<span>评论管理</span></p>
      </div>
      <div className={styles.form}>
        <Form 
          form={form}
          onFinish={submit}
          className={styles.top}>
          <Form.Item name="name"
          label="称呼" className={styles.input1}>
            <Input type="text" placeholder="请输入称呼"/>
          </Form.Item>
          <Form.Item name="email"
          label="Email" className={styles.input1}>
            <Input type="text" placeholder="请输入联系方式"/>
          </Form.Item>
          <Form.Item 
            name="pass"
            label="状态"
            className={styles.select}>
            <Select>
              <Select.Option value="true">通过</Select.Option>
              <Select.Option value="false">未通过</Select.Option>
            </Select>
          </Form.Item>
          <div className={styles.button}>
          <Button htmlType="submit" type="primary" className={styles.sub}>搜索</Button>
          <Button htmlType="reset">重置</Button>
          </div>
        </Form>
        {selectedRowKeys.length?<section>
            <Button onClick={()=>store.comment.updateComment(selectedRowKeys as string[], {pass:true})}>通过</Button>    
            <Button onClick={()=>store.comment.updateComment(selectedRowKeys as string[], {pass:false})}>拒绝</Button>    
            <Button onClick={()=>store.comment.deleteComment(selectedRowKeys as string[])}>删除</Button>    
        </section>:null}

      </div>
      <Table
        rowSelection={rowSelection} columns={columns} dataSource={store.comment.commentList} rowKey="id"
        scroll={{ x: 1300 }}
        className={styles.form}
      />
       <div>
       
        <Modal
         visible={state}
         title="回复评论"
         onOk={openNotification}
         onCancel={handleCancel}
        >
          <textarea
            className={styles.model}
            placeholder="请输入评论内容 (支持Markdown)"
          ></textarea>
        </Modal>
        {/* 分页 */}
      </div>
    </div>
  );
};
export default observer(Comment);
