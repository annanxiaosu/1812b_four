import React, { useEffect, useState } from 'react';
import styles from './index.less';
import { Button, Card, Spin, Empty, Popconfirm, ConfigProvider } from 'antd';

import zhCN from 'antd/es/locale/zh_CN'; // zhCN下面赋值用 中文语言包；
import './index.less';
import 'antd/dist/antd.css';
import Search from "@/component/Search";
import {
  EditOutlined,
  SettingOutlined,
  CloudUploadOutlined,
  DeleteOutlined,
  PlusOutlined,
} from '@ant-design/icons';

import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import Pagination from '@/component/Pagination';
import IDrawer from '@/component/Drawer';
import { IRootObject } from '@/types';
import BigDrawer from '@/component/BigDrawer';
const KnowLedge: React.FC = (props) => {
  const store = useStore();
  const [page, setpage] = useState(1);
  const [local, setLocal] = useState(zhCN);
  const [limit, setlimit] = useState(12);
  const [params, setParams] = useState({});
  const [loading, setLoading] = useState(true);
  const [pageNum, setPageNum] = useState(['8', '12', '24', '36']);
  useEffect(() => {
    store.knowLedge.getKnowledge(page, limit, params);
    return () => {};
  }, [page, limit]);

  useEffect(() => {
    !(store.knowLedge.KnowLedgeList.length > 0) && setLoading(false);
  }, [store.knowLedge.KnowLedgeList]);
  useEffect(() => {}, [store.knowLedge.visible]);
  function onChange(page: number, pageSize: number | undefined) {
    setlimit(pageSize!);
  }
  useEffect(() => {}, [store.knowLedge.visible]);

  const showPopconfirm = (id:string) => {
     store.knowLedge.KnowLedgeList.map(item=>{
       if(item.id===id){
         item.isCommentable=!item.isCommentable   
       }
     })
  };

  const handleOk = (id:string) => {
    store.knowLedge.KnowLedgeList.map(item=>{
      if(item.id===id){
        item.isCommentable=!item.isCommentable   
      }
    })
    store.knowLedge.deleteKnowledge(id);
  };

  const handleCancel = (id:string) => {
    store.knowLedge.KnowLedgeList.map(item=>{
      if(item.id===id){
        item.isCommentable=!item.isCommentable        
      }
    })
  };
  function handleSet(item:IRootObject){
    
    console.log(item.cover);
    
   store.knowLedge.visible=true;
  
   
  }  

  return (
    <div>
      {/* 头部搜索    */}
      <Search page={page} limit={limit} />

      <div style={{ padding: '24px 12px' }} className={styles.main}>
        {/* 新建部分 */}
        <div className={styles.top}>
          <div></div>
          <div>
            <Button
              type="primary"
              onClick={() => {
                store.knowLedge.visible = true;
              }}
            >
              <PlusOutlined /> 新建
            </Button>
          </div>
        </div>
        {/* 图片编辑部分 */}

        <Spin size="large" spinning={loading}>
          {store.knowLedge.KnowLedgeList.length > 0 ? (
            <div className={styles.imgBox}>
              {store.knowLedge.KnowLedgeList.map((item) => {
                return (
                  <Card
                    onLoadCapture={() => {
                      setLoading(false);
                    }}
                    key={item.id}
                    style={{ width: 300, marginTop: 16, marginRight: '1.5%' }}
                    actions={[
                      <EditOutlined key="edit" />,
                      <CloudUploadOutlined key="cloudupload" />,
                      <SettingOutlined key="setting" onClick={()=>handleSet(item)}/>,

                      <Popconfirm
                        title="确认删除"
                        visible={item.isCommentable}
                        okText="确认"
                        cancelText="取消"
                        onConfirm={()=>handleOk(item.id)}
                        // okButtonProps={{ loading: confirmLoading }}
                        onCancel={()=>handleCancel(item.id)}
                      >
                        <Button
                          style={{ border: 'none', top: '-4px' }}
                          onClick={()=>showPopconfirm(item.id)}
                        >
                          <DeleteOutlined
                            key="delete"
                          />
                        </Button>
                      </Popconfirm>,
                    ]}
                    cover={<img src={item.cover} alt="" />}
                  >
                    <h3>{item.title}</h3>
                    {item.summary && <p>{item.summary}</p>}
                  </Card>
                );
              })}
            </div>
          ) : (
            <ConfigProvider locale={local}>
              {' '}
              <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />{' '}
            </ConfigProvider>
          )}
        </Spin>
        {/* 分页部分 */}
        <div className={styles.bottom}>
          <Pagination
            limit={limit}
            page={page}
            pageNum={pageNum}
            onChange={onChange}
            totals={store.knowLedge.total}
          />
        </div>
      </div>
      <div>
        <IDrawer />
      </div>
      <div>
         <BigDrawer />
      </div>
    </div>
  );
};
export default observer(KnowLedge);

