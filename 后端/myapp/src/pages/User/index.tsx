import React, { Key, useState, useEffect } from 'react'
import { Input, Button, Select, Table, Space, Spin, Form, Tooltip } from 'antd';
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import { user_all } from '@/types'
import { ReloadOutlined } from '@ant-design/icons';//图标
import './user.less';
const { Option } = Select;
const { useForm } = Form;

interface IForm {
    [key: string]: string | boolean
}
const User: React.FC = () => {
    const store = useStore();
    const [form] = useForm();
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(12);
    const [params, setParams] = useState<IForm>({});
    const [selectedRowKeys, setselectedRowKeys] = useState<Key[]>([]);
    const [spinning_flag, setspinning_flag] = useState(true);
    const table_list: user_all[] = [];
    const rowSelection = { selectedRowKeys, onChange: onSelectChange };

    useEffect(() => {
        store.user.GET_USER_LIST(page, pageSize, params).then(() => {
            setspinning_flag(false);
        }).catch(() => {
            setspinning_flag(true);
        })
        store.user.GET_CLASSES_USER_LIST();
    }, [page, pageSize, params]);

    function submit() {
        let values = form.getFieldsValue();
        let params: IForm = {};
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setParams(params);
    }
    // 列表：标题头部
    const columns = [
        { title: '账户', dataIndex: 'name', width: 120, fixed: 'left' },
        { title: '邮箱', dataIndex: 'email', width: 100 },
        { title: '角色', dataIndex: 'role', width: 100 },
        { title: '状态', dataIndex: 'status', width: 100 },
        { title: '注册时间', dataIndex: 'createAt', width: 200 },
        {
            title: '操作', dataIndex: '', width: 300, fixed: 'right',
            render: () => (
                <Space size="middle"> <a>启用</a><a>禁用</a> <a>授权</a> <a>解除授权</a></Space>)
        },
    ]; 

    store.user.user_list && store.user.user_list.map((item, index: number) =>
        table_list.push({
            id: item.id,
            key: index,
            name: item.name,
            email: item.email,
            status: item.status == 'active' ? <><b></b>可用</> : <> <b></b>已锁定</>,
            role: item.role == 'visitor' ? <><b></b>访客</> : <><b></b>管理员</>,
            createAt: item.createAt.replace(/T/g, ' ').replace(/\.[\d]{3}Z/, '')

            // row.pass?<Badge status="success" text="通过" />:<Badge status="warning" text="未通过" />
        })
    )

    // 列表——多选框
    function onSelectChange(selectedRowKeys: Key[]) {
        setselectedRowKeys(selectedRowKeys);
        console.log(selectedRowKeys);
    }
    // 按钮：发布
    function btn_release() {
        console.log(selectedRowKeys);

    }
    return (
        <div className='user'>
            {/* --------------------标题-------------------- */}
            <div className="user_head">
                <Form form={form} onFinish={submit}>
                    <div className='user_head_one'>
                        <div className='user_biaoti'>
                            <Form.Item name='name' label="账户" >
                                <Input placeholder="请输入用户账户" />
                            </Form.Item>
                        </div>
                        <div className='user_biaoti'>
                            <Form.Item name='email' label="邮箱" >
                                <Input placeholder="请输入账户邮箱" />
                            </Form.Item>
                        </div>
                        <div className='user_biaoti'>
                            <Form.Item name='role' label='角色'>
                                <Select defaultValue="">
                                    <Option value="admin">管理员</Option>
                                    <Option value="visitor">访客</Option>
                                </Select>
                            </Form.Item>
                        </div>
                        <div className='user_biaoti'>
                            <Form.Item name='status' label='状态'>
                                <Select defaultValue="" >
                                    <Option value="active">锁定</Option>
                                    <Option value="locked">可用</Option>
                                </Select>
                            </Form.Item>
                        </div>

                    </div>
                    <div className='user_head_two'>
                        <Button htmlType="reset">重置</Button>
                        <Button htmlType='submit' type="primary">搜索</Button>
                    </div>
                </Form>
            </div>
            {/* --------------------列表-------------------- */}
            <div className="user_main">
                <div style={{ marginBottom: 16 }} className='user_main_one' >
                    {
                        selectedRowKeys.length ? <>
                            <Button onClick={() => store.user.UPDATE_USERS({ status: 'looked' })}>启用</Button>
                            <Button onClick={() => store.user.UPDATE_USERS({ status: 'active' })}> 禁用</Button>
                            <Button onClick={() => store.user.UPDATE_USERS({ status: 'looked' })}>解除授权</Button>
                            <Button onClick={() => store.user.UPDATE_USERS({ status: 'active' })}> 授权</Button>
                            {/* <Button>首焦推荐</Button>
                            <Button>撤销推荐</Button> */}
                            <Button danger>删除</Button></> : null
                    }
                    {/* row.pass?<Badge status="success" text="通过" />:<Badge status="warning" text="未通过" /> */}

                    <Tooltip title="刷新">
                        <ReloadOutlined />
                    </Tooltip>


                </div>
                {/* 加载、列表 */}
                <Spin tip='loading...' spinning={spinning_flag}>
                    <Table
                        rowSelection={rowSelection}
                        columns={columns}
                        dataSource={table_list}
                        rowKey="id"
                        scroll={{ x: 1000 }}
                        pagination={{
                            defaultCurrent: 1,
                            showSizeChanger: true,
                            pageSizeOptions: ['8', '12', '24', '36'],
                            pageSize: 8
                        }}
                    />
                </Spin>
            </div>
        </div >
    )
}

export default observer(User);