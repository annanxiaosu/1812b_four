import React, { useEffect, useState } from 'react'
import styles from "./index.less"
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import { Button, Card, Drawer, Form, Input, message, Pagination, Popconfirm, Upload } from 'antd';
import Meta from 'antd/lib/card/Meta';
import { file } from '@/types';
import ImageView from '@/component/ImageView';
import HightLight from '@/component/HighLight';
// import Uploading from '@/component/uploading';
import { InboxOutlined } from '@ant-design/icons';



interface IForm {
    [key: string]: number | string
}

const { Dragger } = Upload;





const File: React.FC = () => {

    const [form] = Form.useForm();
    const [params, setparam] = useState<IForm>({})
    const [visible, setVisible] = useState(false)
    const [filedatail, setfiledatail] = useState<file>()
    const store = useStore();
    useEffect(() => {
        store.file.Apifile(1, 12, params);
    }, [params])

    //提交表单
    function submit() {
        let values = form.getFieldsValue();
        let params: IForm = {}
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setparam(params);
        console.log(params);

    }

    //点击出现抽屉
    function clickDrawer(item: file) {
        setVisible(true)
        setfiledatail(item);
    }

    //删除图片按钮
    function handleDelete(id:string){
        store.file.DelApifile(id);
    }

    //上传
    const props = {
        name: 'file',
        multiple: true,
        onChange(info: { file: { name?: any; status?: any; }; fileList: any; }) {
            const { status } = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.file.name);
            }
            if (status === 'done') {
                store.file.UpApiload(info.file.name);
                message.success(`${info.file.name} file uploaded successfully.`);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
        onDrop(e: { dataTransfer: { files: any; }; }) {
            console.log('Dropped files', e.dataTransfer.files);
        },
    };

    return (
        <div className={styles.file}>

            {/* <Uploading /> */}
            {/* 上传 */}
            <div className={styles.divv}>
                <Dragger {...props}>
                    <p className="ant-upload-drag-icon">
                        <InboxOutlined />
                    </p>
                    <p className="ant-upload-text">点击选择文件或将文件拖拽到此处 <br />文件将上传到 OSS, 如未配置请先配置</p>
                    {/* <p className="ant-upload-hint">
            Support for a single or bulk upload. Strictly prohibit from uploading company data or other
            band files
        </p> */}
                </Dragger>
            </div>

            {/* --------------------标题-------------------- */}
            <div className={styles.articles_head}>
                <Form form={form} onFinish={submit}>
                    <div className={styles.articles_head_one}>
                        <div className={styles.biaoti}>
                            <Form.Item
                                name="originalname"
                                label="文件名称"
                            >
                                <Input type="text" placeholder="请输入发件人" />
                            </Form.Item>
                        </div>
                        <div className={styles.biaoti}>
                            <Form.Item
                                name="type"
                                label="文件类型"
                            >
                                <Input type="text" placeholder="请输入收件人" />
                            </Form.Item>
                        </div>
                    </div>
                    <div className={styles.articles_head_two}>
                        <Button htmlType="reset">重置</Button>
                        <Button htmlType="submit" type="primary">搜索</Button>
                    </div>
                </Form>
            </div>

            <div className={styles.tables}>
                {
                    store.file.filedata.map((item: file) => {
                        return <div className={styles.tablestop}>
                            <Card
                                key="id"
                                onClick={() => clickDrawer(item)}
                                hoverable
                                cover={<img alt="example" src={item.url} />}
                            >
                                <Meta title={item.originalname} description={`上传于${item.createAt}`} />
                            </Card>
                        </div>

                    })
                }

                {/* 分页 */}
                <Pagination
                    showSizeChanger
                    showTotal={total => `共 ${total} 条`}
                    // onShowSizeChange={onShowSizeChange}
                    pageSizeOptions={['8', '12', '24', '36']}
                    defaultPageSize={12}
                    total={store.file.filetotal}
                    onChange={async function change(page, pageSize) {
                        await store.file.Apifile(page, pageSize as number, params);
                    }}

                />
                {/* 抽屉 */}
                <Drawer
                    width="27%"
                    title="文件信息"
                    placement="right"
                    onClose={()=>setVisible(false)}
                    visible={visible}
                >
                    <ImageView>
                        <div className={styles.drawerchildren}>
                            <img src={filedatail?.url} alt="" />
                            <p><b>文件名称：</b> {filedatail?.originalname}</p>
                            <p><b>文件路径：</b> {filedatail?.filename}</p>
                            <p><span><b>文件类型：</b> {filedatail?.type}</span> 
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                                <span><b>文件大小：</b>{(filedatail?.size! / 1024).toFixed(2)}KB </span></p>
                            <p className={styles.fileurl}>
                                <b>访问链接：</b>

                                <HightLight><i><code>{filedatail?.url}</code></i></HightLight>
                                    {/* <i>{filedatail?.url}</i> */}
                            </p>
                        </div>
                        <div className={styles.drawerbotton}>
                            
                            <Popconfirm 
                            title="确定删除这个文件吗?" 
                            okText="确定"
                            cancelText="取消" 
                            onConfirm={() => handleDelete(filedatail?.id!)}>
                                <Button danger>删除</Button>
                            </Popconfirm>
                            <Button onClick={() => setVisible(false)}>关闭</Button>
                        </div>
                    </ImageView>
                </Drawer>
            </div>
        </div>

    )
}
export default observer(File);