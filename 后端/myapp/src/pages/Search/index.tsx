import React, { Key, useEffect, useState } from 'react'
import styles from "./index.less"
import { Button, Form, Input, Popconfirm, Table } from 'antd'
import { searchtype } from '@/types'
import useStore from '@/context/useStore'
import { observer } from 'mobx-react-lite'
import { Badge } from 'antd'
import { fomatTime } from '@/utils/time'

interface IForm {
    [key: string]: number | string
}


const search: React.FC = () => {

    //获取数据
    const store = useStore();
    //实现获取表单 1
    const [form] = Form.useForm();

    const [selectedRowKeys, setselectedRowKeys] = useState<Key[]>([]);

    const [params, setparam] = useState<IForm>({})
    function submit() {
        let values = form.getFieldsValue();
        let params: IForm = {}
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setparam(params);
        console.log(params);
    }
    useEffect(() => {
        store.search.Apisearch(1, 12, params)
    }, [params])

    //表格
    const columns = [
        {
            title: '搜索词',
            dataIndex: 'keyword',
        },
        {
            title: '搜索量',
            render: ((item: searchtype) => {
                return <Badge
                    className="site-badge-count-109"
                    count={item.count}
                    style={{ backgroundColor: '#52c41a' }}
                />
            })
        },
        {
            title: '搜索时间',
            render: ((item: searchtype) => {
                return <span>{fomatTime(item.createAt)}</span>
            })
        },
        {
            title: '操作',
            render: (item: searchtype) => {
                return <Popconfirm 
                    title="确定删除这个文件吗?"
                    okText="确定"
                    cancelText="取消"
                    onConfirm={() => handleDelete(item.id)}>
                    <a>删除</a>
                </Popconfirm>
            }
        },
    ]

    // 删除
    const handleDelete = (id: string) => {
        console.log(id);
        //问题id得加数组
        store.search.Apidelsearch([id])
    }



    //单选框
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange
    }

    function onSelectChange(selectedRowKeys: Key[]) {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        setselectedRowKeys(selectedRowKeys)
    };




    return (
        <div className={styles.search}>

            {/* --------------------标题-------------------- */}
            <div className={styles.articles_head}>
                <Form form={form} onFinish={submit}>
                    <div className={styles.articles_head_one}>
                        <div className={styles.biaoti}>
                            <Form.Item
                                name="type"
                                label="类型"
                            >
                                <Input type="text" placeholder="请输入搜索类型" />
                            </Form.Item>
                        </div>
                        <div className={styles.biaoti}>
                            <Form.Item
                                name="keyword"
                                label="搜索词"
                            >
                                <Input type="text" placeholder="请输入搜索词" />
                            </Form.Item>
                        </div>
                        <div className={styles.biaoti}>
                            <Form.Item
                                name="count"
                                label="搜索量"
                            >
                                <Input type="text" placeholder="请输入搜索量" />
                            </Form.Item>
                        </div>
                    </div>
                    <div className={styles.articles_head_two}>
                        <Button htmlType="reset">重置</Button>
                        <Button htmlType="submit" type="primary">搜索</Button>
                        
                    </div>
                </Form>
            </div>

            {/* 表格数据 */}
            <div className={styles.tables}>
                {
                    selectedRowKeys.length ? <Button danger ghost
                        onClick={() => {
                            store.search.Apidelsearch(selectedRowKeys as string[])
                        }}
                    >删除</Button> : ""
                }
                <Table
                    rowKey="id"
                    rowSelection={rowSelection}
                    columns={columns}
                    dataSource={store.search.searchdata}
                    pagination={{
                        total: store.search.searchtotal,
                        showTotal: total => `共 ${total} 条`,
                        showSizeChanger: true,
                        defaultPageSize: 12,
                        pageSizeOptions: ['8', '12', '24', '36'],
                        onChange: async function changeconfige(page, pageSize) {
                            console.log(page, pageSize);
                            await store.search.Apisearch(page, pageSize as number,{});
                        }
                    }}
                >
                </Table>
            </div>

        </div>
    )
}
export default observer(search);