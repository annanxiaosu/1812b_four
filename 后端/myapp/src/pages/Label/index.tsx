import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import { Input, Button } from 'antd';
import './label.less';

const Label: React.FC = () => {
    const store = useStore();
    useEffect(() => {
        store.articles.GET_LABEL_LIST();
    }, []);
    return (
        <div className='label'>
            <div className="label_head">

                <div className='label_head_left'>
                    <h1>添加标签</h1>
                    <Input type="text" placeholder='请输入标签名称' />
                    <Input type="text" placeholder='输入标签值（请输入英文，作为路由使用）' />
                    <Button type="primary" >保存</Button>
                </div>
                <div className='label_head_right'>
                    <h1>所有标签</h1>
                    {
                        store.articles.label_list.map((item, index) => {
                            return <div className='label_box'>
                                <div>{item.label}</div>
                            </div>
                        })
                    }
                </div>

            </div>
        </div >
    )
}
export default observer(Label);