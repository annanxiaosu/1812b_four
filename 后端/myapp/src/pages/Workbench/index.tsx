import React from 'react'
import style from './index.less'
import { NavLink } from 'umi';
import { observer } from 'mobx-react-lite';
import useStore from "@/context/useStore";
import { Popover, Button, Modal ,message ,Popconfirm} from 'antd';
import { useState } from 'react';
import { useEffect } from 'react';
import { Badge } from 'antd';
const Workbench: React.FC = () => {
    const [page, setPage] = useState(1);
    const store = useStore();
    const [visible, setVisible] = React.useState(false);
    const [confirmLoading, setConfirmLoading] = React.useState(false);
    const [modalText, setModalText] = React.useState('Content of the modal');

    const showModal = () => {
        setVisible(true);
      };
      const handleOk = () => {
        setModalText('The modal will be closed after two seconds');
        setConfirmLoading(true);
        setTimeout(() => {
          setVisible(false);
          setConfirmLoading(false);

        }, 2000);
      };

      function confirm() {
        // console.log(e);
        message.success('Click on Yes');
      }
      
      function cancel() {
        // console.log(e);
        message.error('Click on No');
      }
      
      const handleCancel = () => {
        console.log('Clicked cancel button');
        setVisible(false);
      };    
    window.onload = async function () {
        // 你的方法6
        let result = await store.workben.Work();
        console.log(result);
    }

    useEffect(() => {
        store.comment.getComments(page)
    }, [page])


    const text = <span>页面浏览</span>;

    return (
        <div className={style.workbench_s}>
            <header>
                <div className={style.ant_breadcrumb}><span>
                    <span className={style.ant_breadcrumb_link}>
                        <a href="/">工作台</a>
                    </span>
                </span>
                </div>
                <div className={style._356HSOa33WPexiuYnLScFc}></div>
                <div>
                    <article className={style.ant_typography}>
                        <h1>您好，admin</h1>
                        <div>您的角色： 管理员</div>
                    </article>
                </div>
            </header>
            <main>
                <div className={style.ant_card}>
                    <div className={style.ant_card_head}>
                        <div className={style.ant_card_head_wrapper}>
                            <div className={style.ant_card_head_title}>面板导航</div>
                        </div>
                    </div>
                    <div className={style.ant_card_body}>
                        <div className={style.echarts_for_react}>
                            <div>
                                <canvas width="818" height="300" ></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={style.ant_card_second}>
                    <div className={style.ant_card_head}>
                        <div className={style.ant_card_head_wrapper}>
                            <div className={style.ant_card_head_title}>快速导航</div>
                        </div>
                    </div>
                    <div className={style.ant_card_body}>
                        <div className={style.ant_row} >
                            <div className={style.ant_col}>
                                <NavLink to="/Articles">文章管理</NavLink>
                            </div>
                            <div className={style.ant_col}>
                                <NavLink to="/Commit">评论管理</NavLink>
                            </div>
                            <div className={style.ant_col}>
                                <NavLink to="/File">文件管理</NavLink>
                            </div>
                            <div className={style.ant_col}>
                                <NavLink to="/User">用户管理</NavLink>
                            </div>
                            <div className={style.ant_col}>
                                <NavLink to="/View">访问管理</NavLink>
                            </div>
                            <div className={style.ant_col}>
                                <NavLink to="/Setting">系统管理</NavLink>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={style.ant_card_grid}>
                    <div className={style.ant_card_head}>
                        <div className={style.ant_card_head_wrapper}>
                            <div className={style.ant_card_head_title}>最新文章</div>
                            <div className={style.ant_card_extra}>
                                <NavLink to="/Articles">全部文章</NavLink>
                            </div>
                        </div>
                    </div>
                    <div className={style.ant_card_body_s}>
                        {
                            store.workben.workdata && store.workben.workdata.map((item, index) => {
                                return <div className={style.ant_card_grid_s} key={index}>
                                    <a className={style._2kH_Op73GxIfEKgpE8XsRU} href="/article/editor/c63df8fe_908a_41e8_acd9_ff19b7f3e80d">
                                        <img width="120" alt="文章封面" src={item.cover} />
                                        <p className={style._3e3i72MtTQGM2jtV7VdUqx}>{item.title}</p>
                                    </a>
                                </div>
                            })
                        }
                    </div>
                </div>
                <div className={style.ant_card_three}>
                    <div className={style.ant_card_head_three}>
                        <div className={style.ant_card_head_wrapper_three}>
                            <div className={style.ant_card_head_title_three}>最新评论</div>
                            <div className={style.ant_card_extra_three}>
                                <a href="/Commit/comment">
                                    <span>全部评论</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className={style.ant_card_body_three}>
                        <div className={style.ant_list}>
                            <div className={style.ant_spin_nested_loading}>
                                <div className={style.ant_spin_container}>
                                    <ul className={style.ant_list_items}>
                                        {
                                            store.comment.commentList.map((item, index) => {
                                                return <li className={style.antlist} key={index}>
                                                    <span>{item.name}</span>&nbsp;
                                                    在&nbsp;
                                                    <Popover placement="right" title={text} content={<iframe src={'https://creation.shbwyz.com' + item.url}></iframe>}>
                                                        <a>文章</a>
                                                    </Popover>
                                                    &nbsp;评论
                                                    <span>
                                                        <Button className="ant-btn ant-btn-link">
                                                            <Popover content={item.content} title="评论详情-原始内容">
                                                                <a type="primary">查看内容</a>
                                                            </Popover>
                                                        </Button>
                                                    </span>
                                                    <span className="ant-badge ant-badge-status ant-badge-not-a-wrapper">
                                                        {item.pass ? <Badge status="success" text="通过" /> : <Badge status="warning" text="未通过" />}
                                                    </span>
                                                    <ul className={style.ant_list_item_action}>
                                                        <li>
                                                            <div>
                                                                <span className={style._3PphpW_SlonffnZJnC8v8c}>
                                                                    <a onClick={() => store.comment.updateComment([item.id], { pass: true })}>通过</a>
                                                                    <a onClick={() => store.comment.updateComment([item.id], { pass: false })}>拒绝</a>
                                                                    <a type="primary" onClick={showModal}>
                                                                       回复
                                                                    </a>
                                                                    <Popconfirm
                                                                        title="确认删除这个评论？"
                                                                        onConfirm={confirm}
                                                                        onCancel={cancel}
                                                                        okText="确认"
                                                                        cancelText="取消"
                                                                    >
                                                                        <a onClick={() => store.comment.deleteComment([item.id])}>删除</a> 
                                                                    </Popconfirm>                                         
                                                                    <Modal
                                                                        title="回复评论"
                                                                        visible={visible}
                                                                        onOk={handleOk}
                                                                        confirmLoading={confirmLoading}
                                                                        onCancel={handleCancel}
                                                                    >
                                                                        <div className={style.div_teare}>
                                                                        <textarea> </textarea>
                                                                        </div>
                                                                    </Modal>
                                                                </span>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </li>
                                            })
                                        }
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div >
            </main >
        </div >
    )
}
export default observer(Workbench);
