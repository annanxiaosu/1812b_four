import React from 'react'
import useStore from '@/context/useStore';
import { IviewItem } from '@/types'
import {Form,Input,Select,Button,Table,Popconfirm,message,Badge} from "antd"
import { Key, useEffect, useState } from 'react';
import {observer} from 'mobx-react-lite'
import {history } from 'umi';
//引入样式
import styles from "./index.less"
//搜索类型
interface IForm{
  [key: string]: string|boolean
}
const {useForm} = Form;
const View:React.FC= () =>{
  const [form] = useForm();
  const store = useStore();
  const [page,setPage] = useState(1);
  const [params, setParams] = useState<IForm>({})
  const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
  useEffect(() => {
    store.view.getView(page,params);
  }, [page,params])
  //搜索功能
  function submit(){
    let values = form.getFieldsValue();
    //联合类型
    let params: IForm= {};
    for (let key in values){
        values[key] && (params[key] = values[key]);
    }
    setParams(params);
}
  //渲染数据
  const columns = [{
  title: 'URL',
  width: 200,
  fixed: 'left',
  dataIndex: 'url',
  // render: (row:IviewItem)=>{
  //     return <a></a>
  // }
}, {
  title: 'IP',
  dataIndex: 'ip',
  width: 150,
}, {
  title: '浏览器',
  dataIndex: 'browser',
  width: 150,
}, {
  title: '内核',
  dataIndex: 'engine',
  width: 150,
}, {
  title: '操作系统',
  dataIndex: 'os',
  width: 150,
},{
  title: '设备',
  dataIndex: 'osdevice',
  width: 150,
}, {
  title: '地址',
  dataIndex: 'address',
  width: 150,
}, 
{
  title: '访问量',
  render: (row:IviewItem)=>{
      return  <Badge
      count={1}
      style={{ backgroundColor: '#52c41a', color: '#fff', boxShadow: '0 0 0 1px #d9d9d9 inset' }}
    />
  },
  width: 150,
},{
  title: '访问时间',
  dataIndex: 'createAt',
  width: 150,
}, {
  title: '操作',
  fixed: 'right',
  width: 100,
  render: (row:IviewItem)=>{
      return <p>
          <Popconfirm
    title="确认删除这个评论？"
    onConfirm={confirm}
    onCancel={cancel}
    okText="确定"
    cancelText="取消"
  >
            <a onClick={()=>store.view.deleteView(selectedRowKeys as string[])}>删除</a>    
  </Popconfirm>
         
      </p>
  }
}]
function onSelectChange(selectedRowKeys:Key[], items:IviewItem[]){
  setSelectedRowKeys(selectedRowKeys);
}
const rowSelection = {
  selectedRowKeys,
  onChange: onSelectChange
}
//删除
function confirm(e: any) {
  console.log(e);
  message.warning('访客无权进行该操作');
}

function cancel(e: any) {
  console.log(e);
  message.error('访客无权进行该操作');
}
    return (
        <div>
            <div>
        <p><span className={styles.work1} onClick={() => history.push(`/Workbench`)}>工作台</span>/<span>访问统计</span></p>
      </div>
      <div className={styles.form}>
        <Form 
          form={form}
          onFinish={submit}
          className={styles.topview}>
         <div className={styles.form1}>
         <Form.Item name="ip"
          label="IP:" className={styles.input2}>
            <Input type="text" placeholder="请输入IP地址"/>
          </Form.Item>
          <Form.Item name="count"
          label="UA:" className={styles.input2}>
            <Input type="text" placeholder="请输入 User Agent"/>
          </Form.Item>
          <Form.Item name="url"
          label="URL:" className={styles.input2}>
            <Input type="text" placeholder="请输入 URL"/>
          </Form.Item>
          <Form.Item name="address"
          label="地址:" className={styles.input2}>
            <Input type="text" placeholder="请输入地址"/>
          </Form.Item>
         </div>
          <div className={styles.form2}>
          <Form.Item name="browser"
          label="浏览器:" className={styles.input3}>
            <Input type="text" placeholder="请输入浏览器"/>
          </Form.Item>
          <Form.Item name="engine"
          label="内核" className={styles.input3}>
            <Input type="text" placeholder="请输入内核"/>
          </Form.Item>
          <Form.Item name="os"
          label="OS:" className={styles.input3}>
            <Input type="text" placeholder="请输入操作系统"/>
          </Form.Item>
          <Form.Item name="device"
          label="设备:" className={styles.input3}>
            <Input type="text" placeholder="请输入设备"/>
          </Form.Item>
          </div>
          <div className={styles.button}>
          <Button htmlType="submit" type="primary" className={styles.sub}>搜索</Button>
          <Button htmlType="reset">重置</Button>
          </div>
        </Form>
        
        </div>
        {/* 表格 */}
        {selectedRowKeys.length?<section>
            <Button onClick={()=>store.view.deleteView(selectedRowKeys as string[])} className={styles.delect}>删除</Button>    
        </section>:null}
        <Table 
        rowSelection={rowSelection} columns={columns} dataSource={store.view.viewList} rowKey="id"
        scroll={{  x: 240 }}
        className={styles.titleview}
      />
        </div>
    )
}
export default observer(View);