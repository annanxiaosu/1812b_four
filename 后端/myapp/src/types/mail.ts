export interface smtp{
    id : string
    from : string
    to : string
    subject : string
    text ?: string
    html?: string
    createAt : string
}

export interface RootObject1 {
    createAt: string;
    id: string;
    subject: string;
    to: string;
    from: string;
}


export interface IReplyMail {
    to: string;
    subject: string;
    html: string;
}