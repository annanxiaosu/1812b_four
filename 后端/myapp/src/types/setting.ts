export interface setting{
    id: string,
    originalname:string,
    filename:string,
    type: string,
    size: number,
    url:string,
    createAt:string
}