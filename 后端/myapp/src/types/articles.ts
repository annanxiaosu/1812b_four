// 文章详情，类型
export interface articles_all {
    statusCode: number;
    msg?: any;
    success: boolean;
    data: (Datum[] | number)[];
}
export interface Datum {
    id: string;
    title: string;
    cover?: string;
    summary?: any;
    content: string;
    html: string;
    toc: string;
    status: string;
    role:string;
    views: number;
    likes: number;
    isRecommended: boolean;
    needPassword: boolean;
    totalAmount?: string;
    isPay: boolean;
    isCommentable: boolean;
    publishAt: string;
    createAt: string;
    updateAt: string;
    tags: any[];
    category?: any;
}
export interface RootObject {
    id: number;
    statusCode: number;
    msg?: any;
    success: boolean;
    data: Datummm[];
}

export interface Datummm {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
    articleCount: number;
}

export interface label_lei {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
    articleCount: number;
  }