export * from './Login';
export * from './user';
export * from './workbench'
//文章管理
export * from './articles';
//评论管理
export * from "./comments"
//邮箱管理
export * from "./mail"
//文件管理
export * from "./file"
//统计管理
export * from "./view"
//搜索管理
export * from './search'
export * from './knowLedge'
// 页面管理的表格数据
export * from './page'