export interface searchtype {
    count: number;
    createAt: string;
    id: string;
    keyword: string;
    type: string;
    updateAt: string;
}