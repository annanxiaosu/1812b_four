export interface WorkObject {
  id: string;
  title: string;
  cover?: string;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  totalAmount?: string;
  isPay: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  tags: any[];
  category?: any;
}