export * from './modules/Login'
// 用户管理
export * from './modules/user'
export * from './modules/workbench'
//文章管理
export * from './modules/articles'
//文件管理
export * from "./modules/file"
// 评论管理
export * from './modules/comment'
//统计管理
export * from "./modules/view"
//邮箱管理
export * from './modules/mail'
//搜索记录
export * from "./modules/search"

export * from './modules/knowLedge'

export * from './modules/article'

export * from './modules/category'
