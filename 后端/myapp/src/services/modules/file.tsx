import { request } from "umi";

//全部数据及搜索
export function fileApi(page: number, pageSize: number,params:{[key:string]:number|string}) {
    return request(`/api/file?page=${page}&pageSize=${pageSize}`, {params})
}

// https://creationapi.shbwyz.com/api/file/3206ed0c-d158-4f14-bf4d-964d8faa82b2
//删除
export function delfileApi(id:string) {
    return request(`/api/file/${id}`,{
        method:'DELETE'
    })
}

//上传
export function uploadApi(unique:string|number) {
    return request(`/api/file/upload?unique=${unique}`,{
        method:'POST'
    })
}