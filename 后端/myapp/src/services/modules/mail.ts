import { IReplyMail, smtp } from "@/types";
import { request } from "umi";

//邮箱总数据
export function mailApi(page: number, pageSize: number, params: { [key: string]: number | string }) {
    return request(`/api/smtp?page=${page}&pageSize=${pageSize}`, { params })
}

// 删除邮箱页
export function deleteMail(id: string) {
    return request(`/api/smtp/${id}`, {
        method: 'DELETE'
    })
}

//新建及发送邮件
export function AddMail(data: smtp){
    return request(`/api/smtp`,{
        method:'POST',
        data
    })
}

// 回复邮件
export function replyMail(data: IReplyMail) {
    return request('http://127.0.0.1:7001/mail', {
        method: 'POST',
        data
    })
}