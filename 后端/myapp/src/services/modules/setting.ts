import { request } from "umi";

//全部数据
export function settingApi(page: number, pageSize: number) {
    return request('/api/setting', {
        params: {
            page,
            pageSize
        }
    })
}