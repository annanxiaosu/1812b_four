import {request} from 'umi';


export function pageDatas(page=1,pageSize=12){
    return request(`/api/page?page=${page}&pageSize=${pageSize}`)
}