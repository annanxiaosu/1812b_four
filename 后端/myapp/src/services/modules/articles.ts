
import { request } from 'umi';
// 文章管理——所有文章接口
export function getArticleslist(page = 1, pageSize = 12, params: { [key: string]: string | boolean }) {
    return request(`/api/article?page=${page}&pageSize=${pageSize}`, { params });
}
// 文章管理——分类
export function getclasseslist() {
    return request('/api/category');
}
// 文章——状态管理
export function updateArticles(id: string, data: { [key: string]: string } = {}) {
    return request(`/api/article/${id}`, {
        method: 'PATCH',
        data
    })
}
// 文章——删除
export function deleteArticle(id: string) {
    return request(`/api/article/${id}`, {
        method: 'DELETE'
    })
}
// 文章-标签
export function getlabellist() {
    return request('/api/tag');
}