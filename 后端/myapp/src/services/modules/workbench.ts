import { request } from 'umi';
// 
export function getworkben(page = 1, pageSize =6 ) {
    return request(`/api/article?page=${page}&pageSize=${pageSize}`);
}