import { request } from 'umi';
// 用户管理——所有文章接口
export function getUserlist(page = 1, pageSize = 12, params: { [key: string]: string | boolean }) {
    return request(`/api/user?page=${page}&pageSize=${pageSize}`, { params });
}
// 用户管理——分类
export function getclasseslistUser() {
    return request('/api/user');
}
// 用户管理
export function updateUser(data: { [key: string]: string } = {}) {
    return request(`/api/user/update`, {
        method: 'POST',
        data
    })
}