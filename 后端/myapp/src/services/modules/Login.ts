import {ILoginFrom} from '@/types'
import { request } from 'umi'

// 登录接口
export function login(data:ILoginFrom){
    return request('/api/auth/login',{
        method:'POST',
        data
    })
} 