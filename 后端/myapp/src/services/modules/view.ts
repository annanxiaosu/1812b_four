import { request } from 'umi';

// 分页获取统计数据
export function getView(page=1, params:{[key: string]: string|boolean}, pageSize=8){
    return request(`/api/view?page=${page}&pageSize=${pageSize}`, { params})
}
// url 跳详情
// export function updateComment(id:string, data:{[key:string]:boolean}={}){
//     return request(`/api/view/${id}`, {
//         method: 'PATCH',
//         data
//     })
// }
// 删除评论
export function deleteView(id:string){
    return request(`/api/view/${id}`, {
        method: 'DELETE'
    })
}