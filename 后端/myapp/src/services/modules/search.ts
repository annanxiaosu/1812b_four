import { request } from "umi";


//总数据
export function searchApi(page:number,pageSize:number,params:{[key:string]:number|string}){
    return request(`/api/search?page=${page}&pageSize=${pageSize}`,{params})
}

// 删除搜索页
export function deleteSearch(id:string) {
    return request(`/api/search/${id}`, {
        method: 'DELETE'
    })
}