'use strict';

const Controller = require('egg').Controller;
let OSS = require('ali-oss');

let client = new OSS({
    region: '<Your region>',
    accessKeyId: 'LTAI5tQ4MV1Y4tu4kCQqQT2n',
    accessKeySecret: '<Your AccessKeySecret>'
});

class FileController extends Controller {
    async index() {
        const { ctx } = this;
        ctx.body = 'hi, egg';
        try {
            console.log(1);
        }
        catch {
            console.log(2);
        }
    }

    async create(){
        const {ctx}=this;
        client.useBucket('jason111');     
        const stream = await ctx.getFileStream();
        console.log('stream...', stream);
        try {
            let result = await client.put(stream.fieldname, stream);
            console.log(result);
            ctx.body = result;
        } catch (err) {
            console.log(err);
        }
    }

    //  获取oss存储桶列表
    async listBuckets() {
        const { ctx } = this;
        try {
            let result = await client.listBuckets();
            ctx.body = result;
        } catch (err) {
            console.log(err)
        }
    }
}

module.exports = FileController;
