'use strict';

const Controller = require('egg').Controller;
const nodemailer = require('nodemailer')

class MailController extends Controller {
    async index() {
        const { ctx } = this;
        ctx.body = 'hi, egg';
    }

    async getTransport() {
        let testAccount = await nodemailer.createTestAccount();

        let transporter = nodemailer.createTransport({
            host: "smtp.qq.com",
            port: 465,
            secure: true, // true for 465, false for other ports
            auth: {
                user: '210001837', // generated ethereal user
                pass: 'bbbvwmyjpybdbjgc', // generated ethereal password
            },
        });
        return transporter;
    }

    async create() {
        const { ctx } = this;
        let { to, subject, text, html } = ctx.request.body;

        try {
            let transporter = await this.getTransport();
            let info = await transporter.sendMail({
                from: '"Fred Foo 👻" <210001837@qq.com>', // sender address
                to: to||"2731218036@qq.com", // list of receivers
                subject: subject|| "Hello ✔", // Subject line
                text: text||"Hello world?", // plain text body
                html: html||"<b>Hello world?</b>", // html body
            });
            console.log(info);
            ctx.body={
                message:"邮件发送成功"
            }
        } catch(e) {
            ctx.body = {
                message: '邮件发送失败'
            };
        }
    }
}

module.exports = MailController;
