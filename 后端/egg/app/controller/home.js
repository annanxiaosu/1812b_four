'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.body = 'hi, egg';
    try {
      console.log(1);
    }
    catch {
      console.log(2);
    }
  }
}

module.exports = HomeController;
