'use strict';

const Controller = require('egg').Controller;
const AlipaySdk = require('alipay-sdk');
const AlipayFormData = require('alipay-sdk/lib/form');

// 普通公钥模式
const alipaySdk = new AlipaySdk({
  // 参考下方 SDK 配置
    appId: '2021000118609273',
  privateKey: 'MIIEowIBAAKCAQEAsvlp/Jcg6NoNRX4VmD2hn7CZTGYrbGxbEqy4AEWPyCNlVFoq+0CzAFq/gxDmXo9BBVChgXVLsqpQfuxmynhhPxy17tte8jWI8Oie / J5kRFvyQDqf6koN9Ib3Y2kAFkMFeZ7OtQO6O + svWIpBNnjeZ2qxPTw4Nfd8yzlt0lWtb91XCSzbyqxXcVMCmWd8uVDbW5BSNjjG2XiN9zFIq5i9yoppm4B2GoXcwAE3LrFjytZHCj8Mwgqy0UDjJrVA8eIvbI3vABzpW8hMOu9EMJwudAUB / Db5pWiHzCrMOCsCgVVoUAgqLc4iONMPoTa8tEnhye04Mcc1l1UAvrL8bX53dwIDAQABAoIBABHNe3xzsANZhU5vT0iYgNY4o9cUpqcfVCPUaG0mfjnjQ3YTjgj2E5gqbrhj2vZ2 + 3nC3n3ZJBF + nYkyjLsBJHM5sOM0kQo4xwlnqkDopIKkPsGi0ztj6GV1c3BBBpAwVIAXiuo4l7LUHx6IihoYBc91rnGv5st / dbtlWAY / HYRt7eW19Z6hPA4QSKTgsAd / dyRYfM5c08MdEZ5hZr9EMHQTwlPTHqNqDaD0fI3rI2KrdNpvVto / dgNnCAXlssz9IBF5HDx + dn7Sv + MbqPSVtlwYQrGpiJRKl71b7hYjR + mZvpbVrVIV4q4cr1fOwbPCqdNv86HtW7u / xxLxCCy087kCgYEA + Jc3mzar3JQ + Jd8fXdQJYMxpSlGUCa / 5WMfNdjCuGZRpHU + O8qlX5pfGdrAm5Hu4G9Vr / o4pn7wGHQmcYadXxR2fuqbW0oe7VXIxhGqHbpoWhy6D + hLvDvMXntFNt4OcOAwsucUH4hnZZyHs / j39bYMLVsNC + s9GkQQpho8F65UCgYEAuE8DdHg + a7umSjSiK4iBJBgt77V0oOPgzcUf / 4kifoB / r9KfEWGuGOkeTPJSDfvfPOvagWWzRpIvNjtYilzh8gplyuKw0zHHMWFcjcYOCVUs2TYRyUTUz / 1VfJEu0KuVmkVWx47vxJQGNOgidELXQAE59cc6OCVrg0vKuOmyc9sCgYABzEYwrzKLA993Mqfta80tkAzq5rAlacpp4 / gOdMetk6828Q5dnu / t0Qc0cI23dVWdjeucCbcvWpLIvvsMkcNGv7IaDX7cQvMN6SeGh6a3HBIxpBp8QorMFuIC2gyv2RnWJfBk35HxBrLV1h76r8QXeK0qUXs4iSs / fUI3oO0wCQKBgFUd2GCfb2RdgsxeRk6IeetrZzt + t1 / eHWGqGb7vXAjD2zQdtuxWCgrjDeVHO3HN0ijWW1DK4OJEfJ01BwzPHGGlP6FU04iVQoKhjhbDUpQoy898UvZ6XTW4rxQaEF65 / xsBzrPWZRCJs9wXIGJExBeA + X1yBocLcP1vsS14b7tlAoGBAKutcEgUk7UGO2a3Bx6 / TGTfZbbrP0JuANEzdzuYl3iKawCYqpd5hQP5SCItVEzQfmNG + ZHJeBzR4p4rz6cWfWADwXufxy / DKn + Y6vGQxrVAa1uO / lFbO + aYnXdpXdTu5UhG + dcML6KjgwVgHTzHYu0MVsaFnE6RA4An96wdaIZU',
  //可设置AES密钥，调用AES加解密相关接口时需要（可选）
  encryptKey: 'BPUmutEhVY9mQAj/egVbFg=='
});


class PayController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.body = 'hi, egg';
  }


  // 创建支付
  async create() {
    const { ctx } = this;
    let { totalAmount, id } = ctx.request.body
    console.log('totalAmount', totalAmount);
    const formData = new AlipayFormData();
    formData.setMethod('get');
    formData.addField('notifyUrl', 'http://www.com/notify');
    formData.addField('bizContent', {
      outTradeNo: 'out_trade_no',
      productCode: 'FAST_INSTANT_TRADE_PAY',
      totalAmount: totalAmount,
      subject: '商品',
      body: '商品详情',
    });

    alipaySdk.exec(
      'alipay.trade.page.pay',
      {},
      { formData: formData },
    ).then(result => {
      console.log(result);
      ctx.body = {data: result}
    }).catch(err => {
      ctx.body = {msg: err.toString() }
    })
  }
}

module.exports = PayController;
