'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  router.resources('/mail', controller.mail);//邮箱

  // Restful方式添加邮件路由
  router.resources('/pay', controller.pay);

  
  router.resources('/file', controller.file)

};
