# 安楠
## 2021.9.15
1. 文章阅读 
  - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)

2. 源码阅读 

3. leecode 刷题 
    - [字母异位词分组](https://leetcode-cn.com/problems/group-anagrams/)

4. 项目进度
    - [x] 
## 2021.9.14
1. 文章阅读
     - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读

3. leetcode刷题
    - [三数之和](https://leetcode-cn.com/problems/3sum/)
4. 项目进度
   - [x] 路由
## 2021.9.13    
1. 文章阅读
 - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读

3. leecode 刷题
     - [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)

4. 项目进度
    - [x] 小程序路由搭
## 2021.9.12
1. 文章阅读
 - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读

3. leecode 刷题
    - [删除链表的倒数第N个节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn2925/)

4. 项目进度
    - [x] 小程序路由搭建
## 2021.9.10
1. 文章阅读
     - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读 
   
3. LeeCode刷题 
    - [中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)
4. 项目进度 
    - [x] 知识小册
## 2021.9.9
1. 文章阅读
  - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)

4. 项目进度
    - [x] 添加功能
## 2021.9.8
1. 文章阅读
      - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读 
   
3. LeeCode刷题 
    - [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
4. 项目进度

## 2021.9.3
1. 文章阅读
  - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读 
   
3. LeeCode刷题 
    - [合并区间](https://leetcode-cn.com/leetbook/read/array-and-string/c5tv3/)
    
4. 项目进度  
 - [x] 右侧抽屉的实现和渲染

## 2021.9.2
1. 文章阅读
   - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读 
   
3. LeeCode刷题 
    - [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)
    
4. 项目进度  
 - [x] 搜索和重置
## 2021.9.1
1. 文章阅读
     - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读

3. leetcode刷题
    - [交错字符串](https://leetcode-cn.com/problems/interleaving-string/)

4. 项目进度
   - [x] 页面渲染排版
   - [x] 管理数据获取
## 2021.8.31
1. 文章阅读
   - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读

3. LeeCode刷题
    - [最长的回文字](https://leetcode-cn.com/problems/longest-palindromic-substring/)
   
4. 项目进度  
 - [x] 登录页面
 - [x] cookie储存信息

## 2021.8.30
1. 文章阅读
  - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读 

3. leecode 刷题
    - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
4. 项目进度
    - [x] 项目配置
## 2021.8.29
1. 文章阅读
    - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读 
3. LeeCode刷题 
  - [数组的操作](https://leetcode-cn.com/leetbook/read/array-and-string/yjcir/)
4. 项目进度  
 - [x] 后端页面的简单排版
 - [x] 路由的配置
## 2021.8.27
1. 文章阅读
    - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读 
3. LeeCode刷题 
   - [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
4. 项目进度  
 - [x] 优化页面
## 2021.8.26
1. 文章阅读
     - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读
3. LeeCode刷题
    - [删除数组重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
   
4. 项目进度  
 - [x] 支付宝沙盒支付
## 2021.8.25
1. 文章阅读
     - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读
 
3. LeeCode刷题
     - [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
   
4. 项目进度  
 - [x] 路由懒加载
 - [x] hash
## 2021.8.24
1. 文章阅读
    - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读
     - [_.chunk](https://www.lodashjs.com/docs/lodash.chunk)
3. LeeCode刷题
    - [rem](https://jasonandjay.github.io/study/zh/standard/Compatibility.html#rem%E8%AE%A1%E7%AE%97) 
   
4. 项目进度  
 - [x] 目录的切换
 - [x] 高亮
 - [x] 详情渲染
## 2021.8.23
1. 文章阅读
   - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读
  - [_.chunk](https://www.lodashjs.com/docs/lodash.chunk)
3. LeeCode刷题
   1. -[Ajax篇 跨域](https://jasonandjay.github.io/study/zh/book/Ajax%E7%AF%87.html)
   
4. 项目进度  
 - [x] 第二个详情页右侧文章列表切换
 - [x] 点赞
## 2021.8.22
1. 文章阅读
   - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)

2. 源码阅读
    - [_.chunk](https://www.lodashjs.com/docs/lodash.chunk)

3. leecode 刷题
   - [请你找出并返回这两个正序数组的 中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)

4. 项目进度
    - [x] 第二个详情页的渲染
    - [x] 右侧列表跳转和渲染

## 2021.8.20
1. 文章阅读
  - [Uimjs的使用](https://blog.csdn.net/qq_41579104/article/details/98481720)

2. 源码阅读
    - [_.chunk](https://www.lodashjs.com/docs/lodash.chunk)

3. leecode 刷题
    - [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)

4. 项目进度
    - [x] 详情页面再次跳转详情
    - [x] 详情页的排版

## 2021.8.19
1. 文章阅读
   - [Uimjs的使用](https://blog.csdn.net/qq_41579104/article/details/98481720)

2. 源码阅读
    - [_.chunk](https://www.lodashjs.com/docs/lodash.chunk)

3. leecode 刷题
   - [数组的操作](https://leetcode-cn.com/leetbook/read/array-and-string/yjcir/)

4. 项目进度
    - [x] 二次详情页的页面排版
    - [x] 点击阅读再次跳转路由
    - [x] 点击目录跳转路路由
    - [x] 封装右侧推荐文章组件
## 2021.8.18
1. 文章阅读
   - [Uimjs的使用](https://blog.csdn.net/qq_41579104/article/details/98481720)

2. 源码阅读
    - [_.chunk](https://www.lodashjs.com/docs/lodash.chunk)

3. leecode 刷题
   - [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)

4. 项目进度
    - [x] 详情页的页面排版
    - [x] 详情页的再次跳转详情
## 2021.8.17
1. 文章阅读
   - [Uimjs的使用](https://blog.csdn.net/qq_41579104/article/details/98481720)

2. 源码阅读
    - [_.chunk](https://www.lodashjs.com/docs/lodash.chunk)

3. leecode 刷题
    - [插入位置](https://leetcode-cn.com/leetbook/read/array-and-string/cxqdh/)

4. 项目进度
    - [x] 右侧文章推荐的列表
    - [x] 跳转详情
## 2021.8.16
1. 文章阅读
   - [Uimjs的使用](https://blog.csdn.net/qq_41579104/article/details/98481720)

2. 源码阅读
    - [_.chunk](https://www.lodashjs.com/docs/lodash.chunk)

3. leecode 刷题
    - [数组的操作](https://leetcode-cn.com/leetbook/read/array-and-string/yjcir/)

4. 项目进度
    - [x] 知识小册接口获取
    - [x] 知识小册列表渲染
## 2021.8.15
1. 文章阅读
   - [Uimjs的使用](https://blog.csdn.net/qq_41579104/article/details/98481720)

2. 源码阅读
    - [_.chunk](https://www.lodashjs.com/docs/lodash.chunk)

3. leecode 刷题
    - [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
     - [三数之和](https://leetcode-cn.com/problems/3sum/)

4. 项目进度
    - [x] 首页列表渲染
    - [x] 搭建框架
## 2021.8.13
1. 文章阅读
  - [Chrome阅读](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)

2. 源码阅读
   - [_.chunk](https://www.lodashjs.com/docs/lodash.chunk)

3. LeeCode刷题
   - [删除数组重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
   - [寻找中心索引](https://leetcode-cn.com/leetbook/read/array-and-string/yf47s/)

4. 项目进度
   - [x] 配置一级路由以及路由切换
   - [x] 渲染列表
## 2021.8.12
1. 文章阅读
   - [Chrome阅读](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
2. 源码阅读
   
3. LeeCode刷题
   - [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)
   - [只出现一次的数字](https://leetcode-cn.com/leetbook/read/tencent/x51e9r/)
4. 项目进度  
   [x] 创建项目  
   [x] 配置了一个路由

## 2021.8.11
1. 文章阅读
   
2. 源码阅读
   
3. LeeCode刷题
   - [两数之和](https://leetcode-cn.com/leetbook/read/tencent/xxqfy5/)
   - [请你找出并返回这两个正序数组的 中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)
4. 项目进度

# 霍婧苒

## 2021.9.15
1. 文章阅读 
    - 小程序
2. 源码阅读 
   - [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)
   - [lodash源码分析之arrayIncludes](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/arrayIncludes.md)
3. LeeCode刷题 
    - [提莫攻击](https://leetcode-cn.com/problems/teemo-attacking/)
    - [键盘行](https://leetcode-cn.com/problems/keyboard-row/)
4. 项目进度 
    - [x] 大牌优惠

## 2021.9.14
1. 文章阅读 
    - 小程序
2. 源码阅读 
   - [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)
   - [lodash源码分析之arrayIncludes](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/arrayIncludes.md)
3. LeeCode刷题 
    - [提莫攻击](https://leetcode-cn.com/problems/teemo-attacking/)
    - [键盘行](https://leetcode-cn.com/problems/keyboard-row/)
4. 项目进度 

## 2021.9.13
1. 文章阅读 
    - 小程序
2. 源码阅读 
   - [lodash源码分析之arrayIncludes](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/arrayIncludes.md)
3. LeeCode刷题 
    - [提莫攻击](https://leetcode-cn.com/problems/teemo-attacking/)
    - [键盘行](https://leetcode-cn.com/problems/keyboard-row/)
4. 项目进度 

## 2021.9.12
1. 文章阅读
    - oss
    - 小程序
2. 源码阅读 
   
3. LeeCode刷题 
    - [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)
    - [跳跃游戏](https://leetcode-cn.com/problems/jump-game/)
4. 项目进度 

## 2021.9.10
1. 文章阅读
    - oss
    - 小程序
2. 源码阅读 
   
3. LeeCode刷题 
    - [第N位数字](https://leetcode-cn.com/problems/nth-digit/)
    - [提莫攻击](https://leetcode-cn.com/problems/teemo-attacking/)
    - [键盘行](https://leetcode-cn.com/problems/keyboard-row/)
4. 项目进度 

## 2021.9.9
1. 文章阅读
    - oss
    
2. 源码阅读 
   
3. LeeCode刷题 
    - [第N位数字](https://leetcode-cn.com/problems/nth-digit/)
    - [提莫攻击](https://leetcode-cn.com/problems/teemo-attacking/)
    - [键盘行](https://leetcode-cn.com/problems/keyboard-row/)
4. 项目进度 
     

## 2021.9.8
1. 文章阅读
    - oss
    
2. 源码阅读 
   
3. LeeCode刷题 
    - [大的国家](https://leetcode-cn.com/problems/big-countries/)
    - [大礼包](https://leetcode-cn.com/problems/shopping-offers/)
    - [种花问题](https://leetcode-cn.com/problems/can-place-flowers/)
4. 项目进度 
    - [x] 用户管理
    - [x] 发布文章功能

## 2021.9.7
1. 文章阅读
    
2. 源码阅读 
   
3. LeeCode刷题 
    - [大的国家](https://leetcode-cn.com/problems/big-countries/)
    - [大礼包](https://leetcode-cn.com/problems/shopping-offers/)
    - [种花问题](https://leetcode-cn.com/problems/can-place-flowers/)
4. 项目进度 
    - [x] 用户管理
    - [x] 发布文章功能

## 2021.9.6
1. 文章阅读
    
2. 源码阅读 
   
3. LeeCode刷题 
    - [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
    - [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)
    - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
4. 项目进度 
    - [x] 用户管理
    - [x] 发布文章功能

## 2021.9.5
1. 文章阅读
    - modx
    - socket
2. 源码阅读 
   
3. LeeCode刷题 
   - [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)

4. 项目进度 
    - [x] 用户管理
    - [x] 发布文章功能

## 2021.9.3
1. 文章阅读
    - modx
    - socket
2. 源码阅读 
   
3. LeeCode刷题 
    - [回文](https://leetcode-cn.com/problems/palindrome-number/)
    - [中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)
    - [单词接龙](https://leetcode-cn.com/problems/word-ladder/)

4. 项目进度 
    - [x] 用户管理
    - [x] 发布文章功能
    
## 2021.9.2
1. 文章阅读
    - modx
    - socket
2. 源码阅读 
   
3. LeeCode刷题 
    - [回文](https://leetcode-cn.com/problems/palindrome-number/)
    - [中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)
    - [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
4. 项目进度 
    - [x] 用户管理
    - [x] 状态改变    

## 2021.9.1
1. 文章阅读
    - modx
     
2. 源码阅读 
   
3. LeeCode刷题 
    - [中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)
    - [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
4. 项目进度

## 2021.8.31
1. 文章阅读
    - [React状态管理一些思考（中篇）](https://juejin.cn/post/6997376981599780878)
    - [跳跃游戏](https://leetcode-cn.com/problems/jump-game/)
2. 源码阅读 
   
3. LeeCode刷题 
    - [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)
    - [跳跃游戏](https://leetcode-cn.com/problems/jump-game/)
    - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
4. 项目进度

## 2021.8.30
1. 文章阅读
    - [React状态管理一些思考（中篇）](https://juejin.cn/post/6997376981599780878)
2. 源码阅读 
   
3. LeeCode刷题 
    - [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)
    - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
4. 项目进度

## 2021.8.29
1. 文章阅读
    - [React状态管理一些思考（中篇）](https://juejin.cn/post/6997376981599780878)
2. 源码阅读 
   
3. LeeCode刷题 
    - [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)
4. 项目进度  

## 2021.8.27
1. 文章阅读
    - 支付宝
2. 源码阅读 
   
3. LeeCode刷题 
    - [合并区间](https://leetcode-cn.com/leetbook/read/array-and-string/c5tv3/)
    - [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)
4. 项目进度  
 

## 2021.8.26
1. 文章阅读
    - 支付宝
2. 源码阅读 
   
3. LeeCode刷题 
    - [合并区间](https://leetcode-cn.com/leetbook/read/array-and-string/c5tv3/)
    
4. 项目进度  
 - [x] 归档接口获取
 - [x] 归档列表渲染
 - [x] 详情渲染
 - [x] 回到顶部 点赞

## 2021.8.25
1. 文章阅读
    - 缓存
2. 源码阅读 
   
3. LeeCode刷题 
    - [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)
    
4. 项目进度  
 - [x] 归档接口获取
 - [x] 归档列表渲染
 - [x] 详情渲染
 - [x] 回到顶部 点赞

## 2021.8.24
1. 文章阅读
    - Hook
2. 源码阅读 
   
3. LeeCode刷题 
    - [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)
    - [三数之和](https://leetcode-cn.com/problems/3sum/)
4. 项目进度  
 - [x] 归档接口获取
 - [x] 归档列表渲染
 - [x] 详情渲染
 - [x] 回到顶部 点赞

## 2021.8.23
1. 文章阅读
    - Hook
2. 源码阅读 
   
3. LeeCode刷题 
    - [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)
    - [三数之和](https://leetcode-cn.com/problems/3sum/)
4. 项目进度  
 - [x] 归档接口获取
 - [x] 归档列表渲染
 - [x] 详情渲染
 - [x] 回到顶部 点赞

## 2021.8.22
1. 文章阅读
    - [前端知识体系(五)](https://juejin.cn/post/6994657097220620319)
2. 源码阅读 
   - [vue源码解读(一)](https://juejin.cn/post/6950084496515399717
3. LeeCode刷题 
   - [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
4. 项目进度  
 - [x] 归档接口获取
 - [x] 归档列表渲染
 - [x] 详情渲染
 - [x] 回到顶部 点赞

## 2021.8.20
1. 文章阅读
   - 项目 

2. 源码阅读
   - [vue源码解读(三)](https://juejin.cn/post/6950084496515399717)

3. LeeCode刷题
    - [插入位置](https://leetcode-cn.com/leetbook/read/array-and-string/cxqdh/)
    - [最长的回文字](https://leetcode-cn.com/problems/longest-palindromic-substring/)
   
4. 项目进度  
 - [x] 归档接口获取
 - [x] 归档列表渲染
 - [x] 详情渲染
 - [x] 回到顶部 点赞

## 2021.8.19
1. 文章阅读
   - 响应式
   - 单位 px rem em 百分比 vw/vh
2. 源码阅读
 
3. LeeCode刷题
    - [rem](https://jasonandjay.github.io/study/zh/standard/Compatibility.html#rem%E8%AE%A1%E7%AE%97) 
    - [寻找中心索引](https://leetcode-cn.com/leetbook/read/array-and-string/yf47s/)
   
4. 项目进度  
 - [x] 归档接口获取
 - [x] 归档列表渲染
 - [x] 详情渲染
 - [x] 回到顶部
 
## 2021.8.18
1. 文章阅读
   - 响应式
2. 源码阅读
 
3. LeeCode刷题
   1. -[响应式布局的原理](https://jasonandjay.github.io/study/zh/book/%E7%A7%BB%E5%8A%A8%E7%AB%AF%E7%AF%87.html#_22%EF%BC%8C%E5%93%8D%E5%BA%94%E5%BC%8F%E5%B8%83%E5%B1%80%E7%9A%84%E5%8E%9F%E7%90%86) 
   
4. 项目进度  
 - [x] 归档接口获取
 - [x] 归档列表渲染
 - [x] 详情渲染

## 2021.8.17
1. 文章阅读
   - 跨域
   - 详情
   - 上传文件
2. 源码阅读
 
3. LeeCode刷题
   1. -[存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
   2. -[只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
   
4. 项目进度  
 - [x] 归档接口获取
 - [x] 归档列表渲染
 - [x] 详情

## 2021.8.16
1. 文章阅读
   - 跨域
2. 源码阅读
 
3. LeeCode刷题
   1. -[Ajax篇 跨域](https://jasonandjay.github.io/study/zh/book/Ajax%E7%AF%87.html)
   
4. 项目进度  
 - [x] 归档接口获取
 - [x] 归档列表渲染

## 2021.8.15
1. 文章阅读
   - Hooks
2. 源码阅读
 
3. LeeCode刷题
   1. -[React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)
   
4. 项目进度  

## 2021.8.13
1. 文章阅读
   - umijs项目
   - Hooks
2. 源码阅读
 
3. LeeCode刷题
   1. -[umijs项目配置](https://umijs.org/zh-CN/docs/getting-started)
   2. -[api](https://api.blog.wipi.tech/api/#/)
   3. -[React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html)
4. 项目进度 
   

## 2021.8.12
1. 文章阅读
   1. 项目全生命周期
   2. -[项目管理](https://jasonandjay.github.io/study/zh/standard/Project.html#%E5%9B%BE%E8%A7%A3)
   
2. 源码阅读

3. LeeCode刷题
   1. -[TAPD 网址](https://www.tapd.cn/my_worktable?left_tree=1)
   2. 关联gitTab
   3. -[小楼又清风 (项目)](https://blog.wipi.tech/)
   4. -[umijs简介](https://umijs.org/zh-CN/docs/getting-started)
   5. -[hooks简介](https://jasonandjay.github.io/study/zh/standard/Hooks.html)

4. 项目进度 
   

## 2021.8.11
1. 文章阅读
   - chrome
   - gitTab
2. 源码阅读
   
3. LeeCode刷题
   - [基于Node 的 DevOps](https://juejin.cn/book/6948353204648148995)
   
4. 项目进度
    


# 许瑞杰

## 2021.9.15
1. 文章阅读
    - SVG 动画开发实战手册
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [香槟塔](https://leetcode-cn.com/problems/champagne-tower/)
    - [摘樱桃](https://leetcode-cn.com/problems/cherry-pickup/)

4. 项目进度
   - [x] 小程序

## 2021.9.14
1. 文章阅读
    - SVG 动画开发实战手册
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [掉落的方块](https://leetcode-cn.com/problems/falling-squares/)
    - [灯泡开关](https://leetcode-cn.com/problems/bulb-switcher-ii/)

4. 项目进度
   - [x] 小程序

## 2021.9.13
1. 文章阅读
    - SVG 动画开发实战手册
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [交错字符串](https://leetcode-cn.com/problems/interleaving-string/)
    - [异味词](https://leetcode-cn.com/problems/interleaving-string/)

4. 项目进度
   - [x] 页面渲染排版
   - [x] 管理数据获取
   - [x] 搜索

## 2021.9.12
1. 文章阅读
    - SVG 动画开发实战手册
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [砖墙](https://leetcode-cn.com/problems/brick-wall/)
    - [分糖果](https://leetcode-cn.com/problems/distribute-candies/)
    - [安装栅栏](https://leetcode-cn.com/problems/erect-the-fence/)

4. 项目进度
   - [x] 页面渲染排版
   - [x] 管理数据获取
   - [x] 搜索
   
## 2021.9.10
1. 文章阅读
    - SVG 动画开发实战手册
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [合并区间](https://leetcode-cn.com/leetbook/read/array-and-string/c5tv3/)
    - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

4. 项目进度
   - [x] 页面渲染排版
   - [x] 管理数据获取
   - [x] 搜索
   
## 2021.9.9
1. 文章阅读
    - SVG 动画开发实战手册
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [移除盒子](https://leetcode-cn.com/problems/remove-boxes/)
    - [检测大写字母](https://leetcode-cn.com/problems/detect-capital/)

4. 项目进度
   - [x] 页面渲染排版
   - [x] 管理数据获取
   - [x] 搜索
   
## 2021.9.8
1. 文章阅读
    - SVG 动画开发实战手册
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [构造矩形](https://leetcode-cn.com/problems/construct-the-rectangle/)
    - [对角遍历](https://leetcode-cn.com/problems/diagonal-traverse/)
    - [供暖器](https://leetcode-cn.com/problems/heaters/)

4. 项目进度
   - [x] 页面渲染排版
   - [x] 管理数据获取
   - [x] 搜索

## 2021.9.3
1. 文章阅读
    - SVG 动画开发实战手册
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [大的国家](https://leetcode-cn.com/problems/big-countries/)
    - [大礼包](https://leetcode-cn.com/problems/shopping-offers/)
    - [种花问题](https://leetcode-cn.com/problems/can-place-flowers/)

4. 项目进度
   - [x] 页面渲染排版
   - [x] 管理数据获取
   - [x] 搜索

## 2021.9.2
1. 文章阅读
    - SVG 动画开发实战手册
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [交错字符串](https://leetcode-cn.com/problems/interleaving-string/)
    - [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
    - [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)

4. 项目进度
   - [x] 页面渲染排版
   - [x] 管理数据获取
   - [x] 搜索

   
## 2021.9.1
1. 文章阅读
    - SVG 动画开发实战手册
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [第N位数字](https://leetcode-cn.com/problems/nth-digit/)
    - [提莫攻击](https://leetcode-cn.com/problems/teemo-attacking/)
    - [键盘行](https://leetcode-cn.com/problems/keyboard-row/)

4. 项目进度
   - [x] 页面渲染排版
   - [x] 管理数据获取
   - [x] 搜索
   
## 2021.8.31
1. 文章阅读
    - SVG 动画开发实战手册
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [pow](https://leetcode-cn.com/problems/powx-n/)
    - [跳跃游戏](https://leetcode-cn.com/problems/jump-game/)
    - [交错字符串](https://leetcode-cn.com/problems/interleaving-string/)

4. 项目进度
   - [x] 页面渲染排版
   - [x] 管理数据获取

## 2021.8.30
1. 文章阅读
    - SVG 动画开发实战手册
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
    - [数组和字符串](https://leetcode-cn.com/leetbook/detail/array-and-string/)

4. 项目进度
   - [x] 页面渲染排版

## 2021.8.29
1. 文章阅读
    - SVG 动画开发实战手册
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [DevOps](https://juejin.cn/book/6948353204648148995)
    - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

4. 项目进度
   - [x] 文章页面渲染排版
   - [x] 获取列表
   - [x] 路由传参
   - [x] 跳转详情
   - [x] 楼层效果

## 2021.8.27
1. 文章阅读
    - SVG 动画开发实战手册
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [最小栈](https://leetcode-cn.com/problems/min-stack/)
    - [多数元素](https://leetcode-cn.com/problems/majority-element/)
    - [重复序列](https://leetcode-cn.com/problems/repeated-dna-sequences/)

4. 项目进度
   - [x] 文章页面渲染排版
   - [x] 获取列表
   - [x] 路由传参
   - [x] 跳转详情
   - [x] 楼层效果

## 2021.8.26
1. 文章阅读
    - Umijs的使用，api等
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)
    - [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)

4. 项目进度
   - [x] 文章页面渲染排版
   - [x] 获取列表
   - [x] 路由传参
   - [x] 跳转详情
   - [x] 楼层效果


## 2021.8.25
1. 文章阅读
    - Umijs的使用，api等
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [单词接龙](https://leetcode-cn.com/problems/word-ladder/)
    - [重排列表](https://leetcode-cn.com/problems/reorder-list/)
    - [打家劫舍](https://leetcode-cn.com/problems/number-of-islands/)

4. 项目进度
   - [x] 文章页面渲染排版
   - [x] 获取列表
   - [x] 路由传参
   - [x] 跳转详情
   - [x] 楼层效果

## 2021.8.24
1. 文章阅读
    - Umijs的使用，api等
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [两个正序](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)
    - [删除数组重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
    - [字母异位](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)

4. 项目进度
   - [x] 文章页面渲染排版
   - [x] 获取列表
   - [x] 路由传参
   - [x] 跳转详情
   - [x] 楼层效果

## 2021.8.23
1. 文章阅读
    - Umijs的使用，api等
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [删除链表的倒数第N个节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn2925/)
    - [外观数列](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnpvdm/)
    - [回文](https://leetcode-cn.com/problems/palindrome-number/)

4. 项目进度
   - [x] 文章页面渲染排版
   - [x] 获取列表
   - [x] 路由传参
   - [x] 跳转详情
   - [x] 楼层效果

## 2021.8.22
1. 文章阅读
    - Umijs的使用，api等
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [跳跃游戏](https://leetcode-cn.com/problems/jump-game/)
    - [爬楼梯](https://leetcode-cn.com/problems/climbing-stairs/)
    - [简单的树](https://leetcode-cn.com/problems/same-tree/)
4. 项目进度
   - [x] 文章页面渲染排版
   - [x] 获取列表
   - [x] 路由传参
   - [x] 跳转详情
   - [x] 楼层效果

## 2021.8.20
1. 文章阅读
    - Umijs的使用，api等
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [搜索插入位置](https://leetcode-cn.com/problems/search-insert-position/)
    - [旋转图像](https://leetcode-cn.com/problems/rotate-image/)
    - [解数独](https://leetcode-cn.com/problems/sudoku-solver/)
4. 项目进度
   - [x] 文章页面渲染排版
   - [x] 获取列表
   - [x] 路由传参
   - [x] 跳转详情
   - [x] 楼层效果

## 2021.8.19
1. 文章阅读
    - Umijs的使用，api等
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [z字形变化](https://leetcode-cn.com/problems/zigzag-conversion/)
    - [罗马数字转整数](https://leetcode-cn.com/problems/roman-to-integer/)
    - [三数之和](https://leetcode-cn.com/problems/3sum/)
4. 项目进度
   - [x] 文章页面渲染排版
   - [x] 获取列表
   - [x] 路由传参
   - [x] 跳转详情
   - [x] 楼层效果

## 2021.8.18
1. 文章阅读
    - Umijs的使用，api等
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [插入位置](https://leetcode-cn.com/leetbook/read/array-and-string/cxqdh/)
    - [删除数组重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
4. 项目进度
   - [x] 文章页面渲染排版
   - [x] 获取列表
   - [x] 路由传参
   - [x] 跳转详情
   - [x] 楼层效果

## 2021.8.17
1. 文章阅读
    - Umijs的使用，api等
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [数组的操作](https://leetcode-cn.com/leetbook/read/array-and-string/yjcir/)
    - [api](https://api.blog.wipi.tech/api/#/)
4. 项目进度
   - [x] 文章页面渲染排版
   - [x] 获取列表
   - [x] 路由传参
   - [x] 跳转详情

## 2021.8.16
1. 文章阅读
    - Umijs的使用，api等
    - HTML游戏开发快速提升
2. 源码阅读

3. leetcode刷题
    - [整数反转](https://leetcode-cn.com/leetbook/read/tencent/x5s2oj/)
    - [三数之和](https://leetcode-cn.com/problems/3sum/)
4. 项目进度
   - [x] 文章页面渲染排版
   - [x] 获取列表

## 2021.8.15
1. 文章阅读
    - Umijs 官网阅读
    - Hooks 阅读
2. 源码阅读

3. leetcode刷题
    - [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
    - [最长的回文字](https://leetcode-cn.com/problems/longest-palindromic-substring/)
4. 项目进度

## 2021.8.13
1. 文章阅读
    - Chrome的调试技巧
    - Umijs 官网阅读

2. 源码阅读

3. leetcode刷题
    - [合并区间](https://leetcode-cn.com/leetbook/read/array-and-string/c5tv3/)
    - [数组的操作](https://leetcode-cn.com/leetbook/read/array-and-string/yjcir/)
    - [插入位置](https://leetcode-cn.com/leetbook/read/array-and-string/cxqdh/)
4. 项目进度

## 2021.8.12
1. 文章阅读
    - Chrome的调试技巧
    - Umijs 官网阅读

2. 源码阅读

3. leetcode刷题
    - [整数反转](https://leetcode-cn.com/problems/reverse-integer/)
    - [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
    - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
4. 项目进度

## 2021.8.11
1. 文章阅读
- Chrome的调试技巧

2. 源码阅读

3. leetcode 刷题
- [两数之和](https://leetcode-cn.com/leetbook/read/tencent/xxqfy5/)
- [请你找出并返回这两个正序数组的 中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)

4. 项目进度
# 秦宇龙

## 2021.9.15
1. 文章阅读
   - [前端缓存](https://juejin.cn/post/6844903747357769742#heading-5)
   - [函数式编程](https://juejin.cn/post/7000530780057239565)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度

## 2021.9.14
1. 文章阅读
   - [最新的前端大厂面经（详解答案）](https://juejin.cn/post/7004638318843412493#heading-48)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度
    - [x] 邮箱回复
    - [x] 抽屉

## 2021.9.13
1. 文章阅读
   - [最新的前端大厂面经（详解答案）](https://juejin.cn/post/7004638318843412493#heading-48)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度
    - [x] 邮箱回复
    - [x] 抽屉


## 2021.9.12
1. 文章阅读
   - [最新的前端大厂面经（详解答案）](https://juejin.cn/post/7004638318843412493#heading-48)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度
    - [x] 邮箱回复
    - [x] 抽屉


## 2021.9.10
1. 文章阅读
   - [最新的前端大厂面经（详解答案）](https://juejin.cn/post/7004638318843412493#heading-48)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度
    - [x] 邮箱回复
    - [x] 抽屉

## 2021.9.9
1. 文章阅读
   - [最新的前端大厂面经（详解答案）](https://juejin.cn/post/7004638318843412493#heading-48)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度
    - [x] 邮箱回复
    - [x] 抽屉

## 2021.9.8
1. 文章阅读
   - [PC端react实现一键复制图片功能](https://juejin.cn/post/7005121781057421320)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度
    - [x] 邮箱页新建
    - [x] 抽屉


## 2021.9.3
1. 文章阅读
   - [webtoken](https://juejin.cn/post/6844903747357769742#heading-5)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度
    - [x] 邮箱页排版
    - [x] 文件排版
    - [x] 文件获取数据

## 2021.9.2
1. 文章阅读
   - [前端缓存](https://juejin.cn/post/6844903747357769742#heading-5)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度
    - [x] 邮箱页排版
    - [x] 文件排版
    - [x] 文件获取数据

## 2021.9.1
1. 文章阅读
   - [前端缓存](https://juejin.cn/post/6844903747357769742#heading-5)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度
    - [x] 邮箱页排版
    - [x] 文件排版
    - [x] 文件获取数据

## 2021.8.31
1. 文章阅读
   - [前端缓存(四)](https://juejin.cn/post/6844903747357769742#heading-5)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度
    - [x] 邮箱页排版
    - [x] 文件排版
    - [x] 文件获取数据

## 2021.8.30
1. 文章阅读
   - [前端缓存(三)](https://juejin.cn/post/6844903747357769742#heading-5)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度

## 2021.8.29
1. 文章阅读
   - [前端缓存(二)](https://juejin.cn/post/6844903747357769742#heading-5)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度



## 2021.8.27
1. 文章阅读
   - [前端缓存](https://juejin.cn/post/6844903747357769742#heading-5)
   - [函数式编程](https://juejin.cn/post/7000530780057239565)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度

## 2021.8.26
1. 文章阅读
   - [什么是前端路由](https://juejin.cn/post/6844903890278694919#heading-10)
   - [强制缓存和协商缓存](https://juejin.cn/post/6844903838768431118)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [二叉树的最大深度](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/)
    - [验证二叉搜索树](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/)

4. 项目进度


## 2021.8.25
1. 文章阅读
   - [浏览器缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html#%E8%B5%84%E6%BA%90%E8%AF%B7%E6%B1%82%E5%88%86%E7%B1%BB)
   - [React状态管理一些思考（中篇）](https://juejin.cn/post/6997376981599780878)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [删除链表的倒数第N个节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn2925/)
    - [反转链表](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnnhm6/)
    - [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)

4. 项目进度
    - [x] 完成排版
    - [x] 合并
    - [x] 组件封装

## 2021.8.24
1. 文章阅读
   - [React Hook 系列(二)：彻底搞懂react hooks 用法（长文慎点）](https://juejin.cn/post/6844904018817318926#heading-1)
   - [React状态管理一些思考（中篇）](https://juejin.cn/post/6997376981599780878)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [删除链表的倒数第N个节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn2925/)
    - [反转链表](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnnhm6/)
    - [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)

4. 项目进度
    - [x] 完成排版
    - [x] 合并
    - [x] 组件封装

## 2021.8.23
1. 文章阅读
   - [React Hook 系列(一)：彻底搞懂react hooks 用法（长文慎点）](https://juejin.cn/post/6844904018817318926#heading-1)
   - [React状态管理一些思考（中篇）](https://juejin.cn/post/6997376981599780878)

2. 源码阅读
    - [react源码解读(四)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [删除链表的倒数第N个节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn2925/)
    - [反转链表](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnnhm6/)
    - [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)

4. 项目进度
    - [x] 完成排版
    - [x] 合并
    - [x] 组件封装

## 2021.8.22
1. 文章阅读
   - [聊一聊 React 中更新 ui 视图的几种方式](https://zhuanlan.zhihu.com/p/46140569)
   - [彻底弄懂 CSS 必考题（一）](https://juejin.cn/post/6999160403066355743)

2. 源码阅读
    - [react源码解读(三)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [删除链表的倒数第N个节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn2925/)
    - [反转链表](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnnhm6/)
    - [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)

4. 项目进度
    - [x] bug解决
    - [x] 合并冲突
    - [x] 组件封装

## 2021.8.20
1. 文章阅读
   - [React状态管理的一些思考（上篇接）](https://juejin.cn/post/6995497136510992414)
   - [22个高频JavaScript手写代码总结了解](https://juejin.cn/post/6996289669851774984)

2. 源码阅读
    - [react源码解读(二)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [删除链表的倒数第N个节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn2925/)
    - [反转链表](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnnhm6/)
    - [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)

4. 项目进度
    - [x] bug解决
    - [x] 合并冲突
    - [x] 组件封装

## 2021.8.19
1. 文章阅读
   - [React状态管理的一些思考（上篇）](https://juejin.cn/post/6995497136510992414)
   - [22个高频JavaScript手写代码总结了解](https://juejin.cn/post/6996289669851774984)

2. 源码阅读
    - [react源码解读](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [外观数列](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnpvdm/)
    - [最长公共前缀](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnmav1/)
    - [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)

4. 项目进度
    - [x] 搜索功能bug解决
    - [x] 文章右侧点击跳入
    - [x] 文章标签中间跳转

## 2021.8.18
1. 文章阅读
   - [前端知识体系(五)](https://juejin.cn/post/6994657097220620319)
   - [22个高频JavaScript手写代码总结了解(三)](https://juejin.cn/post/6996289669851774984)

2. 源码阅读
    - [vue源码解读(三)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)
    - [字符串转换整数 (atoi)](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnoilh/)
    - [实现 strStr()](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnr003/)

4. 项目进度
    - [x] 搜索功能
    - [x] 文章标签
    - [x] 跳转路由

## 2021.8.17
1. 文章阅读
   - [前端知识体系(四)](https://juejin.cn/post/6994657097220620319)
   - [22个高频JavaScript手写代码总结了解(二)](https://juejin.cn/post/6996289669851774984)

2. 源码阅读
    - [vue源码解读(二)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
    - [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)
    - [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)

4. 项目进度
    - [x] 顶部的排版
    - [x] 中间排版
    - [x] 底部排版


## 2021.8.16
1. 文章阅读
   - [前端知识体系(三)](https://juejin.cn/post/6994657097220620319)
   - [22个高频JavaScript手写代码总结了解(一)](https://juejin.cn/post/6996289669851774984)

2. 源码阅读
    - [vue源码解读(一)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [有效的数独](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2f9gg/)
    - [旋转图像](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhhkv/)
    - [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

4. 项目进度
    - [x] 顶部的排版
    - [x] 中间排版
    - [x] 底部排版

## 2021.8.15 
1. 文章阅读
   - [前端知识体系(二)](https://juejin.cn/post/6994657097220620319)
   - [Vue3 源码中那些实用的基础工具函数(二)](https://juejin.cn/post/6994976281053888519)

2. 源码阅读
    - [vue源码解读(三)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
    - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
    - [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)

4. 项目进度
    - [x] 搭建项目
    - [x] 首页修改

## 2021.8.13 
1. 文章阅读
   - [前端知识体系(一)](https://juejin.cn/post/6994657097220620319)
   - [Vue3 源码中那些实用的基础工具函数(一)](https://juejin.cn/post/6994976281053888519)

2. 源码阅读
    - [vue源码解读(二)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
    - [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
    - [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)

4. 项目进度
    - [x] 搭建项目
    - [x] 首页修改

## 2021.8.12
1. 文章阅读
    - [Chrome 调试技巧(二)](https://juejin.cn/book/6844733783166418958)
    - [精通React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E7%AE%80%E4%BB%8B)

    <!-- - [前端知识体系(一)](https://juejin.cn/post/6994657097220620319) -->

2. 源码阅读
    - [vue源码解读(初始化,简介)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
    - [买卖股票的最佳时机 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
    - [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)

4. 项目进度
    - [x] 项目关联
    - [x] 分配任务
    - [x] 创建需求

## 2021.8.11
1.文章阅读
  - [Chrome 调试技巧](https://juejin.cn/book/6844733783166418958)

2.源码阅读


3.leecode 刷题


4.项目进度
  - [x] 项目
 


# 张喜丘
## 2021 9.15
1. 文章阅读
- [React状态管理的一些思考（上篇接）](https://juejin.cn/post/6995497136510992414)
2. 源码阅读
3. LeeCode刷题
- [外观数列](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnpvdm/)
4. 项目进度
-  抓包 获取数据
## 2021 9.14
1. 文章阅读
  - [前端缓存](https://juejin.cn/post/6844903747357769742#heading-5)
2. 源码阅读
 - [react源码解读(六)](https://juejin.cn/post/6950084496515399717)
3. LeeCode刷题
- - [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)
4. 项目进度
-  了解小程序的路由
## 2021 9.13
1. 文章阅读
 - [实现 strStr(三)](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnr003/)
2. 源码阅读
 - [react源码解读(六)](https://juejin.cn/post/6950084496515399717)
3. LeeCode刷题
- [x字形变化](https://leetcode-cn.com/problems/zigzag-conversion/)
4. 项目进度
- 小程序 分享功能
- 小程序 上拉刷新 下拉触底功能
## 2021 9.12
1. 文章阅读
 - [实现 strStr(二)](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnr003/)
2. 源码阅读
 - [react源码解读(五)](https://juejin.cn/post/6950084496515399717)
3. LeeCode刷题
- [z字形变化](https://leetcode-cn.com/problems/zigzag-conversion/)
4. 项目进度
-  了解小程序的目录
## 2021 9.10
1. 文章阅读
 - [实现 strStr()](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnr003/)
2. 源码阅读
 - [react源码解读(四)](https://juejin.cn/post/6950084496515399717)
3. LeeCode刷题
 - [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)
4. 项目进度
-  OSS案例
## 2021 9.9
1. 文章阅读
2. 源码阅读
  - [vue源码解读(初始化,简介)](https://juejin.cn/post/6950084496515399717)
3. LeeCode刷题
 - [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/
4. 项目进度
- 发送邮件
## 2021 9.8
1. 文章阅读
2. 源码阅读
   - [Chrome 调试技巧](https://juejin.cn/book/6844733783166418958)
3. LeeCode刷题
  - [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)
4. 项目进度
- 登录注册页面
- 存储状态
## 2021 9.5
1. 文章阅读
2. 源码阅读
  - [22个高频JavaScript手写代码总结了解(五)](https://juejin.cn/post/6996289669851774984)
3. LeeCode刷题
  - [合并区间](https://leetcode-cn.com/leetbook/read/array-and-string/c5tv3/)
4. 项目进度
- 百度统计的应用
## 2021 9.3
1. 文章阅读
2. 源码阅读
  - [22个高频JavaScript手写代码总结了解(四)](https://juejin.cn/post/6996289669851774984)
3. LeeCode刷题
  - [强制缓存和协商缓存](https://juejin.cn/post/6844903838768431118)
4. 项目进度
- 统计管理页面标签渲染
- 获取统计管理页面的数据
- 点击分页功能
## 2021 9.2
1. 文章阅读
2. 源码阅读
  - [22个高频JavaScript手写代码总结了解(三)](https://juejin.cn/post/6996289669851774984)
3. LeeCode刷题
  - [聊一聊 React 中更新 ui 视图的几种方式](https://zhuanlan.zhihu.com/p/46140569)
4. 项目进度
- 统计管理页面样式
- 删除功能
- 简单的跳详情
## 2021 9.1
1. 文章阅读
   - 有关短连接 知识点 
2. 源码阅读
  - [22个高频JavaScript手写代码总结了解(二)](https://juejin.cn/post/6996289669851774984)
3. LeeCode刷题
  - [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)
4. 项目进度
- 搜索功能
- 获取数据接口
- 完成状态的设置
## 2021 8.31
1. 文章阅读
   - 有useForm新的钩子函数 知识点 
2. 源码阅读
    
3. LeeCode刷题
   - [22个高频JavaScript手写代码总结了解(一)](https://juejin.cn/post/6996289669851774984)
4. 项目进度
- 重置功能
- 回复弹窗功能
- 删除功能
- 点击文字出现内容
## 2021 8.30
1. 文章阅读
   - 有新的内置环境的 知识点 
2. 源码阅读
    
3. LeeCode刷题
    - [vue源码解读(初始化,简介)](https://juejin.cn/post/6950084496515399717)
4. 项目进度
- 小楼又清风后端样式排版
- 全选功能 和 反选功能
## 2021 8.29
1. 文章阅读
   - 有关支百度统计的代码 知识点 
2. 源码阅读
    
3. LeeCode刷题
       - [有效的数独](https://leetcode-cn.com/problems/valid-sudoku/)
4. 项目进度
## 2021 8.27
1. 文章阅读
   - 有关支付宝支付的代码 知识点 
2. 源码阅读
    
3. LeeCode刷题
       - [有效的数独](https://leetcode-cn.com/problems/valid-sudoku/)
4. 项目进度
## 2021 8.26
1. 文章阅读
   - 有关路由原生的代码 知识点 
2. 源码阅读
    
3. LeeCode刷题
       - [旋转图像](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhhkv/)
4. 项目进度
  - 和代码 解决样式问题 和报错问题
## 2021 8.25
1. 文章阅读
2. 源码阅读
    - 阅读有关缓存的内容
3. LeeCode刷题
      - [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
4. 项目进度
  - 推荐阅读的样式 和原版 改进一样
## 2021 8.24
1. 文章阅读
2. 源码阅读
    - 阅读有关xftp上传文件的内容
3. LeeCode刷题
      - [买卖股票的最佳时机 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
4. 项目进度
  - 钻研 回复框内容(2)
## 2021 8.23
1. 文章阅读
2. 源码阅读
    - 阅读有关hook 钩子函数的源码
3. LeeCode刷题
     - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
4. 项目进度
  - 钻研 回复框内容
## 2021 8.22
1. 文章阅读
2. 源码阅读
    
3. LeeCode刷题
    - [俩数相加](https://leetcode-cn.com/problems/add-two-numbers/)
4. 项目进度
  - 钻研 回复框内容
## 2021 8.20
1. 文章阅读
2. 源码阅读
    
3. LeeCode刷题
    - [俩数之和](https://leetcode-cn.com/problems/two-sum/comments/)
    - [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
4. 项目进度
  - 进行收起功能
  - 完善样式布局
  - 推荐阅读---跳详情 封装组件
## 2021 8.19
1. 文章阅读
   - 响应式布局
2. 源码阅读
    
3. LeeCode刷题
    - [回文](https://leetcode-cn.com/problems/palindrome-number/)
    - [括号的生成](https://leetcode-cn.com/problems/generate-parentheses/)
4. 项目进度
  - 进行回复功能（2）
  - 完善样式布局
  - 推荐阅读---跳详情
  - 完善详情页面
## 2021 8.18
1. 文章阅读
   - 多种传参方式 上传文件（2）
2. 源码阅读
    
3. LeeCode刷题
    - [合并区间](https://leetcode-cn.com/leetbook/read/array-and-string/c5tv3/)
    - [数组的操作](https://leetcode-cn.com/leetbook/read/array-and-string/yjcir/)
4. 项目进度
  - 进行回复功能
  - 完善样式布局
## 2021 8.17
1. 文章阅读
   - 多种传参方式 上传文件
2. 源码阅读
    
3. LeeCode刷题
    - [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
    - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
4. 项目进度
  - 点击出现登录页面 登录之后 进行评论 发布
  - 渲染了推荐阅读页面
## 2021 8.16
1. 文章阅读
   - 查看关于 umi 相关的官网内容 以及钩子函数
2. 源码阅读
    
3. LeeCode刷题
    - [寻找中心索引](https://leetcode-cn.com/leetbook/read/array-and-string/yf47s/)
    - [基于Node 的 DevOps](https://juejin.cn/book/6948353204648148995)
4. 项目进度
  - 渲染了页面图片和文字
  - 渲染了评论区
  - 点击出现登录页面 登录之后 进行评论 发布
## 2021 8.15
1. 文章阅读
   - 查看关于 umi 相关的官网内容
2. 源码阅读
   
3. LeeCode刷题
    - [买卖股票的最佳时机 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/)
    - [旋转数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/)
4. 项目进度
## 2021 8.13
1. 文章阅读
   - 查看look 和 源码
2. 源码阅读
   
   - [_.chunk](https://www.lodashjs.com/docs/lodash.chunk)
3. LeeCode刷题
   - [盛多水的杯子](https://leetcode-cn.com/problems/container-with-most-water/)
   - [三数之和](https://leetcode-cn.com/problems/3sum/)
4. 项目进度

## 2021.8.12
1. 文章阅读
   - 查看look
2. 源码阅读
    
3. LeeCode刷题
   - [无重复字符的长度](https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/)
   - [最长的回文字](https://leetcode-cn.com/problems/longest-palindromic-substring/)
4. 项目进度
  [x] 创建项目  

## 2021.8.11
1. 文章阅读
   
2. 源码阅读
   
3. LeeCode刷题
   - [两数之和](https://leetcode-cn.com/leetbook/read/tencent/xxqfy5/)
   - [请你找出并返回这两个正序数组的 中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)
4. 项目进度


# 苏亚茹
## 2021.9.15
1. 文章阅读
   - [前端知识体系(一)](https://juejin.cn/post/6994657097220620319)

2. 源码阅读

3. leecode 刷题
    - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

4. 项目进度
    - [x]
## 2021.9.14
1. 文章阅读
    - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)

2. 源码阅读 
    - [生产环境js错误收集及定位源码位置](https://juejin.cn/post/6960108736966819848)
3. LeeCode刷题 
    - [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
4. 项目进度
    - [x]搭建小程序路由

## 2021.9.13
1. 文章阅读 
   - [一万字ES6的class类](https://juejin.cn/post/7000891889465425957)

2. 源码阅读 
   - [lodash源码分析之arrayIncludes](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/arrayIncludes.md)

3. leecode 刷题 
    - [数组的操作](https://leetcode-cn.com/leetbook/read/array-and-string/yjcir/)

4. 项目进度
    - [x]路由
## 2021.9.12
1. 文章阅读
   - [React状态管理一些思考（中篇）](https://juejin.cn/post/6997376981599780878)

2. 源码阅读
    - [react源码解读(四)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

4. 项目进度
    - [x] 

## 2021.9.10
1. 文章阅读
   - [React状态管理一些思考（中篇）](https://juejin.cn/post/6997376981599780878)

2. 源码阅读
    - [react源码解读(四)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)

4. 项目进度
    - [x] 

## 2021.9.8
1. 文章阅读
   - [前端缓存](https://juejin.cn/post/6844903747357769742#heading-5)
2. 源码阅读
   - [说说JavaScript中的数据类型？存储上的差别？](https://github.com/dajin1234/web-interview/blob/master/docs/JavaScript/data_type.md)
3. LeeCode刷题
    - [删除链表的倒数第N个节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn2925/)
   
4. 项目进度  
 - [x] 页面管理页面排版
 - [x] 页面管理页面数据获取


## 2021.9.8
1. 文章阅读
   - [看破JS底层——实现a==1、2、3](https://juejin.cn/post/6948257149625729055)
2. 源码阅读
   - [说说JavaScript中的数据类型？存储上的差别？](https://github.com/dajin1234/web-interview/blob/master/docs/JavaScript/data_type.md)
3. LeeCode刷题
   - [单词拆分](https://leetcode-cn.com/leetbook/read/top-interview-questions/xa9v8i/)
   
4. 项目进度  
 - [x] 

## 2021.9.3
1. 文章阅读
   - [深入点了解JSON.stringify使用](https://juejin.cn/post/6974554346562256926)
2. 源码阅读
    - [生产环境js错误收集及定位源码位置](https://juejin.cn/post/6960108736966819848)

3. LeeCode刷题
   - [ 全排列](https://leetcode-cn.com/problems/permutations/)
   
4. 项目进度  
 - [x] 答辩


## 2021.9.2
1. 文章阅读
   - [WebSocket廖雪峰的官方网站](https://www.liaoxuefeng.com/wiki/1022910821149312/1103303693824096)
2. 源码阅读
   - [ MindSpore 人脸识别](https://juejin.cn/post/6965741051646574628)
3. LeeCode刷题
   - [排序](https://leetcode-cn.com/leetbook/read/illustration-of-algorithm/phn3m1/)
   
4. 项目进度  
 - [x] 最新评论的功能实现
 - [x] 最新评论数据重新排版排版


## 2021.9.1
1. 文章阅读
   - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)
2. 源码阅读

3. LeeCode刷题
    - [最长的回文字](https://leetcode-cn.com/problems/longest-palindromic-substring/)
   
4. 项目进度  
 - [x] 最新评论数据获取
 - [x] 最新评论数据排版


## 2021.8.31
1. 文章阅读
    - [Mobx-react](http://wddsss.com/main/displayArticle/350)

2. 源码阅读
   - [lodash源码分析之arrayIncludes](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/arrayIncludes.md)

3. LeeCode刷题
    - [字母异位词分组](https://leetcode-cn.com/problems/group-anagrams/)
   
4. 项目进度  
 - [x] 数据的获取
 - [x] 修改页面


## 2021.8.30
1. 文章阅读 
   - [一万字ES6的class类](https://juejin.cn/post/7000891889465425957)

2. 源码阅读 
   - [lodash源码分析之arrayIncludes](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/arrayIncludes.md)

3. leecode 刷题 
    - [数组的操作](https://leetcode-cn.com/leetbook/read/array-and-string/yjcir/)

4. 项目进度
    - [x]工作台排版
    - [x]工作台跳转页面

## 2021.8.29
1. 文章阅读 
   - [一万字ES6的class类](https://juejin.cn/post/7000891889465425957)

2. 源码阅读 
   - [lodash源码分析之arrayIncludes](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/arrayIncludes.md)

3. leecode 刷题 
   - [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

4. 项目进度


## 2021.8.27
1. 文章阅读 
   - [typescript](https://juejin.cn/book/6844733813021491207/section/)

2. 源码阅读 
    - [vue源码解读(初始化,简介)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题 
    - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

4. 项目进度


## 2021.8.26
1. 文章阅读 
   - ​[alipay-sdk](https://www.npmjs.com/package/alipay-sdk)

2. 源码阅读 
    - [vue源码解读(初始化,简介)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题 
    - [合并区间](https://leetcode-cn.com/leetbook/read/array-and-string/c5tv3/)

4. 项目进度


## 2021.8.25
1. 文章阅读 
   - ​[前端浏览器缓存知识梳理](https://juejin.cn/post/6947936223126093861)

2. 源码阅读 
   - [源码看React setState漫谈（一）](https://segmentfault.com/a/1190000011170740)

3. leecode 刷题 
    - [字母异位词分组](https://leetcode-cn.com/problems/group-anagrams/)

4. 项目进度
    - [x] 代码修缮

## 2021.8.24
1. 文章阅读
   - [深入浅出Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813088583687)

2. 源码阅读
   - [lodash源码分析之arrayIncludes](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/arrayIncludes.md)

3. leecode 刷题
    - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

4. 项目进度
    - [x] 留言板完善
    - [x] 钻研留言板回复


## 2021.8.23
1. 文章阅读
    - [Hooks很强大，为何还需要Mobx](https://juejin.cn/post/6930758273863778317)

2. 源码阅读
   - [lodash源码分析之arrayIncludes](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/arrayIncludes.md)

3. LeeCode刷题
    - [字母异位词分组](https://leetcode-cn.com/problems/group-anagrams/)
   
4. 项目进度  
 - [x] 留言板排版修缮
 - [x] 留言板回复


## 2021.8.22
1. 文章阅读
   - [typescript](https://juejin.cn/book/6844733813021491207/section/)

2. 源码阅读
   - [vue源码解读(三)](https://juejin.cn/post/6950084496515399717)

3. LeeCode刷题
    - [插入位置](https://leetcode-cn.com/leetbook/read/array-and-string/cxqdh/)
   
4. 项目进度  
 - [x] 回到顶部 点留言板排版修缮
 - [x] 留言板点击评论框弹出框
 - [x] 验证评论框输入是否正确


## 2021.8.20
1. 文章阅读
   - [看破JS底层——实现a==1、2、3](https://juejin.cn/post/6948257149625729055)
   - [React Router 入门完全指南(包含 Router Hooks)](https://juejin.cn/post/6948226424427773983)

2. 源码阅读
   - [lodash源码分析之arrayIncludes](https://github.com/yeyuqiudeng/pocket-lodash/blob/master/internal/arrayIncludes.md)

3. leecode 刷题
   - [理清重要问题类型](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmted6/)

4. 项目进度
    - [x] 留言板排版修缮
    - [x] 留言板封装评论组件
    - [x] 留言板评论组件修改


## 2021.8.19
1. 文章阅读
   - [ts+react+hooks](https://blog.csdn.net/qq_44983621/article/details/109261287)

2. 源码阅读
   - [基于Node 的 DevOps](https://juejin.cn/book/6948353204648148995)

3. leecode 刷题
  - [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

4. 项目进度
    - [x] 留言板分页完善
    - [x] 留言板排版修缮
    - [x] 留言板封装评论组件


## 2021.8.18
1. 文章阅读
   - [Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)

2. 源码阅读
    - [vue源码解读(三)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
   - [请你找出并返回这两个正序数组的 中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)

4. 项目进度
    - [x] 留言板分页完善
    - [x] 留言板跳详情渲染
    - [x] 留言板跳详情排版

## 2021.8.17
1. 文章阅读
   - [深入浅出Typescript](https://juejin.cn/book/6844733813021491207/section/6844733813088583687)
   - [Vue3 源码中那些实用的基础工具函数(二)](https://juejin.cn/post/6994976281053888519)

2. 源码阅读
    - [vue源码解读(三)](https://juejin.cn/post/6950084496515399717)

3. leecode 刷题
    - [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
    - [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
    - [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)

4. 项目进度
    - [x] 留言板渲染
    - [x] 留言板修改
    - [x] 留言板分页

## 2021.8.16
1. 文章阅读
   - [Chrome技巧](https://juejin.cn/book/6844733783166418958/section/6844733783187390477) 
   - [通用端]](https://juejin.cn/book/6844733783166418958/section/6844733783204167693)
   
2. 源码阅读
   - [vue源码解读(初始化,简介)](https://juejin.cn/post/6950084496515399717)

3. LeeCode刷题
   - [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)
   - [最长的回文字](https://leetcode-cn.com/problems/longest-palindromic-substring/)

4. 项目进度
  [x] 排版
  [x] 获取数据
  [x] 渲染数据


## 2021.8.15
1. 文章阅读
   - [前端知识体系(一)](https://juejin.cn/post/6994657097220620319)
   - [Vue3 源码中那些实用的基础工具函数(一)](https://juejin.cn/post/6994976281053888519)

2. 源码阅读
    - [vue学习整理](https://juejin.cn/post/6971674228101742629)

3. leecode 刷题
    - [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)
    - [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)
    - [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)

4. 项目进度
    - [x] 搭建项目
    - [x] 首页修改

## 2021.8.13
1. 文章阅读
   - [react api](https://juejin.cn/post/6950063294270930980)

2. 源码阅读
   - [_.chunk](https://www.lodashjs.com/docs/lodash.chunk)

3. LeeCode刷题
   - [两数之和](https://leetcode-cn.com/leetbook/read/tencent/xxqfy5/)
   - [请你找出并返回这两个正序数组的 中位数](https://leetcode-cn.com/leetbook/read/tencent/xx6c46/)

4. 项目进度
   - [x] 创建项目
   - [x] 搭建脚手架
   - [x] 请求数据 响应数据
   - [x] 配置路由

## 2021.8.12
1. 文章阅读
   - [umijs大米饭](https://umijs.org/zh-CN/docs/env-variables)

2. 源码阅读
   - [_.chunk](https://www.lodashjs.com/docs/lodash.chunk)

3. LeeCode刷题
   - [删除数组重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
   - [寻找中心索引](https://leetcode-cn.com/leetbook/read/array-and-string/yf47s/)

4. 项目进度
   - [x] 创建项目
   - [x] 搭建脚手架
   - [x] 路由配好一个路由

## 2021.8.11
1. 文章阅读
   - [git命令大全](https://blog.csdn.net/halaoda/article/details/78661334)

2. 源码阅读

3. LeeCode刷题
   - [数组和字符串](https://leetcode-cn.com/leetbook/detail/array-and-string/)
  
4. 项目进度
